package com.usda.forestry.crais;



import com.wavemaker.runtime.RuntimeAccess;
import org.hibernate.Session;
import org.hibernate.SQLQuery;
import com.wavemaker.runtime.javaservice.JavaServiceSuperClass;
import com.wavemaker.runtime.service.annotations.ExposeToClient;
import com.hrdb.Hrdb;

/**
 * This is a client-facing service class.  All
 * public methods will be exposed to the client.  Their return
 * values and parameters will be passed to the client or taken
 * from the client, respectively.  This will be a singleton
 * instance, shared between all requests. 
 * 
 * To log, call the superclass method log(LOG_LEVEL, String) or log(LOG_LEVEL, String, Exception).
 * LOG_LEVEL is one of FATAL, ERROR, WARN, INFO and DEBUG to modify your log level.
 * For info on these levels, look for tomcat/log4j documentation
 */
@ExposeToClient
public class DBUtility extends JavaServiceSuperClass {
    /* Pass in one of FATAL, ERROR, WARN,  INFO and DEBUG to modify your log level;
     *  recommend changing this to FATAL or ERROR before deploying.  For info on these levels, look for tomcat/log4j documentation
     */
    public DBUtility() {
       super(INFO);
    }

    public String sampleJavaOperation() {
       String result  = null;
       try {
          log(INFO, "Starting sample operation");
          result = "Hello World";
          log(INFO, "Returning " + result);
       } catch(Exception e) {
          log(ERROR, "The sample java service operation has failed", e);
       }
       return result;
    }


public Integer InsertOrder(int iId, String sname) {
    System.out.println("id"+iId);
    System.out.println("name"+sname);
    
Hrdb service = (Hrdb) RuntimeAccess.getInstance().getService(Hrdb.class);
String sQuery = "";
try {
service.begin();
Session session = service.getDataServiceManager().getSession();
sQuery = "insert into report (id,projectname) values("+iId+",'"+sname+"')";
SQLQuery query = session.createSQLQuery(sQuery);
query.executeUpdate();
service.commit();
}
catch (Exception ex)
{
service.rollback();
return 1;
}
return 0;
}




}
