
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisTopography
 *  12/09/2013 10:29:10
 * 
 */
public class CraisTopography {

    private String code;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoW = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoN = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoE = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoS = new HashSet<com.orcl.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoW() {
        return craisSitefileRecordsForTopoW;
    }

    public void setCraisSitefileRecordsForTopoW(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoW) {
        this.craisSitefileRecordsForTopoW = craisSitefileRecordsForTopoW;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoN() {
        return craisSitefileRecordsForTopoN;
    }

    public void setCraisSitefileRecordsForTopoN(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoN) {
        this.craisSitefileRecordsForTopoN = craisSitefileRecordsForTopoN;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoE() {
        return craisSitefileRecordsForTopoE;
    }

    public void setCraisSitefileRecordsForTopoE(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoE) {
        this.craisSitefileRecordsForTopoE = craisSitefileRecordsForTopoE;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoS() {
        return craisSitefileRecordsForTopoS;
    }

    public void setCraisSitefileRecordsForTopoS(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForTopoS) {
        this.craisSitefileRecordsForTopoS = craisSitefileRecordsForTopoS;
    }

}
