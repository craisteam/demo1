
package com.orcl.data;

import java.math.BigDecimal;


/**
 *  orcl.CraisSitefileRecordBad
 *  12/09/2013 10:29:10
 * 
 */
public class CraisSitefileRecordBad {

    private CraisSitefileRecordBadId id;
    private CraisForests craisForests;
    private CraisRockArt craisRockArt;
    private Short reportYear;
    private Short reportNum;
    private Byte inventoryMonth;
    private Byte inventoryDay;
    private Short inventoryYear;
    private Boolean cardOne;
    private String t;
    private BigDecimal township;
    private String NS;
    private String r;
    private BigDecimal range;
    private String EW;
    private Byte sect;
    private Byte state;
    private Short county;
    private Byte zone;
    private Byte northing1;
    private Byte northing2;
    private Short northing3;
    private Boolean easting1;
    private Byte easting2;
    private Short easting3;
    private Integer rimNumber;
    private String evaluation;
    private String condition;
    private Short percentDisturbance;
    private String fileCheck;
    private Byte collectionMade;
    private String collectionType;
    private String siteMarking;
    private Short hoursOnSite;
    private Byte recordedBy;
    private Boolean cardTwo;
    private Integer vegetationCode;
    private String greatGroupSoil;
    private String subsgroupSoil;
    private Short soilDepth;
    private String landform;
    private String topoN;
    private String topoE;
    private String topoS;
    private String topoW;
    private String siteAspect;
    private Byte averageSlope;
    private Integer elevation;
    private String waterType;
    private String waterDirection;
    private BigDecimal waterDistance;
    private String landType;
    private String landDirection;
    private BigDecimal landDistance;
    private String siteClass;
    private String siteUse;
    private Short siteType;
    private Integer siteCoreArea;
    private Integer siteDispersedArea;
    private String flakedStoneObserved;
    private String groundStoneObserved;
    private String ceramicsObserved;
    private String boneObserved;
    private String shellObserved;
    private String otherPrehistObserved;
    private String histCeramicsObserved;
    private String glassObserved;
    private String cansObserved;
    private String otherMetalObserved;
    private String otherHistoricObserved;
    private String constructionMaterial;
    private Boolean cardThree;
    private Integer yearsFrom;
    private Integer yearsTo;
    private String dateBasedOn;
    private Short subsurfaceRooms;
    private Short surfaceRooms;
    private Boolean nonRoomWalls;
    private Boolean partialShelters;
    private Byte middens;
    private Byte hearth;
    private Byte cists;
    private Byte roastingPit;
    private Boolean nonRoofedArea;
    private Short modifiedCave;
    private Boolean depression;
    private Byte waterSoilControl;
    private Boolean nonMiddenMound;
    private Boolean bedrockGrinding;
    private Boolean quarryMine;
    private Short buildings;
    private Byte miscFeatures;
    private String flakedStoneCollected;
    private String groundStoneCollected;
    private String ceramicsCollected;
    private String boneCollected;
    private String shellCollected;
    private String otherPrehistCollected;
    private String histCeramicsCollected;
    private String glassCollected;
    private String cansCollected;
    private String otherMetalCollected;
    private String otherHistoricCollected;
    private String soilSamplesCollected;
    private String pollenCollected;
    private String flotationCollected;
    private String charcoalCollected;
    private String vegSamplesCollected;
    private Boolean multiComponents;
    private String cultClass;
    private String filler;
    private String cultClassPhase;

    public CraisSitefileRecordBadId getId() {
        return id;
    }

    public void setId(CraisSitefileRecordBadId id) {
        this.id = id;
    }

    public CraisForests getCraisForests() {
        return craisForests;
    }

    public void setCraisForests(CraisForests craisForests) {
        this.craisForests = craisForests;
    }

    public CraisRockArt getCraisRockArt() {
        return craisRockArt;
    }

    public void setCraisRockArt(CraisRockArt craisRockArt) {
        this.craisRockArt = craisRockArt;
    }

    public Short getReportYear() {
        return reportYear;
    }

    public void setReportYear(Short reportYear) {
        this.reportYear = reportYear;
    }

    public Short getReportNum() {
        return reportNum;
    }

    public void setReportNum(Short reportNum) {
        this.reportNum = reportNum;
    }

    public Byte getInventoryMonth() {
        return inventoryMonth;
    }

    public void setInventoryMonth(Byte inventoryMonth) {
        this.inventoryMonth = inventoryMonth;
    }

    public Byte getInventoryDay() {
        return inventoryDay;
    }

    public void setInventoryDay(Byte inventoryDay) {
        this.inventoryDay = inventoryDay;
    }

    public Short getInventoryYear() {
        return inventoryYear;
    }

    public void setInventoryYear(Short inventoryYear) {
        this.inventoryYear = inventoryYear;
    }

    public Boolean getCardOne() {
        return cardOne;
    }

    public void setCardOne(Boolean cardOne) {
        this.cardOne = cardOne;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public BigDecimal getTownship() {
        return township;
    }

    public void setTownship(BigDecimal township) {
        this.township = township;
    }

    public String getNS() {
        return NS;
    }

    public void setNS(String NS) {
        this.NS = NS;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public BigDecimal getRange() {
        return range;
    }

    public void setRange(BigDecimal range) {
        this.range = range;
    }

    public String getEW() {
        return EW;
    }

    public void setEW(String EW) {
        this.EW = EW;
    }

    public Byte getSect() {
        return sect;
    }

    public void setSect(Byte sect) {
        this.sect = sect;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

    public Short getCounty() {
        return county;
    }

    public void setCounty(Short county) {
        this.county = county;
    }

    public Byte getZone() {
        return zone;
    }

    public void setZone(Byte zone) {
        this.zone = zone;
    }

    public Byte getNorthing1() {
        return northing1;
    }

    public void setNorthing1(Byte northing1) {
        this.northing1 = northing1;
    }

    public Byte getNorthing2() {
        return northing2;
    }

    public void setNorthing2(Byte northing2) {
        this.northing2 = northing2;
    }

    public Short getNorthing3() {
        return northing3;
    }

    public void setNorthing3(Short northing3) {
        this.northing3 = northing3;
    }

    public Boolean getEasting1() {
        return easting1;
    }

    public void setEasting1(Boolean easting1) {
        this.easting1 = easting1;
    }

    public Byte getEasting2() {
        return easting2;
    }

    public void setEasting2(Byte easting2) {
        this.easting2 = easting2;
    }

    public Short getEasting3() {
        return easting3;
    }

    public void setEasting3(Short easting3) {
        this.easting3 = easting3;
    }

    public Integer getRimNumber() {
        return rimNumber;
    }

    public void setRimNumber(Integer rimNumber) {
        this.rimNumber = rimNumber;
    }

    public String getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(String evaluation) {
        this.evaluation = evaluation;
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public Short getPercentDisturbance() {
        return percentDisturbance;
    }

    public void setPercentDisturbance(Short percentDisturbance) {
        this.percentDisturbance = percentDisturbance;
    }

    public String getFileCheck() {
        return fileCheck;
    }

    public void setFileCheck(String fileCheck) {
        this.fileCheck = fileCheck;
    }

    public Byte getCollectionMade() {
        return collectionMade;
    }

    public void setCollectionMade(Byte collectionMade) {
        this.collectionMade = collectionMade;
    }

    public String getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(String collectionType) {
        this.collectionType = collectionType;
    }

    public String getSiteMarking() {
        return siteMarking;
    }

    public void setSiteMarking(String siteMarking) {
        this.siteMarking = siteMarking;
    }

    public Short getHoursOnSite() {
        return hoursOnSite;
    }

    public void setHoursOnSite(Short hoursOnSite) {
        this.hoursOnSite = hoursOnSite;
    }

    public Byte getRecordedBy() {
        return recordedBy;
    }

    public void setRecordedBy(Byte recordedBy) {
        this.recordedBy = recordedBy;
    }

    public Boolean getCardTwo() {
        return cardTwo;
    }

    public void setCardTwo(Boolean cardTwo) {
        this.cardTwo = cardTwo;
    }

    public Integer getVegetationCode() {
        return vegetationCode;
    }

    public void setVegetationCode(Integer vegetationCode) {
        this.vegetationCode = vegetationCode;
    }

    public String getGreatGroupSoil() {
        return greatGroupSoil;
    }

    public void setGreatGroupSoil(String greatGroupSoil) {
        this.greatGroupSoil = greatGroupSoil;
    }

    public String getSubsgroupSoil() {
        return subsgroupSoil;
    }

    public void setSubsgroupSoil(String subsgroupSoil) {
        this.subsgroupSoil = subsgroupSoil;
    }

    public Short getSoilDepth() {
        return soilDepth;
    }

    public void setSoilDepth(Short soilDepth) {
        this.soilDepth = soilDepth;
    }

    public String getLandform() {
        return landform;
    }

    public void setLandform(String landform) {
        this.landform = landform;
    }

    public String getTopoN() {
        return topoN;
    }

    public void setTopoN(String topoN) {
        this.topoN = topoN;
    }

    public String getTopoE() {
        return topoE;
    }

    public void setTopoE(String topoE) {
        this.topoE = topoE;
    }

    public String getTopoS() {
        return topoS;
    }

    public void setTopoS(String topoS) {
        this.topoS = topoS;
    }

    public String getTopoW() {
        return topoW;
    }

    public void setTopoW(String topoW) {
        this.topoW = topoW;
    }

    public String getSiteAspect() {
        return siteAspect;
    }

    public void setSiteAspect(String siteAspect) {
        this.siteAspect = siteAspect;
    }

    public Byte getAverageSlope() {
        return averageSlope;
    }

    public void setAverageSlope(Byte averageSlope) {
        this.averageSlope = averageSlope;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public String getWaterType() {
        return waterType;
    }

    public void setWaterType(String waterType) {
        this.waterType = waterType;
    }

    public String getWaterDirection() {
        return waterDirection;
    }

    public void setWaterDirection(String waterDirection) {
        this.waterDirection = waterDirection;
    }

    public BigDecimal getWaterDistance() {
        return waterDistance;
    }

    public void setWaterDistance(BigDecimal waterDistance) {
        this.waterDistance = waterDistance;
    }

    public String getLandType() {
        return landType;
    }

    public void setLandType(String landType) {
        this.landType = landType;
    }

    public String getLandDirection() {
        return landDirection;
    }

    public void setLandDirection(String landDirection) {
        this.landDirection = landDirection;
    }

    public BigDecimal getLandDistance() {
        return landDistance;
    }

    public void setLandDistance(BigDecimal landDistance) {
        this.landDistance = landDistance;
    }

    public String getSiteClass() {
        return siteClass;
    }

    public void setSiteClass(String siteClass) {
        this.siteClass = siteClass;
    }

    public String getSiteUse() {
        return siteUse;
    }

    public void setSiteUse(String siteUse) {
        this.siteUse = siteUse;
    }

    public Short getSiteType() {
        return siteType;
    }

    public void setSiteType(Short siteType) {
        this.siteType = siteType;
    }

    public Integer getSiteCoreArea() {
        return siteCoreArea;
    }

    public void setSiteCoreArea(Integer siteCoreArea) {
        this.siteCoreArea = siteCoreArea;
    }

    public Integer getSiteDispersedArea() {
        return siteDispersedArea;
    }

    public void setSiteDispersedArea(Integer siteDispersedArea) {
        this.siteDispersedArea = siteDispersedArea;
    }

    public String getFlakedStoneObserved() {
        return flakedStoneObserved;
    }

    public void setFlakedStoneObserved(String flakedStoneObserved) {
        this.flakedStoneObserved = flakedStoneObserved;
    }

    public String getGroundStoneObserved() {
        return groundStoneObserved;
    }

    public void setGroundStoneObserved(String groundStoneObserved) {
        this.groundStoneObserved = groundStoneObserved;
    }

    public String getCeramicsObserved() {
        return ceramicsObserved;
    }

    public void setCeramicsObserved(String ceramicsObserved) {
        this.ceramicsObserved = ceramicsObserved;
    }

    public String getBoneObserved() {
        return boneObserved;
    }

    public void setBoneObserved(String boneObserved) {
        this.boneObserved = boneObserved;
    }

    public String getShellObserved() {
        return shellObserved;
    }

    public void setShellObserved(String shellObserved) {
        this.shellObserved = shellObserved;
    }

    public String getOtherPrehistObserved() {
        return otherPrehistObserved;
    }

    public void setOtherPrehistObserved(String otherPrehistObserved) {
        this.otherPrehistObserved = otherPrehistObserved;
    }

    public String getHistCeramicsObserved() {
        return histCeramicsObserved;
    }

    public void setHistCeramicsObserved(String histCeramicsObserved) {
        this.histCeramicsObserved = histCeramicsObserved;
    }

    public String getGlassObserved() {
        return glassObserved;
    }

    public void setGlassObserved(String glassObserved) {
        this.glassObserved = glassObserved;
    }

    public String getCansObserved() {
        return cansObserved;
    }

    public void setCansObserved(String cansObserved) {
        this.cansObserved = cansObserved;
    }

    public String getOtherMetalObserved() {
        return otherMetalObserved;
    }

    public void setOtherMetalObserved(String otherMetalObserved) {
        this.otherMetalObserved = otherMetalObserved;
    }

    public String getOtherHistoricObserved() {
        return otherHistoricObserved;
    }

    public void setOtherHistoricObserved(String otherHistoricObserved) {
        this.otherHistoricObserved = otherHistoricObserved;
    }

    public String getConstructionMaterial() {
        return constructionMaterial;
    }

    public void setConstructionMaterial(String constructionMaterial) {
        this.constructionMaterial = constructionMaterial;
    }

    public Boolean getCardThree() {
        return cardThree;
    }

    public void setCardThree(Boolean cardThree) {
        this.cardThree = cardThree;
    }

    public Integer getYearsFrom() {
        return yearsFrom;
    }

    public void setYearsFrom(Integer yearsFrom) {
        this.yearsFrom = yearsFrom;
    }

    public Integer getYearsTo() {
        return yearsTo;
    }

    public void setYearsTo(Integer yearsTo) {
        this.yearsTo = yearsTo;
    }

    public String getDateBasedOn() {
        return dateBasedOn;
    }

    public void setDateBasedOn(String dateBasedOn) {
        this.dateBasedOn = dateBasedOn;
    }

    public Short getSubsurfaceRooms() {
        return subsurfaceRooms;
    }

    public void setSubsurfaceRooms(Short subsurfaceRooms) {
        this.subsurfaceRooms = subsurfaceRooms;
    }

    public Short getSurfaceRooms() {
        return surfaceRooms;
    }

    public void setSurfaceRooms(Short surfaceRooms) {
        this.surfaceRooms = surfaceRooms;
    }

    public Boolean getNonRoomWalls() {
        return nonRoomWalls;
    }

    public void setNonRoomWalls(Boolean nonRoomWalls) {
        this.nonRoomWalls = nonRoomWalls;
    }

    public Boolean getPartialShelters() {
        return partialShelters;
    }

    public void setPartialShelters(Boolean partialShelters) {
        this.partialShelters = partialShelters;
    }

    public Byte getMiddens() {
        return middens;
    }

    public void setMiddens(Byte middens) {
        this.middens = middens;
    }

    public Byte getHearth() {
        return hearth;
    }

    public void setHearth(Byte hearth) {
        this.hearth = hearth;
    }

    public Byte getCists() {
        return cists;
    }

    public void setCists(Byte cists) {
        this.cists = cists;
    }

    public Byte getRoastingPit() {
        return roastingPit;
    }

    public void setRoastingPit(Byte roastingPit) {
        this.roastingPit = roastingPit;
    }

    public Boolean getNonRoofedArea() {
        return nonRoofedArea;
    }

    public void setNonRoofedArea(Boolean nonRoofedArea) {
        this.nonRoofedArea = nonRoofedArea;
    }

    public Short getModifiedCave() {
        return modifiedCave;
    }

    public void setModifiedCave(Short modifiedCave) {
        this.modifiedCave = modifiedCave;
    }

    public Boolean getDepression() {
        return depression;
    }

    public void setDepression(Boolean depression) {
        this.depression = depression;
    }

    public Byte getWaterSoilControl() {
        return waterSoilControl;
    }

    public void setWaterSoilControl(Byte waterSoilControl) {
        this.waterSoilControl = waterSoilControl;
    }

    public Boolean getNonMiddenMound() {
        return nonMiddenMound;
    }

    public void setNonMiddenMound(Boolean nonMiddenMound) {
        this.nonMiddenMound = nonMiddenMound;
    }

    public Boolean getBedrockGrinding() {
        return bedrockGrinding;
    }

    public void setBedrockGrinding(Boolean bedrockGrinding) {
        this.bedrockGrinding = bedrockGrinding;
    }

    public Boolean getQuarryMine() {
        return quarryMine;
    }

    public void setQuarryMine(Boolean quarryMine) {
        this.quarryMine = quarryMine;
    }

    public Short getBuildings() {
        return buildings;
    }

    public void setBuildings(Short buildings) {
        this.buildings = buildings;
    }

    public Byte getMiscFeatures() {
        return miscFeatures;
    }

    public void setMiscFeatures(Byte miscFeatures) {
        this.miscFeatures = miscFeatures;
    }

    public String getFlakedStoneCollected() {
        return flakedStoneCollected;
    }

    public void setFlakedStoneCollected(String flakedStoneCollected) {
        this.flakedStoneCollected = flakedStoneCollected;
    }

    public String getGroundStoneCollected() {
        return groundStoneCollected;
    }

    public void setGroundStoneCollected(String groundStoneCollected) {
        this.groundStoneCollected = groundStoneCollected;
    }

    public String getCeramicsCollected() {
        return ceramicsCollected;
    }

    public void setCeramicsCollected(String ceramicsCollected) {
        this.ceramicsCollected = ceramicsCollected;
    }

    public String getBoneCollected() {
        return boneCollected;
    }

    public void setBoneCollected(String boneCollected) {
        this.boneCollected = boneCollected;
    }

    public String getShellCollected() {
        return shellCollected;
    }

    public void setShellCollected(String shellCollected) {
        this.shellCollected = shellCollected;
    }

    public String getOtherPrehistCollected() {
        return otherPrehistCollected;
    }

    public void setOtherPrehistCollected(String otherPrehistCollected) {
        this.otherPrehistCollected = otherPrehistCollected;
    }

    public String getHistCeramicsCollected() {
        return histCeramicsCollected;
    }

    public void setHistCeramicsCollected(String histCeramicsCollected) {
        this.histCeramicsCollected = histCeramicsCollected;
    }

    public String getGlassCollected() {
        return glassCollected;
    }

    public void setGlassCollected(String glassCollected) {
        this.glassCollected = glassCollected;
    }

    public String getCansCollected() {
        return cansCollected;
    }

    public void setCansCollected(String cansCollected) {
        this.cansCollected = cansCollected;
    }

    public String getOtherMetalCollected() {
        return otherMetalCollected;
    }

    public void setOtherMetalCollected(String otherMetalCollected) {
        this.otherMetalCollected = otherMetalCollected;
    }

    public String getOtherHistoricCollected() {
        return otherHistoricCollected;
    }

    public void setOtherHistoricCollected(String otherHistoricCollected) {
        this.otherHistoricCollected = otherHistoricCollected;
    }

    public String getSoilSamplesCollected() {
        return soilSamplesCollected;
    }

    public void setSoilSamplesCollected(String soilSamplesCollected) {
        this.soilSamplesCollected = soilSamplesCollected;
    }

    public String getPollenCollected() {
        return pollenCollected;
    }

    public void setPollenCollected(String pollenCollected) {
        this.pollenCollected = pollenCollected;
    }

    public String getFlotationCollected() {
        return flotationCollected;
    }

    public void setFlotationCollected(String flotationCollected) {
        this.flotationCollected = flotationCollected;
    }

    public String getCharcoalCollected() {
        return charcoalCollected;
    }

    public void setCharcoalCollected(String charcoalCollected) {
        this.charcoalCollected = charcoalCollected;
    }

    public String getVegSamplesCollected() {
        return vegSamplesCollected;
    }

    public void setVegSamplesCollected(String vegSamplesCollected) {
        this.vegSamplesCollected = vegSamplesCollected;
    }

    public Boolean getMultiComponents() {
        return multiComponents;
    }

    public void setMultiComponents(Boolean multiComponents) {
        this.multiComponents = multiComponents;
    }

    public String getCultClass() {
        return cultClass;
    }

    public void setCultClass(String cultClass) {
        this.cultClass = cultClass;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public String getCultClassPhase() {
        return cultClassPhase;
    }

    public void setCultClassPhase(String cultClassPhase) {
        this.cultClassPhase = cultClassPhase;
    }

}
