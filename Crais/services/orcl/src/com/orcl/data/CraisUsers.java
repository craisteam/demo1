
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisUsers
 *  12/09/2013 10:29:10
 * 
 */
public class CraisUsers {

    private String username;
    private String lname;
    private String fname;
    private String MInit;
    private Set<com.orcl.data.CraisUserRoles> craisUserRoleses = new HashSet<com.orcl.data.CraisUserRoles>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMInit() {
        return MInit;
    }

    public void setMInit(String MInit) {
        this.MInit = MInit;
    }

    public Set<com.orcl.data.CraisUserRoles> getCraisUserRoleses() {
        return craisUserRoleses;
    }

    public void setCraisUserRoleses(Set<com.orcl.data.CraisUserRoles> craisUserRoleses) {
        this.craisUserRoleses = craisUserRoleses;
    }

}
