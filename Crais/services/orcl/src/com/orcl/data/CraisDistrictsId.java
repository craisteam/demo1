
package com.orcl.data;

import java.io.Serializable;


/**
 *  orcl.CraisDistrictsId
 *  12/09/2013 10:29:11
 * 
 */
public class CraisDistrictsId
    implements Serializable
{

    private Byte code;
    private Byte forest;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisDistrictsId)) {
            return false;
        }
        CraisDistrictsId other = ((CraisDistrictsId) o);
        if (this.code == null) {
            if (other.code!= null) {
                return false;
            }
        } else {
            if (!this.code.equals(other.code)) {
                return false;
            }
        }
        if (this.forest == null) {
            if (other.forest!= null) {
                return false;
            }
        } else {
            if (!this.forest.equals(other.forest)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.code!= null) {
            rtn = (rtn + this.code.hashCode());
        }
        rtn = (rtn* 37);
        if (this.forest!= null) {
            rtn = (rtn + this.forest.hashCode());
        }
        return rtn;
    }

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public Byte getForest() {
        return forest;
    }

    public void setForest(Byte forest) {
        this.forest = forest;
    }

}
