
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisSiteType
 *  12/09/2013 10:29:10
 * 
 */
public class CraisSiteType {

    private Short code;
    private CraisSiteUse craisSiteUse;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcl.data.CraisSitefileRecord>();

    public Short getCode() {
        return code;
    }

    public void setCode(Short code) {
        this.code = code;
    }

    public CraisSiteUse getCraisSiteUse() {
        return craisSiteUse;
    }

    public void setCraisSiteUse(CraisSiteUse craisSiteUse) {
        this.craisSiteUse = craisSiteUse;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

}
