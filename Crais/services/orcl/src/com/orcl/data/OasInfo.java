
package com.orcl.data;



/**
 *  orcl.OasInfo
 *  12/09/2013 10:29:10
 * 
 */
public class OasInfo {

    private OasInfoId id;

    public OasInfoId getId() {
        return id;
    }

    public void setId(OasInfoId id) {
        this.id = id;
    }

}
