
package com.orcl.data;

import java.io.Serializable;


/**
 *  orcl.CraisCountiesId
 *  12/09/2013 10:29:10
 * 
 */
public class CraisCountiesId
    implements Serializable
{

    private Short code;
    private Byte state;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisCountiesId)) {
            return false;
        }
        CraisCountiesId other = ((CraisCountiesId) o);
        if (this.code == null) {
            if (other.code!= null) {
                return false;
            }
        } else {
            if (!this.code.equals(other.code)) {
                return false;
            }
        }
        if (this.state == null) {
            if (other.state!= null) {
                return false;
            }
        } else {
            if (!this.state.equals(other.state)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.code!= null) {
            rtn = (rtn + this.code.hashCode());
        }
        rtn = (rtn* 37);
        if (this.state!= null) {
            rtn = (rtn + this.state.hashCode());
        }
        return rtn;
    }

    public Short getCode() {
        return code;
    }

    public void setCode(Short code) {
        this.code = code;
    }

    public Byte getState() {
        return state;
    }

    public void setState(Byte state) {
        this.state = state;
    }

}
