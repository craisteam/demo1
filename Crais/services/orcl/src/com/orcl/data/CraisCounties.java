
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisCounties
 *  12/09/2013 10:29:10
 * 
 */
public class CraisCounties {

    private CraisCountiesId id;
    private CraisStates craisStates;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcl.data.CraisSitefileRecord>();

    public CraisCountiesId getId() {
        return id;
    }

    public void setId(CraisCountiesId id) {
        this.id = id;
    }

    public CraisStates getCraisStates() {
        return craisStates;
    }

    public void setCraisStates(CraisStates craisStates) {
        this.craisStates = craisStates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

}
