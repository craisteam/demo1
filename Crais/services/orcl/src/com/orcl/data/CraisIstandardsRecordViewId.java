
package com.orcl.data;

import java.io.Serializable;
import java.util.Date;


/**
 *  orcl.CraisIstandardsRecordViewId
 *  12/09/2013 10:29:11
 * 
 */
public class CraisIstandardsRecordViewId
    implements Serializable
{

    private Date reg;
    private Date ryear;
    private Date forst;
    private Date rnum;
    private Date s1;
    private Date s2;
    private Date rmon;
    private Date rday;
    private Date ryear1;
    private Date a1Last;
    private Date f1;
    private Date m1;
    private Date a2Last;
    private Date f2;
    private Date m2;
    private Date reportTitle;
    private Date dist;
    private Date pf;
    private Date activityType1;
    private Date activityType2;
    private Date prog;
    private Date tpa;
    private Date inst;
    private Date aci;
    private Date ps;
    private Date ani;
    private Date ar;
    private Date asp;
    private Date ts;
    private Date fh;
    private Date nsl;
    private Date llh;
    private Date th;
    private Date se;
    private Date ah;
    private Date sne;
    private Date ml;
    private Date pdr;
    private Date seh;
    private Date pdp;
    private Date cw;
    private Date rde;
    private Date tcc;
    private Date acc;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisIstandardsRecordViewId)) {
            return false;
        }
        CraisIstandardsRecordViewId other = ((CraisIstandardsRecordViewId) o);
        if (this.reg == null) {
            if (other.reg!= null) {
                return false;
            }
        } else {
            if (!this.reg.equals(other.reg)) {
                return false;
            }
        }
        if (this.ryear == null) {
            if (other.ryear!= null) {
                return false;
            }
        } else {
            if (!this.ryear.equals(other.ryear)) {
                return false;
            }
        }
        if (this.forst == null) {
            if (other.forst!= null) {
                return false;
            }
        } else {
            if (!this.forst.equals(other.forst)) {
                return false;
            }
        }
        if (this.rnum == null) {
            if (other.rnum!= null) {
                return false;
            }
        } else {
            if (!this.rnum.equals(other.rnum)) {
                return false;
            }
        }
        if (this.s1 == null) {
            if (other.s1 != null) {
                return false;
            }
        } else {
            if (!this.s1 .equals(other.s1)) {
                return false;
            }
        }
        if (this.s2 == null) {
            if (other.s2 != null) {
                return false;
            }
        } else {
            if (!this.s2 .equals(other.s2)) {
                return false;
            }
        }
        if (this.rmon == null) {
            if (other.rmon!= null) {
                return false;
            }
        } else {
            if (!this.rmon.equals(other.rmon)) {
                return false;
            }
        }
        if (this.rday == null) {
            if (other.rday!= null) {
                return false;
            }
        } else {
            if (!this.rday.equals(other.rday)) {
                return false;
            }
        }
        if (this.ryear1 == null) {
            if (other.ryear1 != null) {
                return false;
            }
        } else {
            if (!this.ryear1 .equals(other.ryear1)) {
                return false;
            }
        }
        if (this.a1Last == null) {
            if (other.a1Last!= null) {
                return false;
            }
        } else {
            if (!this.a1Last.equals(other.a1Last)) {
                return false;
            }
        }
        if (this.f1 == null) {
            if (other.f1 != null) {
                return false;
            }
        } else {
            if (!this.f1 .equals(other.f1)) {
                return false;
            }
        }
        if (this.m1 == null) {
            if (other.m1 != null) {
                return false;
            }
        } else {
            if (!this.m1 .equals(other.m1)) {
                return false;
            }
        }
        if (this.a2Last == null) {
            if (other.a2Last!= null) {
                return false;
            }
        } else {
            if (!this.a2Last.equals(other.a2Last)) {
                return false;
            }
        }
        if (this.f2 == null) {
            if (other.f2 != null) {
                return false;
            }
        } else {
            if (!this.f2 .equals(other.f2)) {
                return false;
            }
        }
        if (this.m2 == null) {
            if (other.m2 != null) {
                return false;
            }
        } else {
            if (!this.m2 .equals(other.m2)) {
                return false;
            }
        }
        if (this.reportTitle == null) {
            if (other.reportTitle!= null) {
                return false;
            }
        } else {
            if (!this.reportTitle.equals(other.reportTitle)) {
                return false;
            }
        }
        if (this.dist == null) {
            if (other.dist!= null) {
                return false;
            }
        } else {
            if (!this.dist.equals(other.dist)) {
                return false;
            }
        }
        if (this.pf == null) {
            if (other.pf!= null) {
                return false;
            }
        } else {
            if (!this.pf.equals(other.pf)) {
                return false;
            }
        }
        if (this.activityType1 == null) {
            if (other.activityType1 != null) {
                return false;
            }
        } else {
            if (!this.activityType1 .equals(other.activityType1)) {
                return false;
            }
        }
        if (this.activityType2 == null) {
            if (other.activityType2 != null) {
                return false;
            }
        } else {
            if (!this.activityType2 .equals(other.activityType2)) {
                return false;
            }
        }
        if (this.prog == null) {
            if (other.prog!= null) {
                return false;
            }
        } else {
            if (!this.prog.equals(other.prog)) {
                return false;
            }
        }
        if (this.tpa == null) {
            if (other.tpa!= null) {
                return false;
            }
        } else {
            if (!this.tpa.equals(other.tpa)) {
                return false;
            }
        }
        if (this.inst == null) {
            if (other.inst!= null) {
                return false;
            }
        } else {
            if (!this.inst.equals(other.inst)) {
                return false;
            }
        }
        if (this.aci == null) {
            if (other.aci!= null) {
                return false;
            }
        } else {
            if (!this.aci.equals(other.aci)) {
                return false;
            }
        }
        if (this.ps == null) {
            if (other.ps!= null) {
                return false;
            }
        } else {
            if (!this.ps.equals(other.ps)) {
                return false;
            }
        }
        if (this.ani == null) {
            if (other.ani!= null) {
                return false;
            }
        } else {
            if (!this.ani.equals(other.ani)) {
                return false;
            }
        }
        if (this.ar == null) {
            if (other.ar!= null) {
                return false;
            }
        } else {
            if (!this.ar.equals(other.ar)) {
                return false;
            }
        }
        if (this.asp == null) {
            if (other.asp!= null) {
                return false;
            }
        } else {
            if (!this.asp.equals(other.asp)) {
                return false;
            }
        }
        if (this.ts == null) {
            if (other.ts!= null) {
                return false;
            }
        } else {
            if (!this.ts.equals(other.ts)) {
                return false;
            }
        }
        if (this.fh == null) {
            if (other.fh!= null) {
                return false;
            }
        } else {
            if (!this.fh.equals(other.fh)) {
                return false;
            }
        }
        if (this.nsl == null) {
            if (other.nsl!= null) {
                return false;
            }
        } else {
            if (!this.nsl.equals(other.nsl)) {
                return false;
            }
        }
        if (this.llh == null) {
            if (other.llh!= null) {
                return false;
            }
        } else {
            if (!this.llh.equals(other.llh)) {
                return false;
            }
        }
        if (this.th == null) {
            if (other.th!= null) {
                return false;
            }
        } else {
            if (!this.th.equals(other.th)) {
                return false;
            }
        }
        if (this.se == null) {
            if (other.se!= null) {
                return false;
            }
        } else {
            if (!this.se.equals(other.se)) {
                return false;
            }
        }
        if (this.ah == null) {
            if (other.ah!= null) {
                return false;
            }
        } else {
            if (!this.ah.equals(other.ah)) {
                return false;
            }
        }
        if (this.sne == null) {
            if (other.sne!= null) {
                return false;
            }
        } else {
            if (!this.sne.equals(other.sne)) {
                return false;
            }
        }
        if (this.ml == null) {
            if (other.ml!= null) {
                return false;
            }
        } else {
            if (!this.ml.equals(other.ml)) {
                return false;
            }
        }
        if (this.pdr == null) {
            if (other.pdr!= null) {
                return false;
            }
        } else {
            if (!this.pdr.equals(other.pdr)) {
                return false;
            }
        }
        if (this.seh == null) {
            if (other.seh!= null) {
                return false;
            }
        } else {
            if (!this.seh.equals(other.seh)) {
                return false;
            }
        }
        if (this.pdp == null) {
            if (other.pdp!= null) {
                return false;
            }
        } else {
            if (!this.pdp.equals(other.pdp)) {
                return false;
            }
        }
        if (this.cw == null) {
            if (other.cw!= null) {
                return false;
            }
        } else {
            if (!this.cw.equals(other.cw)) {
                return false;
            }
        }
        if (this.rde == null) {
            if (other.rde!= null) {
                return false;
            }
        } else {
            if (!this.rde.equals(other.rde)) {
                return false;
            }
        }
        if (this.tcc == null) {
            if (other.tcc!= null) {
                return false;
            }
        } else {
            if (!this.tcc.equals(other.tcc)) {
                return false;
            }
        }
        if (this.acc == null) {
            if (other.acc!= null) {
                return false;
            }
        } else {
            if (!this.acc.equals(other.acc)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.reg!= null) {
            rtn = (rtn + this.reg.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ryear!= null) {
            rtn = (rtn + this.ryear.hashCode());
        }
        rtn = (rtn* 37);
        if (this.forst!= null) {
            rtn = (rtn + this.forst.hashCode());
        }
        rtn = (rtn* 37);
        if (this.rnum!= null) {
            rtn = (rtn + this.rnum.hashCode());
        }
        rtn = (rtn* 37);
        if (this.s1 != null) {
            rtn = (rtn + this.s1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.s2 != null) {
            rtn = (rtn + this.s2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.rmon!= null) {
            rtn = (rtn + this.rmon.hashCode());
        }
        rtn = (rtn* 37);
        if (this.rday!= null) {
            rtn = (rtn + this.rday.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ryear1 != null) {
            rtn = (rtn + this.ryear1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.a1Last!= null) {
            rtn = (rtn + this.a1Last.hashCode());
        }
        rtn = (rtn* 37);
        if (this.f1 != null) {
            rtn = (rtn + this.f1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.m1 != null) {
            rtn = (rtn + this.m1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.a2Last!= null) {
            rtn = (rtn + this.a2Last.hashCode());
        }
        rtn = (rtn* 37);
        if (this.f2 != null) {
            rtn = (rtn + this.f2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.m2 != null) {
            rtn = (rtn + this.m2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportTitle!= null) {
            rtn = (rtn + this.reportTitle.hashCode());
        }
        rtn = (rtn* 37);
        if (this.dist!= null) {
            rtn = (rtn + this.dist.hashCode());
        }
        rtn = (rtn* 37);
        if (this.pf!= null) {
            rtn = (rtn + this.pf.hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType1 != null) {
            rtn = (rtn + this.activityType1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType2 != null) {
            rtn = (rtn + this.activityType2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.prog!= null) {
            rtn = (rtn + this.prog.hashCode());
        }
        rtn = (rtn* 37);
        if (this.tpa!= null) {
            rtn = (rtn + this.tpa.hashCode());
        }
        rtn = (rtn* 37);
        if (this.inst!= null) {
            rtn = (rtn + this.inst.hashCode());
        }
        rtn = (rtn* 37);
        if (this.aci!= null) {
            rtn = (rtn + this.aci.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ps!= null) {
            rtn = (rtn + this.ps.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ani!= null) {
            rtn = (rtn + this.ani.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ar!= null) {
            rtn = (rtn + this.ar.hashCode());
        }
        rtn = (rtn* 37);
        if (this.asp!= null) {
            rtn = (rtn + this.asp.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ts!= null) {
            rtn = (rtn + this.ts.hashCode());
        }
        rtn = (rtn* 37);
        if (this.fh!= null) {
            rtn = (rtn + this.fh.hashCode());
        }
        rtn = (rtn* 37);
        if (this.nsl!= null) {
            rtn = (rtn + this.nsl.hashCode());
        }
        rtn = (rtn* 37);
        if (this.llh!= null) {
            rtn = (rtn + this.llh.hashCode());
        }
        rtn = (rtn* 37);
        if (this.th!= null) {
            rtn = (rtn + this.th.hashCode());
        }
        rtn = (rtn* 37);
        if (this.se!= null) {
            rtn = (rtn + this.se.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ah!= null) {
            rtn = (rtn + this.ah.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sne!= null) {
            rtn = (rtn + this.sne.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ml!= null) {
            rtn = (rtn + this.ml.hashCode());
        }
        rtn = (rtn* 37);
        if (this.pdr!= null) {
            rtn = (rtn + this.pdr.hashCode());
        }
        rtn = (rtn* 37);
        if (this.seh!= null) {
            rtn = (rtn + this.seh.hashCode());
        }
        rtn = (rtn* 37);
        if (this.pdp!= null) {
            rtn = (rtn + this.pdp.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cw!= null) {
            rtn = (rtn + this.cw.hashCode());
        }
        rtn = (rtn* 37);
        if (this.rde!= null) {
            rtn = (rtn + this.rde.hashCode());
        }
        rtn = (rtn* 37);
        if (this.tcc!= null) {
            rtn = (rtn + this.tcc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.acc!= null) {
            rtn = (rtn + this.acc.hashCode());
        }
        return rtn;
    }

    public Date getReg() {
        return reg;
    }

    public void setReg(Date reg) {
        this.reg = reg;
    }

    public Date getRyear() {
        return ryear;
    }

    public void setRyear(Date ryear) {
        this.ryear = ryear;
    }

    public Date getForst() {
        return forst;
    }

    public void setForst(Date forst) {
        this.forst = forst;
    }

    public Date getRnum() {
        return rnum;
    }

    public void setRnum(Date rnum) {
        this.rnum = rnum;
    }

    public Date getS1() {
        return s1;
    }

    public void setS1(Date s1) {
        this.s1 = s1;
    }

    public Date getS2() {
        return s2;
    }

    public void setS2(Date s2) {
        this.s2 = s2;
    }

    public Date getRmon() {
        return rmon;
    }

    public void setRmon(Date rmon) {
        this.rmon = rmon;
    }

    public Date getRday() {
        return rday;
    }

    public void setRday(Date rday) {
        this.rday = rday;
    }

    public Date getRyear1() {
        return ryear1;
    }

    public void setRyear1(Date ryear1) {
        this.ryear1 = ryear1;
    }

    public Date getA1Last() {
        return a1Last;
    }

    public void setA1Last(Date a1Last) {
        this.a1Last = a1Last;
    }

    public Date getF1() {
        return f1;
    }

    public void setF1(Date f1) {
        this.f1 = f1;
    }

    public Date getM1() {
        return m1;
    }

    public void setM1(Date m1) {
        this.m1 = m1;
    }

    public Date getA2Last() {
        return a2Last;
    }

    public void setA2Last(Date a2Last) {
        this.a2Last = a2Last;
    }

    public Date getF2() {
        return f2;
    }

    public void setF2(Date f2) {
        this.f2 = f2;
    }

    public Date getM2() {
        return m2;
    }

    public void setM2(Date m2) {
        this.m2 = m2;
    }

    public Date getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(Date reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Date getDist() {
        return dist;
    }

    public void setDist(Date dist) {
        this.dist = dist;
    }

    public Date getPf() {
        return pf;
    }

    public void setPf(Date pf) {
        this.pf = pf;
    }

    public Date getActivityType1() {
        return activityType1;
    }

    public void setActivityType1(Date activityType1) {
        this.activityType1 = activityType1;
    }

    public Date getActivityType2() {
        return activityType2;
    }

    public void setActivityType2(Date activityType2) {
        this.activityType2 = activityType2;
    }

    public Date getProg() {
        return prog;
    }

    public void setProg(Date prog) {
        this.prog = prog;
    }

    public Date getTpa() {
        return tpa;
    }

    public void setTpa(Date tpa) {
        this.tpa = tpa;
    }

    public Date getInst() {
        return inst;
    }

    public void setInst(Date inst) {
        this.inst = inst;
    }

    public Date getAci() {
        return aci;
    }

    public void setAci(Date aci) {
        this.aci = aci;
    }

    public Date getPs() {
        return ps;
    }

    public void setPs(Date ps) {
        this.ps = ps;
    }

    public Date getAni() {
        return ani;
    }

    public void setAni(Date ani) {
        this.ani = ani;
    }

    public Date getAr() {
        return ar;
    }

    public void setAr(Date ar) {
        this.ar = ar;
    }

    public Date getAsp() {
        return asp;
    }

    public void setAsp(Date asp) {
        this.asp = asp;
    }

    public Date getTs() {
        return ts;
    }

    public void setTs(Date ts) {
        this.ts = ts;
    }

    public Date getFh() {
        return fh;
    }

    public void setFh(Date fh) {
        this.fh = fh;
    }

    public Date getNsl() {
        return nsl;
    }

    public void setNsl(Date nsl) {
        this.nsl = nsl;
    }

    public Date getLlh() {
        return llh;
    }

    public void setLlh(Date llh) {
        this.llh = llh;
    }

    public Date getTh() {
        return th;
    }

    public void setTh(Date th) {
        this.th = th;
    }

    public Date getSe() {
        return se;
    }

    public void setSe(Date se) {
        this.se = se;
    }

    public Date getAh() {
        return ah;
    }

    public void setAh(Date ah) {
        this.ah = ah;
    }

    public Date getSne() {
        return sne;
    }

    public void setSne(Date sne) {
        this.sne = sne;
    }

    public Date getMl() {
        return ml;
    }

    public void setMl(Date ml) {
        this.ml = ml;
    }

    public Date getPdr() {
        return pdr;
    }

    public void setPdr(Date pdr) {
        this.pdr = pdr;
    }

    public Date getSeh() {
        return seh;
    }

    public void setSeh(Date seh) {
        this.seh = seh;
    }

    public Date getPdp() {
        return pdp;
    }

    public void setPdp(Date pdp) {
        this.pdp = pdp;
    }

    public Date getCw() {
        return cw;
    }

    public void setCw(Date cw) {
        this.cw = cw;
    }

    public Date getRde() {
        return rde;
    }

    public void setRde(Date rde) {
        this.rde = rde;
    }

    public Date getTcc() {
        return tcc;
    }

    public void setTcc(Date tcc) {
        this.tcc = tcc;
    }

    public Date getAcc() {
        return acc;
    }

    public void setAcc(Date acc) {
        this.acc = acc;
    }

}
