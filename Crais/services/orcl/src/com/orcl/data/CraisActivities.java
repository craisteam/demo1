
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisActivities
 *  12/09/2013 10:29:10
 * 
 */
public class CraisActivities {

    private String code;
    private String description;
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType2 = new HashSet<com.orcl.data.CraisIstandardsRecord>();
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType1 = new HashSet<com.orcl.data.CraisIstandardsRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecordsForActivityType2() {
        return craisIstandardsRecordsForActivityType2;
    }

    public void setCraisIstandardsRecordsForActivityType2(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType2) {
        this.craisIstandardsRecordsForActivityType2 = craisIstandardsRecordsForActivityType2;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecordsForActivityType1() {
        return craisIstandardsRecordsForActivityType1;
    }

    public void setCraisIstandardsRecordsForActivityType1(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType1) {
        this.craisIstandardsRecordsForActivityType1 = craisIstandardsRecordsForActivityType1;
    }

}
