
package com.orcl.data;



/**
 *  orcl.CraisIstandardsRecord
 *  12/09/2013 10:29:10
 * 
 */
public class CraisIstandardsRecord {

    private CraisIstandardsRecordId id;
    private com.orcl.data.CraisActivities craisActivitiesByActivityType1;
    private CraisProjectFunction craisProjectFunction;
    private CraisForests craisForests;
    private CraisPerDiemRate craisPerDiemRate;
    private CraisInstitutions craisInstitutions;
    private CraisDetOfEffect craisDetOfEffect;
    private CraisCostWtFactor craisCostWtFactor;
    private CraisStates craisStates;
    private CraisProgramming craisProgramming;
    private CraisDistricts craisDistricts;
    private CraisCosts craisCosts;
    private com.orcl.data.CraisActivities craisActivitiesByActivityType2;

    public CraisIstandardsRecordId getId() {
        return id;
    }

    public void setId(CraisIstandardsRecordId id) {
        this.id = id;
    }

    public com.orcl.data.CraisActivities getCraisActivitiesByActivityType1() {
        return craisActivitiesByActivityType1;
    }

    public void setCraisActivitiesByActivityType1(com.orcl.data.CraisActivities craisActivitiesByActivityType1) {
        this.craisActivitiesByActivityType1 = craisActivitiesByActivityType1;
    }

    public CraisProjectFunction getCraisProjectFunction() {
        return craisProjectFunction;
    }

    public void setCraisProjectFunction(CraisProjectFunction craisProjectFunction) {
        this.craisProjectFunction = craisProjectFunction;
    }

    public CraisForests getCraisForests() {
        return craisForests;
    }

    public void setCraisForests(CraisForests craisForests) {
        this.craisForests = craisForests;
    }

    public CraisPerDiemRate getCraisPerDiemRate() {
        return craisPerDiemRate;
    }

    public void setCraisPerDiemRate(CraisPerDiemRate craisPerDiemRate) {
        this.craisPerDiemRate = craisPerDiemRate;
    }

    public CraisInstitutions getCraisInstitutions() {
        return craisInstitutions;
    }

    public void setCraisInstitutions(CraisInstitutions craisInstitutions) {
        this.craisInstitutions = craisInstitutions;
    }

    public CraisDetOfEffect getCraisDetOfEffect() {
        return craisDetOfEffect;
    }

    public void setCraisDetOfEffect(CraisDetOfEffect craisDetOfEffect) {
        this.craisDetOfEffect = craisDetOfEffect;
    }

    public CraisCostWtFactor getCraisCostWtFactor() {
        return craisCostWtFactor;
    }

    public void setCraisCostWtFactor(CraisCostWtFactor craisCostWtFactor) {
        this.craisCostWtFactor = craisCostWtFactor;
    }

    public CraisStates getCraisStates() {
        return craisStates;
    }

    public void setCraisStates(CraisStates craisStates) {
        this.craisStates = craisStates;
    }

    public CraisProgramming getCraisProgramming() {
        return craisProgramming;
    }

    public void setCraisProgramming(CraisProgramming craisProgramming) {
        this.craisProgramming = craisProgramming;
    }

    public CraisDistricts getCraisDistricts() {
        return craisDistricts;
    }

    public void setCraisDistricts(CraisDistricts craisDistricts) {
        this.craisDistricts = craisDistricts;
    }

    public CraisCosts getCraisCosts() {
        return craisCosts;
    }

    public void setCraisCosts(CraisCosts craisCosts) {
        this.craisCosts = craisCosts;
    }

    public com.orcl.data.CraisActivities getCraisActivitiesByActivityType2() {
        return craisActivitiesByActivityType2;
    }

    public void setCraisActivitiesByActivityType2(com.orcl.data.CraisActivities craisActivitiesByActivityType2) {
        this.craisActivitiesByActivityType2 = craisActivitiesByActivityType2;
    }

}
