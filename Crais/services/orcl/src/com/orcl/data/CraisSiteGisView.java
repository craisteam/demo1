
package com.orcl.data;



/**
 *  orcl.CraisSiteGisView
 *  12/09/2013 10:29:10
 * 
 */
public class CraisSiteGisView {

    private CraisSiteGisViewId id;

    public CraisSiteGisViewId getId() {
        return id;
    }

    public void setId(CraisSiteGisViewId id) {
        this.id = id;
    }

}
