
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisDistricts
 *  12/09/2013 10:29:11
 * 
 */
public class CraisDistricts {

    private CraisDistrictsId id;
    private CraisForests craisForests;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcl.data.CraisIstandardsRecord>();

    public CraisDistrictsId getId() {
        return id;
    }

    public void setId(CraisDistrictsId id) {
        this.id = id;
    }

    public CraisForests getCraisForests() {
        return craisForests;
    }

    public void setCraisForests(CraisForests craisForests) {
        this.craisForests = craisForests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
