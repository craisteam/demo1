
package com.orcl.data;



/**
 *  orcl.CraisIstandardsRecordView
 *  12/09/2013 10:29:11
 * 
 */
public class CraisIstandardsRecordView {

    private CraisIstandardsRecordViewId id;

    public CraisIstandardsRecordViewId getId() {
        return id;
    }

    public void setId(CraisIstandardsRecordViewId id) {
        this.id = id;
    }

}
