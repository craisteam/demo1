
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisDirection
 *  12/09/2013 10:29:10
 * 
 */
public class CraisDirection {

    private String code;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForLandDirection = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForWaterDirection = new HashSet<com.orcl.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForLandDirection() {
        return craisSitefileRecordsForLandDirection;
    }

    public void setCraisSitefileRecordsForLandDirection(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForLandDirection) {
        this.craisSitefileRecordsForLandDirection = craisSitefileRecordsForLandDirection;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForWaterDirection() {
        return craisSitefileRecordsForWaterDirection;
    }

    public void setCraisSitefileRecordsForWaterDirection(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForWaterDirection) {
        this.craisSitefileRecordsForWaterDirection = craisSitefileRecordsForWaterDirection;
    }

}
