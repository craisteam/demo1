
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisForests
 *  12/09/2013 10:29:10
 * 
 */
public class CraisForests {

    private Byte code;
    private String description;
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcl.data.CraisIstandardsRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForForest = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisDistricts> craisDistrictses = new HashSet<com.orcl.data.CraisDistricts>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForReportForest = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecordBad> craisSitefileRecordBads = new HashSet<com.orcl.data.CraisSitefileRecordBad>();

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForForest() {
        return craisSitefileRecordsForForest;
    }

    public void setCraisSitefileRecordsForForest(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForForest) {
        this.craisSitefileRecordsForForest = craisSitefileRecordsForForest;
    }

    public Set<com.orcl.data.CraisDistricts> getCraisDistrictses() {
        return craisDistrictses;
    }

    public void setCraisDistrictses(Set<com.orcl.data.CraisDistricts> craisDistrictses) {
        this.craisDistrictses = craisDistrictses;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForReportForest() {
        return craisSitefileRecordsForReportForest;
    }

    public void setCraisSitefileRecordsForReportForest(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForReportForest) {
        this.craisSitefileRecordsForReportForest = craisSitefileRecordsForReportForest;
    }

    public Set<com.orcl.data.CraisSitefileRecordBad> getCraisSitefileRecordBads() {
        return craisSitefileRecordBads;
    }

    public void setCraisSitefileRecordBads(Set<com.orcl.data.CraisSitefileRecordBad> craisSitefileRecordBads) {
        this.craisSitefileRecordBads = craisSitefileRecordBads;
    }

}
