
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisArtifactsObs
 *  12/09/2013 10:29:10
 * 
 */
public class CraisArtifactsObs {

    private String code;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForShellObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForBoneObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCansObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGlassObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsObserved = new HashSet<com.orcl.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForShellObserved() {
        return craisSitefileRecordsForShellObserved;
    }

    public void setCraisSitefileRecordsForShellObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForShellObserved) {
        this.craisSitefileRecordsForShellObserved = craisSitefileRecordsForShellObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForBoneObserved() {
        return craisSitefileRecordsForBoneObserved;
    }

    public void setCraisSitefileRecordsForBoneObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForBoneObserved) {
        this.craisSitefileRecordsForBoneObserved = craisSitefileRecordsForBoneObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherHistoricObserved() {
        return craisSitefileRecordsForOtherHistoricObserved;
    }

    public void setCraisSitefileRecordsForOtherHistoricObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricObserved) {
        this.craisSitefileRecordsForOtherHistoricObserved = craisSitefileRecordsForOtherHistoricObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForHistCeramicsObserved() {
        return craisSitefileRecordsForHistCeramicsObserved;
    }

    public void setCraisSitefileRecordsForHistCeramicsObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsObserved) {
        this.craisSitefileRecordsForHistCeramicsObserved = craisSitefileRecordsForHistCeramicsObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherPrehistObserved() {
        return craisSitefileRecordsForOtherPrehistObserved;
    }

    public void setCraisSitefileRecordsForOtherPrehistObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistObserved) {
        this.craisSitefileRecordsForOtherPrehistObserved = craisSitefileRecordsForOtherPrehistObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForCansObserved() {
        return craisSitefileRecordsForCansObserved;
    }

    public void setCraisSitefileRecordsForCansObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCansObserved) {
        this.craisSitefileRecordsForCansObserved = craisSitefileRecordsForCansObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForGlassObserved() {
        return craisSitefileRecordsForGlassObserved;
    }

    public void setCraisSitefileRecordsForGlassObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGlassObserved) {
        this.craisSitefileRecordsForGlassObserved = craisSitefileRecordsForGlassObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherMetalObserved() {
        return craisSitefileRecordsForOtherMetalObserved;
    }

    public void setCraisSitefileRecordsForOtherMetalObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalObserved) {
        this.craisSitefileRecordsForOtherMetalObserved = craisSitefileRecordsForOtherMetalObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForFlakedStoneObserved() {
        return craisSitefileRecordsForFlakedStoneObserved;
    }

    public void setCraisSitefileRecordsForFlakedStoneObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneObserved) {
        this.craisSitefileRecordsForFlakedStoneObserved = craisSitefileRecordsForFlakedStoneObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForGroundStoneObserved() {
        return craisSitefileRecordsForGroundStoneObserved;
    }

    public void setCraisSitefileRecordsForGroundStoneObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneObserved) {
        this.craisSitefileRecordsForGroundStoneObserved = craisSitefileRecordsForGroundStoneObserved;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForCeramicsObserved() {
        return craisSitefileRecordsForCeramicsObserved;
    }

    public void setCraisSitefileRecordsForCeramicsObserved(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsObserved) {
        this.craisSitefileRecordsForCeramicsObserved = craisSitefileRecordsForCeramicsObserved;
    }

}
