
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisArtifactsColl
 *  12/09/2013 10:29:10
 * 
 */
public class CraisArtifactsColl {

    private String code;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForSoilSamplesCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForPollenCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForFlotationCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCharcoalCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForVegSamplesCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGlassCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCansCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForShellCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForBoneCollected = new HashSet<com.orcl.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherHistoricCollected() {
        return craisSitefileRecordsForOtherHistoricCollected;
    }

    public void setCraisSitefileRecordsForOtherHistoricCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricCollected) {
        this.craisSitefileRecordsForOtherHistoricCollected = craisSitefileRecordsForOtherHistoricCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForSoilSamplesCollected() {
        return craisSitefileRecordsForSoilSamplesCollected;
    }

    public void setCraisSitefileRecordsForSoilSamplesCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForSoilSamplesCollected) {
        this.craisSitefileRecordsForSoilSamplesCollected = craisSitefileRecordsForSoilSamplesCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForPollenCollected() {
        return craisSitefileRecordsForPollenCollected;
    }

    public void setCraisSitefileRecordsForPollenCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForPollenCollected) {
        this.craisSitefileRecordsForPollenCollected = craisSitefileRecordsForPollenCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForFlotationCollected() {
        return craisSitefileRecordsForFlotationCollected;
    }

    public void setCraisSitefileRecordsForFlotationCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForFlotationCollected) {
        this.craisSitefileRecordsForFlotationCollected = craisSitefileRecordsForFlotationCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForCharcoalCollected() {
        return craisSitefileRecordsForCharcoalCollected;
    }

    public void setCraisSitefileRecordsForCharcoalCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCharcoalCollected) {
        this.craisSitefileRecordsForCharcoalCollected = craisSitefileRecordsForCharcoalCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForVegSamplesCollected() {
        return craisSitefileRecordsForVegSamplesCollected;
    }

    public void setCraisSitefileRecordsForVegSamplesCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForVegSamplesCollected) {
        this.craisSitefileRecordsForVegSamplesCollected = craisSitefileRecordsForVegSamplesCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForGlassCollected() {
        return craisSitefileRecordsForGlassCollected;
    }

    public void setCraisSitefileRecordsForGlassCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGlassCollected) {
        this.craisSitefileRecordsForGlassCollected = craisSitefileRecordsForGlassCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForCansCollected() {
        return craisSitefileRecordsForCansCollected;
    }

    public void setCraisSitefileRecordsForCansCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCansCollected) {
        this.craisSitefileRecordsForCansCollected = craisSitefileRecordsForCansCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherMetalCollected() {
        return craisSitefileRecordsForOtherMetalCollected;
    }

    public void setCraisSitefileRecordsForOtherMetalCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalCollected) {
        this.craisSitefileRecordsForOtherMetalCollected = craisSitefileRecordsForOtherMetalCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForCeramicsCollected() {
        return craisSitefileRecordsForCeramicsCollected;
    }

    public void setCraisSitefileRecordsForCeramicsCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsCollected) {
        this.craisSitefileRecordsForCeramicsCollected = craisSitefileRecordsForCeramicsCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForGroundStoneCollected() {
        return craisSitefileRecordsForGroundStoneCollected;
    }

    public void setCraisSitefileRecordsForGroundStoneCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneCollected) {
        this.craisSitefileRecordsForGroundStoneCollected = craisSitefileRecordsForGroundStoneCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForFlakedStoneCollected() {
        return craisSitefileRecordsForFlakedStoneCollected;
    }

    public void setCraisSitefileRecordsForFlakedStoneCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneCollected) {
        this.craisSitefileRecordsForFlakedStoneCollected = craisSitefileRecordsForFlakedStoneCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForHistCeramicsCollected() {
        return craisSitefileRecordsForHistCeramicsCollected;
    }

    public void setCraisSitefileRecordsForHistCeramicsCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsCollected) {
        this.craisSitefileRecordsForHistCeramicsCollected = craisSitefileRecordsForHistCeramicsCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherPrehistCollected() {
        return craisSitefileRecordsForOtherPrehistCollected;
    }

    public void setCraisSitefileRecordsForOtherPrehistCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistCollected) {
        this.craisSitefileRecordsForOtherPrehistCollected = craisSitefileRecordsForOtherPrehistCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForShellCollected() {
        return craisSitefileRecordsForShellCollected;
    }

    public void setCraisSitefileRecordsForShellCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForShellCollected) {
        this.craisSitefileRecordsForShellCollected = craisSitefileRecordsForShellCollected;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecordsForBoneCollected() {
        return craisSitefileRecordsForBoneCollected;
    }

    public void setCraisSitefileRecordsForBoneCollected(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecordsForBoneCollected) {
        this.craisSitefileRecordsForBoneCollected = craisSitefileRecordsForBoneCollected;
    }

}
