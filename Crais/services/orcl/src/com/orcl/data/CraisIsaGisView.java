
package com.orcl.data;



/**
 *  orcl.CraisIsaGisView
 *  12/09/2013 10:29:10
 * 
 */
public class CraisIsaGisView {

    private CraisIsaGisViewId id;

    public CraisIsaGisViewId getId() {
        return id;
    }

    public void setId(CraisIsaGisViewId id) {
        this.id = id;
    }

}
