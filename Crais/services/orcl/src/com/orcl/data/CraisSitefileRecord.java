
package com.orcl.data;

import java.math.BigDecimal;
import java.util.Date;


/**
 *  orcl.CraisSitefileRecord
 *  12/09/2013 10:29:10
 * 
 */
public class CraisSitefileRecord {

    private CraisSitefileRecordId id;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByOtherHistoricCollected;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByBoneCollected;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByCharcoalCollected;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByOtherMetalObserved;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByGroundStoneObserved;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollBySoilSamplesCollected;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByOtherMetalCollected;
    private CraisWaterType craisWaterType;
    private CraisSiteAspect craisSiteAspect;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByBoneObserved;
    private CraisSiteCondition craisSiteCondition;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByCeramicsObserved;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByCansCollected;
    private CraisSiteUse craisSiteUse;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByCeramicsCollected;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByOtherHistoricObserved;
    private com.orcl.data.CraisDirection craisDirectionByLandDirection;
    private CraisAgLandType craisAgLandType;
    private CraisDistricts craisDistricts;
    private CraisSoilSubgroup craisSoilSubgroup;
    private CraisVegetation craisVegetation;
    private CraisSiteType craisSiteType;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByPollenCollected;
    private CraisCultAffiliation craisCultAffiliation;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByHistCeramicsObserved;
    private CraisCollectionType craisCollectionType;
    private CraisDateBasis craisDateBasis;
    private com.orcl.data.CraisDirection craisDirectionByWaterDirection;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByShellCollected;
    private CraisSoilGroup craisSoilGroup;
    private CraisSiteEvaluation craisSiteEvaluation;
    private com.orcl.data.CraisTopography craisTopographyByTopoS;
    private com.orcl.data.CraisTopography craisTopographyByTopoW;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByVegSamplesCollected;
    private CraisRockArt craisRockArt;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByHistCeramicsCollected;
    private CraisMarkedOnGround craisMarkedOnGround;
    private com.orcl.data.CraisTopography craisTopographyByTopoN;
    private CraisCounties craisCounties;
    private com.orcl.data.CraisTopography craisTopographyByTopoE;
    private com.orcl.data.CraisForests craisForestsByReportForest;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByGroundStoneCollected;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByFlakedStoneObserved;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByOtherPrehistObserved;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByGlassObserved;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByFlakedStoneCollected;
    private com.orcl.data.CraisForests craisForestsByForest;
    private CraisStates craisStates;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByOtherPrehistCollected;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByCansObserved;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByGlassCollected;
    private CraisConstMaterial craisConstMaterial;
    private CraisRecordedBy craisRecordedBy;
    private CraisSiteDescription craisSiteDescription;
    private CraisUtmZone craisUtmZone;
    private com.orcl.data.CraisArtifactsObs craisArtifactsObsByShellObserved;
    private com.orcl.data.CraisArtifactsColl craisArtifactsCollByFlotationCollected;
    private Byte region;
    private String stateSiteNumber;
    private Short reportYear;
    private Short reportNum;
    private Byte inventoryMonth;
    private Byte inventoryDay;
    private Short inventoryYear;
    private Boolean cardOne;
    private String t;
    private BigDecimal township;
    private String NS;
    private String r;
    private BigDecimal range;
    private String EW;
    private Byte sect;
    private Byte northing1;
    private Byte northing2;
    private Short northing3;
    private Boolean easting1;
    private Byte easting2;
    private Short easting3;
    private Integer rimNumber;
    private Short percentDisturbance;
    private String fileCheck;
    private Byte collectionMade;
    private Short hoursOnSite;
    private Boolean cardTwo;
    private Short soilDepth;
    private String landform;
    private Byte averageSlope;
    private Integer elevation;
    private BigDecimal waterDistance;
    private BigDecimal landDistance;
    private Integer siteCoreArea;
    private Integer siteDispersedArea;
    private Boolean cardThree;
    private Integer yearsFrom;
    private Integer yearsTo;
    private Short subsurfaceRooms;
    private Short surfaceRooms;
    private Boolean nonRoomWalls;
    private Boolean partialShelters;
    private Byte middens;
    private Byte hearth;
    private Byte cists;
    private Byte roastingPit;
    private Boolean nonRoofedArea;
    private Short modifiedCave;
    private Boolean depression;
    private Byte waterSoilControl;
    private Boolean nonMiddenMound;
    private Boolean bedrockGrinding;
    private Boolean quarryMine;
    private Short buildings;
    private Byte miscFeatures;
    private Boolean multiComponents;
    private String filler;
    private String cultClassPhase;
    private String createdBy;
    private Date createdDate;
    private BigDecimal createdInInstance;
    private String modifiedBy;
    private Date modifiedDate;
    private BigDecimal modifiedInInstance;

    public CraisSitefileRecordId getId() {
        return id;
    }

    public void setId(CraisSitefileRecordId id) {
        this.id = id;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByOtherHistoricCollected() {
        return craisArtifactsCollByOtherHistoricCollected;
    }

    public void setCraisArtifactsCollByOtherHistoricCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByOtherHistoricCollected) {
        this.craisArtifactsCollByOtherHistoricCollected = craisArtifactsCollByOtherHistoricCollected;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByBoneCollected() {
        return craisArtifactsCollByBoneCollected;
    }

    public void setCraisArtifactsCollByBoneCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByBoneCollected) {
        this.craisArtifactsCollByBoneCollected = craisArtifactsCollByBoneCollected;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByCharcoalCollected() {
        return craisArtifactsCollByCharcoalCollected;
    }

    public void setCraisArtifactsCollByCharcoalCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByCharcoalCollected) {
        this.craisArtifactsCollByCharcoalCollected = craisArtifactsCollByCharcoalCollected;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByOtherMetalObserved() {
        return craisArtifactsObsByOtherMetalObserved;
    }

    public void setCraisArtifactsObsByOtherMetalObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByOtherMetalObserved) {
        this.craisArtifactsObsByOtherMetalObserved = craisArtifactsObsByOtherMetalObserved;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByGroundStoneObserved() {
        return craisArtifactsObsByGroundStoneObserved;
    }

    public void setCraisArtifactsObsByGroundStoneObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByGroundStoneObserved) {
        this.craisArtifactsObsByGroundStoneObserved = craisArtifactsObsByGroundStoneObserved;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollBySoilSamplesCollected() {
        return craisArtifactsCollBySoilSamplesCollected;
    }

    public void setCraisArtifactsCollBySoilSamplesCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollBySoilSamplesCollected) {
        this.craisArtifactsCollBySoilSamplesCollected = craisArtifactsCollBySoilSamplesCollected;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByOtherMetalCollected() {
        return craisArtifactsCollByOtherMetalCollected;
    }

    public void setCraisArtifactsCollByOtherMetalCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByOtherMetalCollected) {
        this.craisArtifactsCollByOtherMetalCollected = craisArtifactsCollByOtherMetalCollected;
    }

    public CraisWaterType getCraisWaterType() {
        return craisWaterType;
    }

    public void setCraisWaterType(CraisWaterType craisWaterType) {
        this.craisWaterType = craisWaterType;
    }

    public CraisSiteAspect getCraisSiteAspect() {
        return craisSiteAspect;
    }

    public void setCraisSiteAspect(CraisSiteAspect craisSiteAspect) {
        this.craisSiteAspect = craisSiteAspect;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByBoneObserved() {
        return craisArtifactsObsByBoneObserved;
    }

    public void setCraisArtifactsObsByBoneObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByBoneObserved) {
        this.craisArtifactsObsByBoneObserved = craisArtifactsObsByBoneObserved;
    }

    public CraisSiteCondition getCraisSiteCondition() {
        return craisSiteCondition;
    }

    public void setCraisSiteCondition(CraisSiteCondition craisSiteCondition) {
        this.craisSiteCondition = craisSiteCondition;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByCeramicsObserved() {
        return craisArtifactsObsByCeramicsObserved;
    }

    public void setCraisArtifactsObsByCeramicsObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByCeramicsObserved) {
        this.craisArtifactsObsByCeramicsObserved = craisArtifactsObsByCeramicsObserved;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByCansCollected() {
        return craisArtifactsCollByCansCollected;
    }

    public void setCraisArtifactsCollByCansCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByCansCollected) {
        this.craisArtifactsCollByCansCollected = craisArtifactsCollByCansCollected;
    }

    public CraisSiteUse getCraisSiteUse() {
        return craisSiteUse;
    }

    public void setCraisSiteUse(CraisSiteUse craisSiteUse) {
        this.craisSiteUse = craisSiteUse;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByCeramicsCollected() {
        return craisArtifactsCollByCeramicsCollected;
    }

    public void setCraisArtifactsCollByCeramicsCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByCeramicsCollected) {
        this.craisArtifactsCollByCeramicsCollected = craisArtifactsCollByCeramicsCollected;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByOtherHistoricObserved() {
        return craisArtifactsObsByOtherHistoricObserved;
    }

    public void setCraisArtifactsObsByOtherHistoricObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByOtherHistoricObserved) {
        this.craisArtifactsObsByOtherHistoricObserved = craisArtifactsObsByOtherHistoricObserved;
    }

    public com.orcl.data.CraisDirection getCraisDirectionByLandDirection() {
        return craisDirectionByLandDirection;
    }

    public void setCraisDirectionByLandDirection(com.orcl.data.CraisDirection craisDirectionByLandDirection) {
        this.craisDirectionByLandDirection = craisDirectionByLandDirection;
    }

    public CraisAgLandType getCraisAgLandType() {
        return craisAgLandType;
    }

    public void setCraisAgLandType(CraisAgLandType craisAgLandType) {
        this.craisAgLandType = craisAgLandType;
    }

    public CraisDistricts getCraisDistricts() {
        return craisDistricts;
    }

    public void setCraisDistricts(CraisDistricts craisDistricts) {
        this.craisDistricts = craisDistricts;
    }

    public CraisSoilSubgroup getCraisSoilSubgroup() {
        return craisSoilSubgroup;
    }

    public void setCraisSoilSubgroup(CraisSoilSubgroup craisSoilSubgroup) {
        this.craisSoilSubgroup = craisSoilSubgroup;
    }

    public CraisVegetation getCraisVegetation() {
        return craisVegetation;
    }

    public void setCraisVegetation(CraisVegetation craisVegetation) {
        this.craisVegetation = craisVegetation;
    }

    public CraisSiteType getCraisSiteType() {
        return craisSiteType;
    }

    public void setCraisSiteType(CraisSiteType craisSiteType) {
        this.craisSiteType = craisSiteType;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByPollenCollected() {
        return craisArtifactsCollByPollenCollected;
    }

    public void setCraisArtifactsCollByPollenCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByPollenCollected) {
        this.craisArtifactsCollByPollenCollected = craisArtifactsCollByPollenCollected;
    }

    public CraisCultAffiliation getCraisCultAffiliation() {
        return craisCultAffiliation;
    }

    public void setCraisCultAffiliation(CraisCultAffiliation craisCultAffiliation) {
        this.craisCultAffiliation = craisCultAffiliation;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByHistCeramicsObserved() {
        return craisArtifactsObsByHistCeramicsObserved;
    }

    public void setCraisArtifactsObsByHistCeramicsObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByHistCeramicsObserved) {
        this.craisArtifactsObsByHistCeramicsObserved = craisArtifactsObsByHistCeramicsObserved;
    }

    public CraisCollectionType getCraisCollectionType() {
        return craisCollectionType;
    }

    public void setCraisCollectionType(CraisCollectionType craisCollectionType) {
        this.craisCollectionType = craisCollectionType;
    }

    public CraisDateBasis getCraisDateBasis() {
        return craisDateBasis;
    }

    public void setCraisDateBasis(CraisDateBasis craisDateBasis) {
        this.craisDateBasis = craisDateBasis;
    }

    public com.orcl.data.CraisDirection getCraisDirectionByWaterDirection() {
        return craisDirectionByWaterDirection;
    }

    public void setCraisDirectionByWaterDirection(com.orcl.data.CraisDirection craisDirectionByWaterDirection) {
        this.craisDirectionByWaterDirection = craisDirectionByWaterDirection;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByShellCollected() {
        return craisArtifactsCollByShellCollected;
    }

    public void setCraisArtifactsCollByShellCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByShellCollected) {
        this.craisArtifactsCollByShellCollected = craisArtifactsCollByShellCollected;
    }

    public CraisSoilGroup getCraisSoilGroup() {
        return craisSoilGroup;
    }

    public void setCraisSoilGroup(CraisSoilGroup craisSoilGroup) {
        this.craisSoilGroup = craisSoilGroup;
    }

    public CraisSiteEvaluation getCraisSiteEvaluation() {
        return craisSiteEvaluation;
    }

    public void setCraisSiteEvaluation(CraisSiteEvaluation craisSiteEvaluation) {
        this.craisSiteEvaluation = craisSiteEvaluation;
    }

    public com.orcl.data.CraisTopography getCraisTopographyByTopoS() {
        return craisTopographyByTopoS;
    }

    public void setCraisTopographyByTopoS(com.orcl.data.CraisTopography craisTopographyByTopoS) {
        this.craisTopographyByTopoS = craisTopographyByTopoS;
    }

    public com.orcl.data.CraisTopography getCraisTopographyByTopoW() {
        return craisTopographyByTopoW;
    }

    public void setCraisTopographyByTopoW(com.orcl.data.CraisTopography craisTopographyByTopoW) {
        this.craisTopographyByTopoW = craisTopographyByTopoW;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByVegSamplesCollected() {
        return craisArtifactsCollByVegSamplesCollected;
    }

    public void setCraisArtifactsCollByVegSamplesCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByVegSamplesCollected) {
        this.craisArtifactsCollByVegSamplesCollected = craisArtifactsCollByVegSamplesCollected;
    }

    public CraisRockArt getCraisRockArt() {
        return craisRockArt;
    }

    public void setCraisRockArt(CraisRockArt craisRockArt) {
        this.craisRockArt = craisRockArt;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByHistCeramicsCollected() {
        return craisArtifactsCollByHistCeramicsCollected;
    }

    public void setCraisArtifactsCollByHistCeramicsCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByHistCeramicsCollected) {
        this.craisArtifactsCollByHistCeramicsCollected = craisArtifactsCollByHistCeramicsCollected;
    }

    public CraisMarkedOnGround getCraisMarkedOnGround() {
        return craisMarkedOnGround;
    }

    public void setCraisMarkedOnGround(CraisMarkedOnGround craisMarkedOnGround) {
        this.craisMarkedOnGround = craisMarkedOnGround;
    }

    public com.orcl.data.CraisTopography getCraisTopographyByTopoN() {
        return craisTopographyByTopoN;
    }

    public void setCraisTopographyByTopoN(com.orcl.data.CraisTopography craisTopographyByTopoN) {
        this.craisTopographyByTopoN = craisTopographyByTopoN;
    }

    public CraisCounties getCraisCounties() {
        return craisCounties;
    }

    public void setCraisCounties(CraisCounties craisCounties) {
        this.craisCounties = craisCounties;
    }

    public com.orcl.data.CraisTopography getCraisTopographyByTopoE() {
        return craisTopographyByTopoE;
    }

    public void setCraisTopographyByTopoE(com.orcl.data.CraisTopography craisTopographyByTopoE) {
        this.craisTopographyByTopoE = craisTopographyByTopoE;
    }

    public com.orcl.data.CraisForests getCraisForestsByReportForest() {
        return craisForestsByReportForest;
    }

    public void setCraisForestsByReportForest(com.orcl.data.CraisForests craisForestsByReportForest) {
        this.craisForestsByReportForest = craisForestsByReportForest;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByGroundStoneCollected() {
        return craisArtifactsCollByGroundStoneCollected;
    }

    public void setCraisArtifactsCollByGroundStoneCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByGroundStoneCollected) {
        this.craisArtifactsCollByGroundStoneCollected = craisArtifactsCollByGroundStoneCollected;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByFlakedStoneObserved() {
        return craisArtifactsObsByFlakedStoneObserved;
    }

    public void setCraisArtifactsObsByFlakedStoneObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByFlakedStoneObserved) {
        this.craisArtifactsObsByFlakedStoneObserved = craisArtifactsObsByFlakedStoneObserved;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByOtherPrehistObserved() {
        return craisArtifactsObsByOtherPrehistObserved;
    }

    public void setCraisArtifactsObsByOtherPrehistObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByOtherPrehistObserved) {
        this.craisArtifactsObsByOtherPrehistObserved = craisArtifactsObsByOtherPrehistObserved;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByGlassObserved() {
        return craisArtifactsObsByGlassObserved;
    }

    public void setCraisArtifactsObsByGlassObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByGlassObserved) {
        this.craisArtifactsObsByGlassObserved = craisArtifactsObsByGlassObserved;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByFlakedStoneCollected() {
        return craisArtifactsCollByFlakedStoneCollected;
    }

    public void setCraisArtifactsCollByFlakedStoneCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByFlakedStoneCollected) {
        this.craisArtifactsCollByFlakedStoneCollected = craisArtifactsCollByFlakedStoneCollected;
    }

    public com.orcl.data.CraisForests getCraisForestsByForest() {
        return craisForestsByForest;
    }

    public void setCraisForestsByForest(com.orcl.data.CraisForests craisForestsByForest) {
        this.craisForestsByForest = craisForestsByForest;
    }

    public CraisStates getCraisStates() {
        return craisStates;
    }

    public void setCraisStates(CraisStates craisStates) {
        this.craisStates = craisStates;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByOtherPrehistCollected() {
        return craisArtifactsCollByOtherPrehistCollected;
    }

    public void setCraisArtifactsCollByOtherPrehistCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByOtherPrehistCollected) {
        this.craisArtifactsCollByOtherPrehistCollected = craisArtifactsCollByOtherPrehistCollected;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByCansObserved() {
        return craisArtifactsObsByCansObserved;
    }

    public void setCraisArtifactsObsByCansObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByCansObserved) {
        this.craisArtifactsObsByCansObserved = craisArtifactsObsByCansObserved;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByGlassCollected() {
        return craisArtifactsCollByGlassCollected;
    }

    public void setCraisArtifactsCollByGlassCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByGlassCollected) {
        this.craisArtifactsCollByGlassCollected = craisArtifactsCollByGlassCollected;
    }

    public CraisConstMaterial getCraisConstMaterial() {
        return craisConstMaterial;
    }

    public void setCraisConstMaterial(CraisConstMaterial craisConstMaterial) {
        this.craisConstMaterial = craisConstMaterial;
    }

    public CraisRecordedBy getCraisRecordedBy() {
        return craisRecordedBy;
    }

    public void setCraisRecordedBy(CraisRecordedBy craisRecordedBy) {
        this.craisRecordedBy = craisRecordedBy;
    }

    public CraisSiteDescription getCraisSiteDescription() {
        return craisSiteDescription;
    }

    public void setCraisSiteDescription(CraisSiteDescription craisSiteDescription) {
        this.craisSiteDescription = craisSiteDescription;
    }

    public CraisUtmZone getCraisUtmZone() {
        return craisUtmZone;
    }

    public void setCraisUtmZone(CraisUtmZone craisUtmZone) {
        this.craisUtmZone = craisUtmZone;
    }

    public com.orcl.data.CraisArtifactsObs getCraisArtifactsObsByShellObserved() {
        return craisArtifactsObsByShellObserved;
    }

    public void setCraisArtifactsObsByShellObserved(com.orcl.data.CraisArtifactsObs craisArtifactsObsByShellObserved) {
        this.craisArtifactsObsByShellObserved = craisArtifactsObsByShellObserved;
    }

    public com.orcl.data.CraisArtifactsColl getCraisArtifactsCollByFlotationCollected() {
        return craisArtifactsCollByFlotationCollected;
    }

    public void setCraisArtifactsCollByFlotationCollected(com.orcl.data.CraisArtifactsColl craisArtifactsCollByFlotationCollected) {
        this.craisArtifactsCollByFlotationCollected = craisArtifactsCollByFlotationCollected;
    }

    public Byte getRegion() {
        return region;
    }

    public void setRegion(Byte region) {
        this.region = region;
    }

    public String getStateSiteNumber() {
        return stateSiteNumber;
    }

    public void setStateSiteNumber(String stateSiteNumber) {
        this.stateSiteNumber = stateSiteNumber;
    }

    public Short getReportYear() {
        return reportYear;
    }

    public void setReportYear(Short reportYear) {
        this.reportYear = reportYear;
    }

    public Short getReportNum() {
        return reportNum;
    }

    public void setReportNum(Short reportNum) {
        this.reportNum = reportNum;
    }

    public Byte getInventoryMonth() {
        return inventoryMonth;
    }

    public void setInventoryMonth(Byte inventoryMonth) {
        this.inventoryMonth = inventoryMonth;
    }

    public Byte getInventoryDay() {
        return inventoryDay;
    }

    public void setInventoryDay(Byte inventoryDay) {
        this.inventoryDay = inventoryDay;
    }

    public Short getInventoryYear() {
        return inventoryYear;
    }

    public void setInventoryYear(Short inventoryYear) {
        this.inventoryYear = inventoryYear;
    }

    public Boolean getCardOne() {
        return cardOne;
    }

    public void setCardOne(Boolean cardOne) {
        this.cardOne = cardOne;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public BigDecimal getTownship() {
        return township;
    }

    public void setTownship(BigDecimal township) {
        this.township = township;
    }

    public String getNS() {
        return NS;
    }

    public void setNS(String NS) {
        this.NS = NS;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public BigDecimal getRange() {
        return range;
    }

    public void setRange(BigDecimal range) {
        this.range = range;
    }

    public String getEW() {
        return EW;
    }

    public void setEW(String EW) {
        this.EW = EW;
    }

    public Byte getSect() {
        return sect;
    }

    public void setSect(Byte sect) {
        this.sect = sect;
    }

    public Byte getNorthing1() {
        return northing1;
    }

    public void setNorthing1(Byte northing1) {
        this.northing1 = northing1;
    }

    public Byte getNorthing2() {
        return northing2;
    }

    public void setNorthing2(Byte northing2) {
        this.northing2 = northing2;
    }

    public Short getNorthing3() {
        return northing3;
    }

    public void setNorthing3(Short northing3) {
        this.northing3 = northing3;
    }

    public Boolean getEasting1() {
        return easting1;
    }

    public void setEasting1(Boolean easting1) {
        this.easting1 = easting1;
    }

    public Byte getEasting2() {
        return easting2;
    }

    public void setEasting2(Byte easting2) {
        this.easting2 = easting2;
    }

    public Short getEasting3() {
        return easting3;
    }

    public void setEasting3(Short easting3) {
        this.easting3 = easting3;
    }

    public Integer getRimNumber() {
        return rimNumber;
    }

    public void setRimNumber(Integer rimNumber) {
        this.rimNumber = rimNumber;
    }

    public Short getPercentDisturbance() {
        return percentDisturbance;
    }

    public void setPercentDisturbance(Short percentDisturbance) {
        this.percentDisturbance = percentDisturbance;
    }

    public String getFileCheck() {
        return fileCheck;
    }

    public void setFileCheck(String fileCheck) {
        this.fileCheck = fileCheck;
    }

    public Byte getCollectionMade() {
        return collectionMade;
    }

    public void setCollectionMade(Byte collectionMade) {
        this.collectionMade = collectionMade;
    }

    public Short getHoursOnSite() {
        return hoursOnSite;
    }

    public void setHoursOnSite(Short hoursOnSite) {
        this.hoursOnSite = hoursOnSite;
    }

    public Boolean getCardTwo() {
        return cardTwo;
    }

    public void setCardTwo(Boolean cardTwo) {
        this.cardTwo = cardTwo;
    }

    public Short getSoilDepth() {
        return soilDepth;
    }

    public void setSoilDepth(Short soilDepth) {
        this.soilDepth = soilDepth;
    }

    public String getLandform() {
        return landform;
    }

    public void setLandform(String landform) {
        this.landform = landform;
    }

    public Byte getAverageSlope() {
        return averageSlope;
    }

    public void setAverageSlope(Byte averageSlope) {
        this.averageSlope = averageSlope;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public BigDecimal getWaterDistance() {
        return waterDistance;
    }

    public void setWaterDistance(BigDecimal waterDistance) {
        this.waterDistance = waterDistance;
    }

    public BigDecimal getLandDistance() {
        return landDistance;
    }

    public void setLandDistance(BigDecimal landDistance) {
        this.landDistance = landDistance;
    }

    public Integer getSiteCoreArea() {
        return siteCoreArea;
    }

    public void setSiteCoreArea(Integer siteCoreArea) {
        this.siteCoreArea = siteCoreArea;
    }

    public Integer getSiteDispersedArea() {
        return siteDispersedArea;
    }

    public void setSiteDispersedArea(Integer siteDispersedArea) {
        this.siteDispersedArea = siteDispersedArea;
    }

    public Boolean getCardThree() {
        return cardThree;
    }

    public void setCardThree(Boolean cardThree) {
        this.cardThree = cardThree;
    }

    public Integer getYearsFrom() {
        return yearsFrom;
    }

    public void setYearsFrom(Integer yearsFrom) {
        this.yearsFrom = yearsFrom;
    }

    public Integer getYearsTo() {
        return yearsTo;
    }

    public void setYearsTo(Integer yearsTo) {
        this.yearsTo = yearsTo;
    }

    public Short getSubsurfaceRooms() {
        return subsurfaceRooms;
    }

    public void setSubsurfaceRooms(Short subsurfaceRooms) {
        this.subsurfaceRooms = subsurfaceRooms;
    }

    public Short getSurfaceRooms() {
        return surfaceRooms;
    }

    public void setSurfaceRooms(Short surfaceRooms) {
        this.surfaceRooms = surfaceRooms;
    }

    public Boolean getNonRoomWalls() {
        return nonRoomWalls;
    }

    public void setNonRoomWalls(Boolean nonRoomWalls) {
        this.nonRoomWalls = nonRoomWalls;
    }

    public Boolean getPartialShelters() {
        return partialShelters;
    }

    public void setPartialShelters(Boolean partialShelters) {
        this.partialShelters = partialShelters;
    }

    public Byte getMiddens() {
        return middens;
    }

    public void setMiddens(Byte middens) {
        this.middens = middens;
    }

    public Byte getHearth() {
        return hearth;
    }

    public void setHearth(Byte hearth) {
        this.hearth = hearth;
    }

    public Byte getCists() {
        return cists;
    }

    public void setCists(Byte cists) {
        this.cists = cists;
    }

    public Byte getRoastingPit() {
        return roastingPit;
    }

    public void setRoastingPit(Byte roastingPit) {
        this.roastingPit = roastingPit;
    }

    public Boolean getNonRoofedArea() {
        return nonRoofedArea;
    }

    public void setNonRoofedArea(Boolean nonRoofedArea) {
        this.nonRoofedArea = nonRoofedArea;
    }

    public Short getModifiedCave() {
        return modifiedCave;
    }

    public void setModifiedCave(Short modifiedCave) {
        this.modifiedCave = modifiedCave;
    }

    public Boolean getDepression() {
        return depression;
    }

    public void setDepression(Boolean depression) {
        this.depression = depression;
    }

    public Byte getWaterSoilControl() {
        return waterSoilControl;
    }

    public void setWaterSoilControl(Byte waterSoilControl) {
        this.waterSoilControl = waterSoilControl;
    }

    public Boolean getNonMiddenMound() {
        return nonMiddenMound;
    }

    public void setNonMiddenMound(Boolean nonMiddenMound) {
        this.nonMiddenMound = nonMiddenMound;
    }

    public Boolean getBedrockGrinding() {
        return bedrockGrinding;
    }

    public void setBedrockGrinding(Boolean bedrockGrinding) {
        this.bedrockGrinding = bedrockGrinding;
    }

    public Boolean getQuarryMine() {
        return quarryMine;
    }

    public void setQuarryMine(Boolean quarryMine) {
        this.quarryMine = quarryMine;
    }

    public Short getBuildings() {
        return buildings;
    }

    public void setBuildings(Short buildings) {
        this.buildings = buildings;
    }

    public Byte getMiscFeatures() {
        return miscFeatures;
    }

    public void setMiscFeatures(Byte miscFeatures) {
        this.miscFeatures = miscFeatures;
    }

    public Boolean getMultiComponents() {
        return multiComponents;
    }

    public void setMultiComponents(Boolean multiComponents) {
        this.multiComponents = multiComponents;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public String getCultClassPhase() {
        return cultClassPhase;
    }

    public void setCultClassPhase(String cultClassPhase) {
        this.cultClassPhase = cultClassPhase;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getCreatedInInstance() {
        return createdInInstance;
    }

    public void setCreatedInInstance(BigDecimal createdInInstance) {
        this.createdInInstance = createdInInstance;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public BigDecimal getModifiedInInstance() {
        return modifiedInInstance;
    }

    public void setModifiedInInstance(BigDecimal modifiedInInstance) {
        this.modifiedInInstance = modifiedInInstance;
    }

}
