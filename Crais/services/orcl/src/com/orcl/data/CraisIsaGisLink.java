
package com.orcl.data;



/**
 *  orcl.CraisIsaGisLink
 *  12/09/2013 10:29:10
 * 
 */
public class CraisIsaGisLink {

    private CraisIsaGisLinkId id;

    public CraisIsaGisLinkId getId() {
        return id;
    }

    public void setId(CraisIsaGisLinkId id) {
        this.id = id;
    }

}
