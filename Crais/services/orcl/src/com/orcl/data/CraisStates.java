
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisStates
 *  12/09/2013 10:29:11
 * 
 */
public class CraisStates {

    private Byte code;
    private String state;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisCounties> craisCountieses = new HashSet<com.orcl.data.CraisCounties>();
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcl.data.CraisIstandardsRecord>();

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcl.data.CraisCounties> getCraisCountieses() {
        return craisCountieses;
    }

    public void setCraisCountieses(Set<com.orcl.data.CraisCounties> craisCountieses) {
        this.craisCountieses = craisCountieses;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
