
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisRockArt
 *  12/09/2013 10:29:10
 * 
 */
public class CraisRockArt {

    private Boolean code;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSitefileRecordBad> craisSitefileRecordBads = new HashSet<com.orcl.data.CraisSitefileRecordBad>();

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcl.data.CraisSitefileRecordBad> getCraisSitefileRecordBads() {
        return craisSitefileRecordBads;
    }

    public void setCraisSitefileRecordBads(Set<com.orcl.data.CraisSitefileRecordBad> craisSitefileRecordBads) {
        this.craisSitefileRecordBads = craisSitefileRecordBads;
    }

}
