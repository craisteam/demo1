
package com.orcl.data;

import java.util.Date;


/**
 *  orcl.CraisUserRoles
 *  12/09/2013 10:29:10
 * 
 */
public class CraisUserRoles {

    private CraisUserRolesId id;
    private CraisUsers craisUsers;
    private CraisRoles craisRoles;
    private String createdBy;
    private Date createdDate;
    private String modifiedBy;
    private Date modifiedDate;

    public CraisUserRolesId getId() {
        return id;
    }

    public void setId(CraisUserRolesId id) {
        this.id = id;
    }

    public CraisUsers getCraisUsers() {
        return craisUsers;
    }

    public void setCraisUsers(CraisUsers craisUsers) {
        this.craisUsers = craisUsers;
    }

    public CraisRoles getCraisRoles() {
        return craisRoles;
    }

    public void setCraisRoles(CraisRoles craisRoles) {
        this.craisRoles = craisRoles;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

}
