
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisDetOfEffect
 *  12/09/2013 10:29:10
 * 
 */
public class CraisDetOfEffect {

    private Boolean code;
    private String description;
    private Set<com.orcl.data.CraisIstandardsRecordBad> craisIstandardsRecordBads = new HashSet<com.orcl.data.CraisIstandardsRecordBad>();
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcl.data.CraisIstandardsRecord>();

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisIstandardsRecordBad> getCraisIstandardsRecordBads() {
        return craisIstandardsRecordBads;
    }

    public void setCraisIstandardsRecordBads(Set<com.orcl.data.CraisIstandardsRecordBad> craisIstandardsRecordBads) {
        this.craisIstandardsRecordBads = craisIstandardsRecordBads;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
