
package com.orcl.data;

import java.io.Serializable;


/**
 *  orcl.OasInfoId
 *  12/09/2013 10:29:10
 * 
 */
public class OasInfoId
    implements Serializable
{

    private String webHost;
    private String reportServerName;
    private String reportPath;
    private String portNum;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof OasInfoId)) {
            return false;
        }
        OasInfoId other = ((OasInfoId) o);
        if (this.webHost == null) {
            if (other.webHost!= null) {
                return false;
            }
        } else {
            if (!this.webHost.equals(other.webHost)) {
                return false;
            }
        }
        if (this.reportServerName == null) {
            if (other.reportServerName!= null) {
                return false;
            }
        } else {
            if (!this.reportServerName.equals(other.reportServerName)) {
                return false;
            }
        }
        if (this.reportPath == null) {
            if (other.reportPath!= null) {
                return false;
            }
        } else {
            if (!this.reportPath.equals(other.reportPath)) {
                return false;
            }
        }
        if (this.portNum == null) {
            if (other.portNum!= null) {
                return false;
            }
        } else {
            if (!this.portNum.equals(other.portNum)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.webHost!= null) {
            rtn = (rtn + this.webHost.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportServerName!= null) {
            rtn = (rtn + this.reportServerName.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportPath!= null) {
            rtn = (rtn + this.reportPath.hashCode());
        }
        rtn = (rtn* 37);
        if (this.portNum!= null) {
            rtn = (rtn + this.portNum.hashCode());
        }
        return rtn;
    }

    public String getWebHost() {
        return webHost;
    }

    public void setWebHost(String webHost) {
        this.webHost = webHost;
    }

    public String getReportServerName() {
        return reportServerName;
    }

    public void setReportServerName(String reportServerName) {
        this.reportServerName = reportServerName;
    }

    public String getReportPath() {
        return reportPath;
    }

    public void setReportPath(String reportPath) {
        this.reportPath = reportPath;
    }

    public String getPortNum() {
        return portNum;
    }

    public void setPortNum(String portNum) {
        this.portNum = portNum;
    }

}
