
package com.orcl.data;



/**
 *  orcl.CraisIstandardsRecordBad
 *  12/09/2013 10:29:10
 * 
 */
public class CraisIstandardsRecordBad {

    private CraisIstandardsRecordBadId id;
    private CraisDetOfEffect craisDetOfEffect;
    private CraisCostWtFactor craisCostWtFactor;

    public CraisIstandardsRecordBadId getId() {
        return id;
    }

    public void setId(CraisIstandardsRecordBadId id) {
        this.id = id;
    }

    public CraisDetOfEffect getCraisDetOfEffect() {
        return craisDetOfEffect;
    }

    public void setCraisDetOfEffect(CraisDetOfEffect craisDetOfEffect) {
        this.craisDetOfEffect = craisDetOfEffect;
    }

    public CraisCostWtFactor getCraisCostWtFactor() {
        return craisCostWtFactor;
    }

    public void setCraisCostWtFactor(CraisCostWtFactor craisCostWtFactor) {
        this.craisCostWtFactor = craisCostWtFactor;
    }

}
