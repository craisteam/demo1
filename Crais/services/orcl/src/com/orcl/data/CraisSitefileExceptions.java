
package com.orcl.data;



/**
 *  orcl.CraisSitefileExceptions
 *  12/09/2013 10:29:11
 * 
 */
public class CraisSitefileExceptions {

    private CraisSitefileExceptionsId id;

    public CraisSitefileExceptionsId getId() {
        return id;
    }

    public void setId(CraisSitefileExceptionsId id) {
        this.id = id;
    }

}
