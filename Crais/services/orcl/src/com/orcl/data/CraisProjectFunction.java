
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisProjectFunction
 *  12/09/2013 10:29:10
 * 
 */
public class CraisProjectFunction {

    private Byte code;
    private String description;
    private Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcl.data.CraisIstandardsRecord>();

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcl.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
