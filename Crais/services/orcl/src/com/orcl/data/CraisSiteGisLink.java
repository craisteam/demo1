
package com.orcl.data;



/**
 *  orcl.CraisSiteGisLink
 *  12/09/2013 10:29:10
 * 
 */
public class CraisSiteGisLink {

    private CraisSiteGisLinkId id;

    public CraisSiteGisLinkId getId() {
        return id;
    }

    public void setId(CraisSiteGisLinkId id) {
        this.id = id;
    }

}
