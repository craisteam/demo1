
package com.orcl.data;



/**
 *  orcl.CraisIstandardsExceptions
 *  12/09/2013 10:29:10
 * 
 */
public class CraisIstandardsExceptions {

    private CraisIstandardsExceptionsId id;

    public CraisIstandardsExceptionsId getId() {
        return id;
    }

    public void setId(CraisIstandardsExceptionsId id) {
        this.id = id;
    }

}
