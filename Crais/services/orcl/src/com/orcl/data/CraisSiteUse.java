
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisSiteUse
 *  12/09/2013 10:29:10
 * 
 */
public class CraisSiteUse {

    private String code;
    private String description;
    private Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcl.data.CraisSitefileRecord>();
    private Set<com.orcl.data.CraisSiteType> craisSiteTypes = new HashSet<com.orcl.data.CraisSiteType>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcl.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcl.data.CraisSiteType> getCraisSiteTypes() {
        return craisSiteTypes;
    }

    public void setCraisSiteTypes(Set<com.orcl.data.CraisSiteType> craisSiteTypes) {
        this.craisSiteTypes = craisSiteTypes;
    }

}
