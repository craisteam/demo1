
package com.orcl.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orcl.CraisRoles
 *  12/09/2013 10:29:10
 * 
 */
public class CraisRoles {

    private String roleName;
    private String description;
    private Set<com.orcl.data.CraisUserRoles> craisUserRoleses = new HashSet<com.orcl.data.CraisUserRoles>();

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcl.data.CraisUserRoles> getCraisUserRoleses() {
        return craisUserRoleses;
    }

    public void setCraisUserRoleses(Set<com.orcl.data.CraisUserRoles> craisUserRoleses) {
        this.craisUserRoleses = craisUserRoleses;
    }

}
