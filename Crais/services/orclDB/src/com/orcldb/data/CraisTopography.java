
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisTopography
 *  12/08/2013 22:45:20
 * 
 */
public class CraisTopography {

    private String code;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoW = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoN = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoE = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoS = new HashSet<com.orcldb.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoW() {
        return craisSitefileRecordsForTopoW;
    }

    public void setCraisSitefileRecordsForTopoW(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoW) {
        this.craisSitefileRecordsForTopoW = craisSitefileRecordsForTopoW;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoN() {
        return craisSitefileRecordsForTopoN;
    }

    public void setCraisSitefileRecordsForTopoN(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoN) {
        this.craisSitefileRecordsForTopoN = craisSitefileRecordsForTopoN;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoE() {
        return craisSitefileRecordsForTopoE;
    }

    public void setCraisSitefileRecordsForTopoE(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoE) {
        this.craisSitefileRecordsForTopoE = craisSitefileRecordsForTopoE;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForTopoS() {
        return craisSitefileRecordsForTopoS;
    }

    public void setCraisSitefileRecordsForTopoS(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForTopoS) {
        this.craisSitefileRecordsForTopoS = craisSitefileRecordsForTopoS;
    }

}
