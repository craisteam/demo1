
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisRoles
 *  12/08/2013 22:45:19
 * 
 */
public class CraisRoles {

    private String roleName;
    private String description;
    private Set<com.orcldb.data.CraisUserRoles> craisUserRoleses = new HashSet<com.orcldb.data.CraisUserRoles>();

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisUserRoles> getCraisUserRoleses() {
        return craisUserRoleses;
    }

    public void setCraisUserRoleses(Set<com.orcldb.data.CraisUserRoles> craisUserRoleses) {
        this.craisUserRoleses = craisUserRoleses;
    }

}
