
package com.orcldb.data;



/**
 *  orclDB.CraisIsaGisView
 *  12/08/2013 22:45:19
 * 
 */
public class CraisIsaGisView {

    private CraisIsaGisViewId id;

    public CraisIsaGisViewId getId() {
        return id;
    }

    public void setId(CraisIsaGisViewId id) {
        this.id = id;
    }

}
