
package com.orcldb.data;

import java.io.Serializable;
import java.util.Date;


/**
 *  orclDB.CraisSitefileExceptionsId
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSitefileExceptionsId
    implements Serializable
{

    private Date rowId;
    private String owner;
    private String tableName;
    private String constraint;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisSitefileExceptionsId)) {
            return false;
        }
        CraisSitefileExceptionsId other = ((CraisSitefileExceptionsId) o);
        if (this.rowId == null) {
            if (other.rowId!= null) {
                return false;
            }
        } else {
            if (!this.rowId.equals(other.rowId)) {
                return false;
            }
        }
        if (this.owner == null) {
            if (other.owner!= null) {
                return false;
            }
        } else {
            if (!this.owner.equals(other.owner)) {
                return false;
            }
        }
        if (this.tableName == null) {
            if (other.tableName!= null) {
                return false;
            }
        } else {
            if (!this.tableName.equals(other.tableName)) {
                return false;
            }
        }
        if (this.constraint == null) {
            if (other.constraint!= null) {
                return false;
            }
        } else {
            if (!this.constraint.equals(other.constraint)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.rowId!= null) {
            rtn = (rtn + this.rowId.hashCode());
        }
        rtn = (rtn* 37);
        if (this.owner!= null) {
            rtn = (rtn + this.owner.hashCode());
        }
        rtn = (rtn* 37);
        if (this.tableName!= null) {
            rtn = (rtn + this.tableName.hashCode());
        }
        rtn = (rtn* 37);
        if (this.constraint!= null) {
            rtn = (rtn + this.constraint.hashCode());
        }
        return rtn;
    }

    public Date getRowId() {
        return rowId;
    }

    public void setRowId(Date rowId) {
        this.rowId = rowId;
    }

    public String getOwner() {
        return owner;
    }

    public void setOwner(String owner) {
        this.owner = owner;
    }

    public String getTableName() {
        return tableName;
    }

    public void setTableName(String tableName) {
        this.tableName = tableName;
    }

    public String getConstraint() {
        return constraint;
    }

    public void setConstraint(String constraint) {
        this.constraint = constraint;
    }

}
