
package com.orcldb.data;

import java.math.BigDecimal;
import java.util.Date;


/**
 *  orclDB.CraisSitefileRecord
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSitefileRecord {

    private CraisSitefileRecordId id;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByOtherHistoricCollected;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByBoneCollected;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByCharcoalCollected;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByOtherMetalObserved;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByGroundStoneObserved;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollBySoilSamplesCollected;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByOtherMetalCollected;
    private CraisWaterType craisWaterType;
    private CraisSiteAspect craisSiteAspect;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByBoneObserved;
    private CraisSiteCondition craisSiteCondition;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByCeramicsObserved;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByCansCollected;
    private CraisSiteUse craisSiteUse;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByCeramicsCollected;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByOtherHistoricObserved;
    private com.orcldb.data.CraisDirection craisDirectionByLandDirection;
    private CraisAgLandType craisAgLandType;
    private CraisDistricts craisDistricts;
    private CraisSoilSubgroup craisSoilSubgroup;
    private CraisVegetation craisVegetation;
    private CraisSiteType craisSiteType;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByPollenCollected;
    private CraisCultAffiliation craisCultAffiliation;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByHistCeramicsObserved;
    private CraisCollectionType craisCollectionType;
    private CraisDateBasis craisDateBasis;
    private com.orcldb.data.CraisDirection craisDirectionByWaterDirection;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByShellCollected;
    private CraisSoilGroup craisSoilGroup;
    private CraisSiteEvaluation craisSiteEvaluation;
    private com.orcldb.data.CraisTopography craisTopographyByTopoS;
    private com.orcldb.data.CraisTopography craisTopographyByTopoW;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByVegSamplesCollected;
    private CraisRockArt craisRockArt;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByHistCeramicsCollected;
    private CraisMarkedOnGround craisMarkedOnGround;
    private com.orcldb.data.CraisTopography craisTopographyByTopoN;
    private CraisCounties craisCounties;
    private com.orcldb.data.CraisTopography craisTopographyByTopoE;
    private com.orcldb.data.CraisForests craisForestsByReportForest;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByGroundStoneCollected;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByFlakedStoneObserved;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByOtherPrehistObserved;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByGlassObserved;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByFlakedStoneCollected;
    private com.orcldb.data.CraisForests craisForestsByForest;
    private CraisStates craisStates;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByOtherPrehistCollected;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByCansObserved;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByGlassCollected;
    private CraisConstMaterial craisConstMaterial;
    private CraisRecordedBy craisRecordedBy;
    private CraisSiteDescription craisSiteDescription;
    private CraisUtmZone craisUtmZone;
    private com.orcldb.data.CraisArtifactsObs craisArtifactsObsByShellObserved;
    private com.orcldb.data.CraisArtifactsColl craisArtifactsCollByFlotationCollected;
    private Byte region;
    private String stateSiteNumber;
    private Short reportYear;
    private Short reportNum;
    private Byte inventoryMonth;
    private Byte inventoryDay;
    private Short inventoryYear;
    private Boolean cardOne;
    private String t;
    private BigDecimal township;
    private String NS;
    private String r;
    private BigDecimal range;
    private String EW;
    private Byte sect;
    private Byte northing1;
    private Byte northing2;
    private Short northing3;
    private Boolean easting1;
    private Byte easting2;
    private Short easting3;
    private Integer rimNumber;
    private Short percentDisturbance;
    private String fileCheck;
    private Byte collectionMade;
    private Short hoursOnSite;
    private Boolean cardTwo;
    private Short soilDepth;
    private String landform;
    private Byte averageSlope;
    private Integer elevation;
    private BigDecimal waterDistance;
    private BigDecimal landDistance;
    private Integer siteCoreArea;
    private Integer siteDispersedArea;
    private Boolean cardThree;
    private Integer yearsFrom;
    private Integer yearsTo;
    private Short subsurfaceRooms;
    private Short surfaceRooms;
    private Boolean nonRoomWalls;
    private Boolean partialShelters;
    private Byte middens;
    private Byte hearth;
    private Byte cists;
    private Byte roastingPit;
    private Boolean nonRoofedArea;
    private Short modifiedCave;
    private Boolean depression;
    private Byte waterSoilControl;
    private Boolean nonMiddenMound;
    private Boolean bedrockGrinding;
    private Boolean quarryMine;
    private Short buildings;
    private Byte miscFeatures;
    private Boolean multiComponents;
    private String filler;
    private String cultClassPhase;
    private String createdBy;
    private Date createdDate;
    private BigDecimal createdInInstance;
    private String modifiedBy;
    private Date modifiedDate;
    private BigDecimal modifiedInInstance;

    public CraisSitefileRecordId getId() {
        return id;
    }

    public void setId(CraisSitefileRecordId id) {
        this.id = id;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByOtherHistoricCollected() {
        return craisArtifactsCollByOtherHistoricCollected;
    }

    public void setCraisArtifactsCollByOtherHistoricCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByOtherHistoricCollected) {
        this.craisArtifactsCollByOtherHistoricCollected = craisArtifactsCollByOtherHistoricCollected;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByBoneCollected() {
        return craisArtifactsCollByBoneCollected;
    }

    public void setCraisArtifactsCollByBoneCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByBoneCollected) {
        this.craisArtifactsCollByBoneCollected = craisArtifactsCollByBoneCollected;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByCharcoalCollected() {
        return craisArtifactsCollByCharcoalCollected;
    }

    public void setCraisArtifactsCollByCharcoalCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByCharcoalCollected) {
        this.craisArtifactsCollByCharcoalCollected = craisArtifactsCollByCharcoalCollected;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByOtherMetalObserved() {
        return craisArtifactsObsByOtherMetalObserved;
    }

    public void setCraisArtifactsObsByOtherMetalObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByOtherMetalObserved) {
        this.craisArtifactsObsByOtherMetalObserved = craisArtifactsObsByOtherMetalObserved;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByGroundStoneObserved() {
        return craisArtifactsObsByGroundStoneObserved;
    }

    public void setCraisArtifactsObsByGroundStoneObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByGroundStoneObserved) {
        this.craisArtifactsObsByGroundStoneObserved = craisArtifactsObsByGroundStoneObserved;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollBySoilSamplesCollected() {
        return craisArtifactsCollBySoilSamplesCollected;
    }

    public void setCraisArtifactsCollBySoilSamplesCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollBySoilSamplesCollected) {
        this.craisArtifactsCollBySoilSamplesCollected = craisArtifactsCollBySoilSamplesCollected;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByOtherMetalCollected() {
        return craisArtifactsCollByOtherMetalCollected;
    }

    public void setCraisArtifactsCollByOtherMetalCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByOtherMetalCollected) {
        this.craisArtifactsCollByOtherMetalCollected = craisArtifactsCollByOtherMetalCollected;
    }

    public CraisWaterType getCraisWaterType() {
        return craisWaterType;
    }

    public void setCraisWaterType(CraisWaterType craisWaterType) {
        this.craisWaterType = craisWaterType;
    }

    public CraisSiteAspect getCraisSiteAspect() {
        return craisSiteAspect;
    }

    public void setCraisSiteAspect(CraisSiteAspect craisSiteAspect) {
        this.craisSiteAspect = craisSiteAspect;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByBoneObserved() {
        return craisArtifactsObsByBoneObserved;
    }

    public void setCraisArtifactsObsByBoneObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByBoneObserved) {
        this.craisArtifactsObsByBoneObserved = craisArtifactsObsByBoneObserved;
    }

    public CraisSiteCondition getCraisSiteCondition() {
        return craisSiteCondition;
    }

    public void setCraisSiteCondition(CraisSiteCondition craisSiteCondition) {
        this.craisSiteCondition = craisSiteCondition;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByCeramicsObserved() {
        return craisArtifactsObsByCeramicsObserved;
    }

    public void setCraisArtifactsObsByCeramicsObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByCeramicsObserved) {
        this.craisArtifactsObsByCeramicsObserved = craisArtifactsObsByCeramicsObserved;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByCansCollected() {
        return craisArtifactsCollByCansCollected;
    }

    public void setCraisArtifactsCollByCansCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByCansCollected) {
        this.craisArtifactsCollByCansCollected = craisArtifactsCollByCansCollected;
    }

    public CraisSiteUse getCraisSiteUse() {
        return craisSiteUse;
    }

    public void setCraisSiteUse(CraisSiteUse craisSiteUse) {
        this.craisSiteUse = craisSiteUse;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByCeramicsCollected() {
        return craisArtifactsCollByCeramicsCollected;
    }

    public void setCraisArtifactsCollByCeramicsCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByCeramicsCollected) {
        this.craisArtifactsCollByCeramicsCollected = craisArtifactsCollByCeramicsCollected;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByOtherHistoricObserved() {
        return craisArtifactsObsByOtherHistoricObserved;
    }

    public void setCraisArtifactsObsByOtherHistoricObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByOtherHistoricObserved) {
        this.craisArtifactsObsByOtherHistoricObserved = craisArtifactsObsByOtherHistoricObserved;
    }

    public com.orcldb.data.CraisDirection getCraisDirectionByLandDirection() {
        return craisDirectionByLandDirection;
    }

    public void setCraisDirectionByLandDirection(com.orcldb.data.CraisDirection craisDirectionByLandDirection) {
        this.craisDirectionByLandDirection = craisDirectionByLandDirection;
    }

    public CraisAgLandType getCraisAgLandType() {
        return craisAgLandType;
    }

    public void setCraisAgLandType(CraisAgLandType craisAgLandType) {
        this.craisAgLandType = craisAgLandType;
    }

    public CraisDistricts getCraisDistricts() {
        return craisDistricts;
    }

    public void setCraisDistricts(CraisDistricts craisDistricts) {
        this.craisDistricts = craisDistricts;
    }

    public CraisSoilSubgroup getCraisSoilSubgroup() {
        return craisSoilSubgroup;
    }

    public void setCraisSoilSubgroup(CraisSoilSubgroup craisSoilSubgroup) {
        this.craisSoilSubgroup = craisSoilSubgroup;
    }

    public CraisVegetation getCraisVegetation() {
        return craisVegetation;
    }

    public void setCraisVegetation(CraisVegetation craisVegetation) {
        this.craisVegetation = craisVegetation;
    }

    public CraisSiteType getCraisSiteType() {
        return craisSiteType;
    }

    public void setCraisSiteType(CraisSiteType craisSiteType) {
        this.craisSiteType = craisSiteType;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByPollenCollected() {
        return craisArtifactsCollByPollenCollected;
    }

    public void setCraisArtifactsCollByPollenCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByPollenCollected) {
        this.craisArtifactsCollByPollenCollected = craisArtifactsCollByPollenCollected;
    }

    public CraisCultAffiliation getCraisCultAffiliation() {
        return craisCultAffiliation;
    }

    public void setCraisCultAffiliation(CraisCultAffiliation craisCultAffiliation) {
        this.craisCultAffiliation = craisCultAffiliation;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByHistCeramicsObserved() {
        return craisArtifactsObsByHistCeramicsObserved;
    }

    public void setCraisArtifactsObsByHistCeramicsObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByHistCeramicsObserved) {
        this.craisArtifactsObsByHistCeramicsObserved = craisArtifactsObsByHistCeramicsObserved;
    }

    public CraisCollectionType getCraisCollectionType() {
        return craisCollectionType;
    }

    public void setCraisCollectionType(CraisCollectionType craisCollectionType) {
        this.craisCollectionType = craisCollectionType;
    }

    public CraisDateBasis getCraisDateBasis() {
        return craisDateBasis;
    }

    public void setCraisDateBasis(CraisDateBasis craisDateBasis) {
        this.craisDateBasis = craisDateBasis;
    }

    public com.orcldb.data.CraisDirection getCraisDirectionByWaterDirection() {
        return craisDirectionByWaterDirection;
    }

    public void setCraisDirectionByWaterDirection(com.orcldb.data.CraisDirection craisDirectionByWaterDirection) {
        this.craisDirectionByWaterDirection = craisDirectionByWaterDirection;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByShellCollected() {
        return craisArtifactsCollByShellCollected;
    }

    public void setCraisArtifactsCollByShellCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByShellCollected) {
        this.craisArtifactsCollByShellCollected = craisArtifactsCollByShellCollected;
    }

    public CraisSoilGroup getCraisSoilGroup() {
        return craisSoilGroup;
    }

    public void setCraisSoilGroup(CraisSoilGroup craisSoilGroup) {
        this.craisSoilGroup = craisSoilGroup;
    }

    public CraisSiteEvaluation getCraisSiteEvaluation() {
        return craisSiteEvaluation;
    }

    public void setCraisSiteEvaluation(CraisSiteEvaluation craisSiteEvaluation) {
        this.craisSiteEvaluation = craisSiteEvaluation;
    }

    public com.orcldb.data.CraisTopography getCraisTopographyByTopoS() {
        return craisTopographyByTopoS;
    }

    public void setCraisTopographyByTopoS(com.orcldb.data.CraisTopography craisTopographyByTopoS) {
        this.craisTopographyByTopoS = craisTopographyByTopoS;
    }

    public com.orcldb.data.CraisTopography getCraisTopographyByTopoW() {
        return craisTopographyByTopoW;
    }

    public void setCraisTopographyByTopoW(com.orcldb.data.CraisTopography craisTopographyByTopoW) {
        this.craisTopographyByTopoW = craisTopographyByTopoW;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByVegSamplesCollected() {
        return craisArtifactsCollByVegSamplesCollected;
    }

    public void setCraisArtifactsCollByVegSamplesCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByVegSamplesCollected) {
        this.craisArtifactsCollByVegSamplesCollected = craisArtifactsCollByVegSamplesCollected;
    }

    public CraisRockArt getCraisRockArt() {
        return craisRockArt;
    }

    public void setCraisRockArt(CraisRockArt craisRockArt) {
        this.craisRockArt = craisRockArt;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByHistCeramicsCollected() {
        return craisArtifactsCollByHistCeramicsCollected;
    }

    public void setCraisArtifactsCollByHistCeramicsCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByHistCeramicsCollected) {
        this.craisArtifactsCollByHistCeramicsCollected = craisArtifactsCollByHistCeramicsCollected;
    }

    public CraisMarkedOnGround getCraisMarkedOnGround() {
        return craisMarkedOnGround;
    }

    public void setCraisMarkedOnGround(CraisMarkedOnGround craisMarkedOnGround) {
        this.craisMarkedOnGround = craisMarkedOnGround;
    }

    public com.orcldb.data.CraisTopography getCraisTopographyByTopoN() {
        return craisTopographyByTopoN;
    }

    public void setCraisTopographyByTopoN(com.orcldb.data.CraisTopography craisTopographyByTopoN) {
        this.craisTopographyByTopoN = craisTopographyByTopoN;
    }

    public CraisCounties getCraisCounties() {
        return craisCounties;
    }

    public void setCraisCounties(CraisCounties craisCounties) {
        this.craisCounties = craisCounties;
    }

    public com.orcldb.data.CraisTopography getCraisTopographyByTopoE() {
        return craisTopographyByTopoE;
    }

    public void setCraisTopographyByTopoE(com.orcldb.data.CraisTopography craisTopographyByTopoE) {
        this.craisTopographyByTopoE = craisTopographyByTopoE;
    }

    public com.orcldb.data.CraisForests getCraisForestsByReportForest() {
        return craisForestsByReportForest;
    }

    public void setCraisForestsByReportForest(com.orcldb.data.CraisForests craisForestsByReportForest) {
        this.craisForestsByReportForest = craisForestsByReportForest;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByGroundStoneCollected() {
        return craisArtifactsCollByGroundStoneCollected;
    }

    public void setCraisArtifactsCollByGroundStoneCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByGroundStoneCollected) {
        this.craisArtifactsCollByGroundStoneCollected = craisArtifactsCollByGroundStoneCollected;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByFlakedStoneObserved() {
        return craisArtifactsObsByFlakedStoneObserved;
    }

    public void setCraisArtifactsObsByFlakedStoneObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByFlakedStoneObserved) {
        this.craisArtifactsObsByFlakedStoneObserved = craisArtifactsObsByFlakedStoneObserved;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByOtherPrehistObserved() {
        return craisArtifactsObsByOtherPrehistObserved;
    }

    public void setCraisArtifactsObsByOtherPrehistObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByOtherPrehistObserved) {
        this.craisArtifactsObsByOtherPrehistObserved = craisArtifactsObsByOtherPrehistObserved;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByGlassObserved() {
        return craisArtifactsObsByGlassObserved;
    }

    public void setCraisArtifactsObsByGlassObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByGlassObserved) {
        this.craisArtifactsObsByGlassObserved = craisArtifactsObsByGlassObserved;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByFlakedStoneCollected() {
        return craisArtifactsCollByFlakedStoneCollected;
    }

    public void setCraisArtifactsCollByFlakedStoneCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByFlakedStoneCollected) {
        this.craisArtifactsCollByFlakedStoneCollected = craisArtifactsCollByFlakedStoneCollected;
    }

    public com.orcldb.data.CraisForests getCraisForestsByForest() {
        return craisForestsByForest;
    }

    public void setCraisForestsByForest(com.orcldb.data.CraisForests craisForestsByForest) {
        this.craisForestsByForest = craisForestsByForest;
    }

    public CraisStates getCraisStates() {
        return craisStates;
    }

    public void setCraisStates(CraisStates craisStates) {
        this.craisStates = craisStates;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByOtherPrehistCollected() {
        return craisArtifactsCollByOtherPrehistCollected;
    }

    public void setCraisArtifactsCollByOtherPrehistCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByOtherPrehistCollected) {
        this.craisArtifactsCollByOtherPrehistCollected = craisArtifactsCollByOtherPrehistCollected;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByCansObserved() {
        return craisArtifactsObsByCansObserved;
    }

    public void setCraisArtifactsObsByCansObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByCansObserved) {
        this.craisArtifactsObsByCansObserved = craisArtifactsObsByCansObserved;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByGlassCollected() {
        return craisArtifactsCollByGlassCollected;
    }

    public void setCraisArtifactsCollByGlassCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByGlassCollected) {
        this.craisArtifactsCollByGlassCollected = craisArtifactsCollByGlassCollected;
    }

    public CraisConstMaterial getCraisConstMaterial() {
        return craisConstMaterial;
    }

    public void setCraisConstMaterial(CraisConstMaterial craisConstMaterial) {
        this.craisConstMaterial = craisConstMaterial;
    }

    public CraisRecordedBy getCraisRecordedBy() {
        return craisRecordedBy;
    }

    public void setCraisRecordedBy(CraisRecordedBy craisRecordedBy) {
        this.craisRecordedBy = craisRecordedBy;
    }

    public CraisSiteDescription getCraisSiteDescription() {
        return craisSiteDescription;
    }

    public void setCraisSiteDescription(CraisSiteDescription craisSiteDescription) {
        this.craisSiteDescription = craisSiteDescription;
    }

    public CraisUtmZone getCraisUtmZone() {
        return craisUtmZone;
    }

    public void setCraisUtmZone(CraisUtmZone craisUtmZone) {
        this.craisUtmZone = craisUtmZone;
    }

    public com.orcldb.data.CraisArtifactsObs getCraisArtifactsObsByShellObserved() {
        return craisArtifactsObsByShellObserved;
    }

    public void setCraisArtifactsObsByShellObserved(com.orcldb.data.CraisArtifactsObs craisArtifactsObsByShellObserved) {
        this.craisArtifactsObsByShellObserved = craisArtifactsObsByShellObserved;
    }

    public com.orcldb.data.CraisArtifactsColl getCraisArtifactsCollByFlotationCollected() {
        return craisArtifactsCollByFlotationCollected;
    }

    public void setCraisArtifactsCollByFlotationCollected(com.orcldb.data.CraisArtifactsColl craisArtifactsCollByFlotationCollected) {
        this.craisArtifactsCollByFlotationCollected = craisArtifactsCollByFlotationCollected;
    }

    public Byte getRegion() {
        return region;
    }

    public void setRegion(Byte region) {
        this.region = region;
    }

    public String getStateSiteNumber() {
        return stateSiteNumber;
    }

    public void setStateSiteNumber(String stateSiteNumber) {
        this.stateSiteNumber = stateSiteNumber;
    }

    public Short getReportYear() {
        return reportYear;
    }

    public void setReportYear(Short reportYear) {
        this.reportYear = reportYear;
    }

    public Short getReportNum() {
        return reportNum;
    }

    public void setReportNum(Short reportNum) {
        this.reportNum = reportNum;
    }

    public Byte getInventoryMonth() {
        return inventoryMonth;
    }

    public void setInventoryMonth(Byte inventoryMonth) {
        this.inventoryMonth = inventoryMonth;
    }

    public Byte getInventoryDay() {
        return inventoryDay;
    }

    public void setInventoryDay(Byte inventoryDay) {
        this.inventoryDay = inventoryDay;
    }

    public Short getInventoryYear() {
        return inventoryYear;
    }

    public void setInventoryYear(Short inventoryYear) {
        this.inventoryYear = inventoryYear;
    }

    public Boolean getCardOne() {
        return cardOne;
    }

    public void setCardOne(Boolean cardOne) {
        this.cardOne = cardOne;
    }

    public String getT() {
        return t;
    }

    public void setT(String t) {
        this.t = t;
    }

    public BigDecimal getTownship() {
        return township;
    }

    public void setTownship(BigDecimal township) {
        this.township = township;
    }

    public String getNS() {
        return NS;
    }

    public void setNS(String NS) {
        this.NS = NS;
    }

    public String getR() {
        return r;
    }

    public void setR(String r) {
        this.r = r;
    }

    public BigDecimal getRange() {
        return range;
    }

    public void setRange(BigDecimal range) {
        this.range = range;
    }

    public String getEW() {
        return EW;
    }

    public void setEW(String EW) {
        this.EW = EW;
    }

    public Byte getSect() {
        return sect;
    }

    public void setSect(Byte sect) {
        this.sect = sect;
    }

    public Byte getNorthing1() {
        return northing1;
    }

    public void setNorthing1(Byte northing1) {
        this.northing1 = northing1;
    }

    public Byte getNorthing2() {
        return northing2;
    }

    public void setNorthing2(Byte northing2) {
        this.northing2 = northing2;
    }

    public Short getNorthing3() {
        return northing3;
    }

    public void setNorthing3(Short northing3) {
        this.northing3 = northing3;
    }

    public Boolean getEasting1() {
        return easting1;
    }

    public void setEasting1(Boolean easting1) {
        this.easting1 = easting1;
    }

    public Byte getEasting2() {
        return easting2;
    }

    public void setEasting2(Byte easting2) {
        this.easting2 = easting2;
    }

    public Short getEasting3() {
        return easting3;
    }

    public void setEasting3(Short easting3) {
        this.easting3 = easting3;
    }

    public Integer getRimNumber() {
        return rimNumber;
    }

    public void setRimNumber(Integer rimNumber) {
        this.rimNumber = rimNumber;
    }

    public Short getPercentDisturbance() {
        return percentDisturbance;
    }

    public void setPercentDisturbance(Short percentDisturbance) {
        this.percentDisturbance = percentDisturbance;
    }

    public String getFileCheck() {
        return fileCheck;
    }

    public void setFileCheck(String fileCheck) {
        this.fileCheck = fileCheck;
    }

    public Byte getCollectionMade() {
        return collectionMade;
    }

    public void setCollectionMade(Byte collectionMade) {
        this.collectionMade = collectionMade;
    }

    public Short getHoursOnSite() {
        return hoursOnSite;
    }

    public void setHoursOnSite(Short hoursOnSite) {
        this.hoursOnSite = hoursOnSite;
    }

    public Boolean getCardTwo() {
        return cardTwo;
    }

    public void setCardTwo(Boolean cardTwo) {
        this.cardTwo = cardTwo;
    }

    public Short getSoilDepth() {
        return soilDepth;
    }

    public void setSoilDepth(Short soilDepth) {
        this.soilDepth = soilDepth;
    }

    public String getLandform() {
        return landform;
    }

    public void setLandform(String landform) {
        this.landform = landform;
    }

    public Byte getAverageSlope() {
        return averageSlope;
    }

    public void setAverageSlope(Byte averageSlope) {
        this.averageSlope = averageSlope;
    }

    public Integer getElevation() {
        return elevation;
    }

    public void setElevation(Integer elevation) {
        this.elevation = elevation;
    }

    public BigDecimal getWaterDistance() {
        return waterDistance;
    }

    public void setWaterDistance(BigDecimal waterDistance) {
        this.waterDistance = waterDistance;
    }

    public BigDecimal getLandDistance() {
        return landDistance;
    }

    public void setLandDistance(BigDecimal landDistance) {
        this.landDistance = landDistance;
    }

    public Integer getSiteCoreArea() {
        return siteCoreArea;
    }

    public void setSiteCoreArea(Integer siteCoreArea) {
        this.siteCoreArea = siteCoreArea;
    }

    public Integer getSiteDispersedArea() {
        return siteDispersedArea;
    }

    public void setSiteDispersedArea(Integer siteDispersedArea) {
        this.siteDispersedArea = siteDispersedArea;
    }

    public Boolean getCardThree() {
        return cardThree;
    }

    public void setCardThree(Boolean cardThree) {
        this.cardThree = cardThree;
    }

    public Integer getYearsFrom() {
        return yearsFrom;
    }

    public void setYearsFrom(Integer yearsFrom) {
        this.yearsFrom = yearsFrom;
    }

    public Integer getYearsTo() {
        return yearsTo;
    }

    public void setYearsTo(Integer yearsTo) {
        this.yearsTo = yearsTo;
    }

    public Short getSubsurfaceRooms() {
        return subsurfaceRooms;
    }

    public void setSubsurfaceRooms(Short subsurfaceRooms) {
        this.subsurfaceRooms = subsurfaceRooms;
    }

    public Short getSurfaceRooms() {
        return surfaceRooms;
    }

    public void setSurfaceRooms(Short surfaceRooms) {
        this.surfaceRooms = surfaceRooms;
    }

    public Boolean getNonRoomWalls() {
        return nonRoomWalls;
    }

    public void setNonRoomWalls(Boolean nonRoomWalls) {
        this.nonRoomWalls = nonRoomWalls;
    }

    public Boolean getPartialShelters() {
        return partialShelters;
    }

    public void setPartialShelters(Boolean partialShelters) {
        this.partialShelters = partialShelters;
    }

    public Byte getMiddens() {
        return middens;
    }

    public void setMiddens(Byte middens) {
        this.middens = middens;
    }

    public Byte getHearth() {
        return hearth;
    }

    public void setHearth(Byte hearth) {
        this.hearth = hearth;
    }

    public Byte getCists() {
        return cists;
    }

    public void setCists(Byte cists) {
        this.cists = cists;
    }

    public Byte getRoastingPit() {
        return roastingPit;
    }

    public void setRoastingPit(Byte roastingPit) {
        this.roastingPit = roastingPit;
    }

    public Boolean getNonRoofedArea() {
        return nonRoofedArea;
    }

    public void setNonRoofedArea(Boolean nonRoofedArea) {
        this.nonRoofedArea = nonRoofedArea;
    }

    public Short getModifiedCave() {
        return modifiedCave;
    }

    public void setModifiedCave(Short modifiedCave) {
        this.modifiedCave = modifiedCave;
    }

    public Boolean getDepression() {
        return depression;
    }

    public void setDepression(Boolean depression) {
        this.depression = depression;
    }

    public Byte getWaterSoilControl() {
        return waterSoilControl;
    }

    public void setWaterSoilControl(Byte waterSoilControl) {
        this.waterSoilControl = waterSoilControl;
    }

    public Boolean getNonMiddenMound() {
        return nonMiddenMound;
    }

    public void setNonMiddenMound(Boolean nonMiddenMound) {
        this.nonMiddenMound = nonMiddenMound;
    }

    public Boolean getBedrockGrinding() {
        return bedrockGrinding;
    }

    public void setBedrockGrinding(Boolean bedrockGrinding) {
        this.bedrockGrinding = bedrockGrinding;
    }

    public Boolean getQuarryMine() {
        return quarryMine;
    }

    public void setQuarryMine(Boolean quarryMine) {
        this.quarryMine = quarryMine;
    }

    public Short getBuildings() {
        return buildings;
    }

    public void setBuildings(Short buildings) {
        this.buildings = buildings;
    }

    public Byte getMiscFeatures() {
        return miscFeatures;
    }

    public void setMiscFeatures(Byte miscFeatures) {
        this.miscFeatures = miscFeatures;
    }

    public Boolean getMultiComponents() {
        return multiComponents;
    }

    public void setMultiComponents(Boolean multiComponents) {
        this.multiComponents = multiComponents;
    }

    public String getFiller() {
        return filler;
    }

    public void setFiller(String filler) {
        this.filler = filler;
    }

    public String getCultClassPhase() {
        return cultClassPhase;
    }

    public void setCultClassPhase(String cultClassPhase) {
        this.cultClassPhase = cultClassPhase;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getCreatedInInstance() {
        return createdInInstance;
    }

    public void setCreatedInInstance(BigDecimal createdInInstance) {
        this.createdInInstance = createdInInstance;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public BigDecimal getModifiedInInstance() {
        return modifiedInInstance;
    }

    public void setModifiedInInstance(BigDecimal modifiedInInstance) {
        this.modifiedInInstance = modifiedInInstance;
    }

}
