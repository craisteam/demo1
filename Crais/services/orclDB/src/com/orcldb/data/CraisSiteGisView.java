
package com.orcldb.data;



/**
 *  orclDB.CraisSiteGisView
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSiteGisView {

    private CraisSiteGisViewId id;

    public CraisSiteGisViewId getId() {
        return id;
    }

    public void setId(CraisSiteGisViewId id) {
        this.id = id;
    }

}
