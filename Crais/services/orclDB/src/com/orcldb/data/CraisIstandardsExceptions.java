
package com.orcldb.data;



/**
 *  orclDB.CraisIstandardsExceptions
 *  12/08/2013 22:45:19
 * 
 */
public class CraisIstandardsExceptions {

    private CraisIstandardsExceptionsId id;

    public CraisIstandardsExceptionsId getId() {
        return id;
    }

    public void setId(CraisIstandardsExceptionsId id) {
        this.id = id;
    }

}
