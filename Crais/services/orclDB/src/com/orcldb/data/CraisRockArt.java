
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisRockArt
 *  12/08/2013 22:45:19
 * 
 */
public class CraisRockArt {

    private Boolean code;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecordBad> craisSitefileRecordBads = new HashSet<com.orcldb.data.CraisSitefileRecordBad>();

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcldb.data.CraisSitefileRecordBad> getCraisSitefileRecordBads() {
        return craisSitefileRecordBads;
    }

    public void setCraisSitefileRecordBads(Set<com.orcldb.data.CraisSitefileRecordBad> craisSitefileRecordBads) {
        this.craisSitefileRecordBads = craisSitefileRecordBads;
    }

}
