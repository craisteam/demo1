
package com.orcldb.data;

import java.io.Serializable;


/**
 *  orclDB.CraisSitefileRecordBadId
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSitefileRecordBadId
    implements Serializable
{

    private Byte forest;
    private Byte district;
    private Integer siteNumber;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisSitefileRecordBadId)) {
            return false;
        }
        CraisSitefileRecordBadId other = ((CraisSitefileRecordBadId) o);
        if (this.forest == null) {
            if (other.forest!= null) {
                return false;
            }
        } else {
            if (!this.forest.equals(other.forest)) {
                return false;
            }
        }
        if (this.district == null) {
            if (other.district!= null) {
                return false;
            }
        } else {
            if (!this.district.equals(other.district)) {
                return false;
            }
        }
        if (this.siteNumber == null) {
            if (other.siteNumber!= null) {
                return false;
            }
        } else {
            if (!this.siteNumber.equals(other.siteNumber)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.forest!= null) {
            rtn = (rtn + this.forest.hashCode());
        }
        rtn = (rtn* 37);
        if (this.district!= null) {
            rtn = (rtn + this.district.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteNumber!= null) {
            rtn = (rtn + this.siteNumber.hashCode());
        }
        return rtn;
    }

    public Byte getForest() {
        return forest;
    }

    public void setForest(Byte forest) {
        this.forest = forest;
    }

    public Byte getDistrict() {
        return district;
    }

    public void setDistrict(Byte district) {
        this.district = district;
    }

    public Integer getSiteNumber() {
        return siteNumber;
    }

    public void setSiteNumber(Integer siteNumber) {
        this.siteNumber = siteNumber;
    }

}
