
package com.orcldb.data;

import java.io.Serializable;


/**
 *  orclDB.CraisUserRolesId
 *  12/08/2013 22:45:19
 * 
 */
public class CraisUserRolesId
    implements Serializable
{

    private String username;
    private String roleName;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisUserRolesId)) {
            return false;
        }
        CraisUserRolesId other = ((CraisUserRolesId) o);
        if (this.username == null) {
            if (other.username!= null) {
                return false;
            }
        } else {
            if (!this.username.equals(other.username)) {
                return false;
            }
        }
        if (this.roleName == null) {
            if (other.roleName!= null) {
                return false;
            }
        } else {
            if (!this.roleName.equals(other.roleName)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.username!= null) {
            rtn = (rtn + this.username.hashCode());
        }
        rtn = (rtn* 37);
        if (this.roleName!= null) {
            rtn = (rtn + this.roleName.hashCode());
        }
        return rtn;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getRoleName() {
        return roleName;
    }

    public void setRoleName(String roleName) {
        this.roleName = roleName;
    }

}
