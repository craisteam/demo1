
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisProjectFunction
 *  12/08/2013 22:45:19
 * 
 */
public class CraisProjectFunction {

    private Byte code;
    private String description;
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcldb.data.CraisIstandardsRecord>();

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
