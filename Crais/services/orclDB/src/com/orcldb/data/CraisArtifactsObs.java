
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisArtifactsObs
 *  12/08/2013 22:45:19
 * 
 */
public class CraisArtifactsObs {

    private String code;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForShellObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForBoneObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCansObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGlassObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsObserved = new HashSet<com.orcldb.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForShellObserved() {
        return craisSitefileRecordsForShellObserved;
    }

    public void setCraisSitefileRecordsForShellObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForShellObserved) {
        this.craisSitefileRecordsForShellObserved = craisSitefileRecordsForShellObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForBoneObserved() {
        return craisSitefileRecordsForBoneObserved;
    }

    public void setCraisSitefileRecordsForBoneObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForBoneObserved) {
        this.craisSitefileRecordsForBoneObserved = craisSitefileRecordsForBoneObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherHistoricObserved() {
        return craisSitefileRecordsForOtherHistoricObserved;
    }

    public void setCraisSitefileRecordsForOtherHistoricObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricObserved) {
        this.craisSitefileRecordsForOtherHistoricObserved = craisSitefileRecordsForOtherHistoricObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForHistCeramicsObserved() {
        return craisSitefileRecordsForHistCeramicsObserved;
    }

    public void setCraisSitefileRecordsForHistCeramicsObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsObserved) {
        this.craisSitefileRecordsForHistCeramicsObserved = craisSitefileRecordsForHistCeramicsObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherPrehistObserved() {
        return craisSitefileRecordsForOtherPrehistObserved;
    }

    public void setCraisSitefileRecordsForOtherPrehistObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistObserved) {
        this.craisSitefileRecordsForOtherPrehistObserved = craisSitefileRecordsForOtherPrehistObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForCansObserved() {
        return craisSitefileRecordsForCansObserved;
    }

    public void setCraisSitefileRecordsForCansObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCansObserved) {
        this.craisSitefileRecordsForCansObserved = craisSitefileRecordsForCansObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForGlassObserved() {
        return craisSitefileRecordsForGlassObserved;
    }

    public void setCraisSitefileRecordsForGlassObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGlassObserved) {
        this.craisSitefileRecordsForGlassObserved = craisSitefileRecordsForGlassObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherMetalObserved() {
        return craisSitefileRecordsForOtherMetalObserved;
    }

    public void setCraisSitefileRecordsForOtherMetalObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalObserved) {
        this.craisSitefileRecordsForOtherMetalObserved = craisSitefileRecordsForOtherMetalObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForFlakedStoneObserved() {
        return craisSitefileRecordsForFlakedStoneObserved;
    }

    public void setCraisSitefileRecordsForFlakedStoneObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneObserved) {
        this.craisSitefileRecordsForFlakedStoneObserved = craisSitefileRecordsForFlakedStoneObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForGroundStoneObserved() {
        return craisSitefileRecordsForGroundStoneObserved;
    }

    public void setCraisSitefileRecordsForGroundStoneObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneObserved) {
        this.craisSitefileRecordsForGroundStoneObserved = craisSitefileRecordsForGroundStoneObserved;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForCeramicsObserved() {
        return craisSitefileRecordsForCeramicsObserved;
    }

    public void setCraisSitefileRecordsForCeramicsObserved(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsObserved) {
        this.craisSitefileRecordsForCeramicsObserved = craisSitefileRecordsForCeramicsObserved;
    }

}
