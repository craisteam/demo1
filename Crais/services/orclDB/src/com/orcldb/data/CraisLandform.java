
package com.orcldb.data;



/**
 *  orclDB.CraisLandform
 *  12/08/2013 22:45:19
 * 
 */
public class CraisLandform {

    private String code;
    private String description;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

}
