
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisUsers
 *  12/08/2013 22:45:19
 * 
 */
public class CraisUsers {

    private String username;
    private String lname;
    private String fname;
    private String MInit;
    private Set<com.orcldb.data.CraisUserRoles> craisUserRoleses = new HashSet<com.orcldb.data.CraisUserRoles>();

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getLname() {
        return lname;
    }

    public void setLname(String lname) {
        this.lname = lname;
    }

    public String getFname() {
        return fname;
    }

    public void setFname(String fname) {
        this.fname = fname;
    }

    public String getMInit() {
        return MInit;
    }

    public void setMInit(String MInit) {
        this.MInit = MInit;
    }

    public Set<com.orcldb.data.CraisUserRoles> getCraisUserRoleses() {
        return craisUserRoleses;
    }

    public void setCraisUserRoleses(Set<com.orcldb.data.CraisUserRoles> craisUserRoleses) {
        this.craisUserRoleses = craisUserRoleses;
    }

}
