
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisDistricts
 *  12/08/2013 22:45:19
 * 
 */
public class CraisDistricts {

    private CraisDistrictsId id;
    private CraisForests craisForests;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcldb.data.CraisIstandardsRecord>();

    public CraisDistrictsId getId() {
        return id;
    }

    public void setId(CraisDistrictsId id) {
        this.id = id;
    }

    public CraisForests getCraisForests() {
        return craisForests;
    }

    public void setCraisForests(CraisForests craisForests) {
        this.craisForests = craisForests;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
