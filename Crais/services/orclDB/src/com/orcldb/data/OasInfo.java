
package com.orcldb.data;



/**
 *  orclDB.OasInfo
 *  12/08/2013 22:45:19
 * 
 */
public class OasInfo {

    private OasInfoId id;

    public OasInfoId getId() {
        return id;
    }

    public void setId(OasInfoId id) {
        this.id = id;
    }

}
