
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisSiteUse
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSiteUse {

    private String code;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSiteType> craisSiteTypes = new HashSet<com.orcldb.data.CraisSiteType>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcldb.data.CraisSiteType> getCraisSiteTypes() {
        return craisSiteTypes;
    }

    public void setCraisSiteTypes(Set<com.orcldb.data.CraisSiteType> craisSiteTypes) {
        this.craisSiteTypes = craisSiteTypes;
    }

}
