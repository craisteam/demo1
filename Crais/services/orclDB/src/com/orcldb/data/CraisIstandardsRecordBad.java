
package com.orcldb.data;



/**
 *  orclDB.CraisIstandardsRecordBad
 *  12/08/2013 22:45:19
 * 
 */
public class CraisIstandardsRecordBad {

    private CraisIstandardsRecordBadId id;
    private CraisDetOfEffect craisDetOfEffect;
    private CraisCostWtFactor craisCostWtFactor;

    public CraisIstandardsRecordBadId getId() {
        return id;
    }

    public void setId(CraisIstandardsRecordBadId id) {
        this.id = id;
    }

    public CraisDetOfEffect getCraisDetOfEffect() {
        return craisDetOfEffect;
    }

    public void setCraisDetOfEffect(CraisDetOfEffect craisDetOfEffect) {
        this.craisDetOfEffect = craisDetOfEffect;
    }

    public CraisCostWtFactor getCraisCostWtFactor() {
        return craisCostWtFactor;
    }

    public void setCraisCostWtFactor(CraisCostWtFactor craisCostWtFactor) {
        this.craisCostWtFactor = craisCostWtFactor;
    }

}
