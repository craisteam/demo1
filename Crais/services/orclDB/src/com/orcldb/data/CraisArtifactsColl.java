
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisArtifactsColl
 *  12/08/2013 22:45:20
 * 
 */
public class CraisArtifactsColl {

    private String code;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForSoilSamplesCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForPollenCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForFlotationCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCharcoalCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForVegSamplesCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGlassCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCansCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForShellCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForBoneCollected = new HashSet<com.orcldb.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherHistoricCollected() {
        return craisSitefileRecordsForOtherHistoricCollected;
    }

    public void setCraisSitefileRecordsForOtherHistoricCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherHistoricCollected) {
        this.craisSitefileRecordsForOtherHistoricCollected = craisSitefileRecordsForOtherHistoricCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForSoilSamplesCollected() {
        return craisSitefileRecordsForSoilSamplesCollected;
    }

    public void setCraisSitefileRecordsForSoilSamplesCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForSoilSamplesCollected) {
        this.craisSitefileRecordsForSoilSamplesCollected = craisSitefileRecordsForSoilSamplesCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForPollenCollected() {
        return craisSitefileRecordsForPollenCollected;
    }

    public void setCraisSitefileRecordsForPollenCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForPollenCollected) {
        this.craisSitefileRecordsForPollenCollected = craisSitefileRecordsForPollenCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForFlotationCollected() {
        return craisSitefileRecordsForFlotationCollected;
    }

    public void setCraisSitefileRecordsForFlotationCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForFlotationCollected) {
        this.craisSitefileRecordsForFlotationCollected = craisSitefileRecordsForFlotationCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForCharcoalCollected() {
        return craisSitefileRecordsForCharcoalCollected;
    }

    public void setCraisSitefileRecordsForCharcoalCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCharcoalCollected) {
        this.craisSitefileRecordsForCharcoalCollected = craisSitefileRecordsForCharcoalCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForVegSamplesCollected() {
        return craisSitefileRecordsForVegSamplesCollected;
    }

    public void setCraisSitefileRecordsForVegSamplesCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForVegSamplesCollected) {
        this.craisSitefileRecordsForVegSamplesCollected = craisSitefileRecordsForVegSamplesCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForGlassCollected() {
        return craisSitefileRecordsForGlassCollected;
    }

    public void setCraisSitefileRecordsForGlassCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGlassCollected) {
        this.craisSitefileRecordsForGlassCollected = craisSitefileRecordsForGlassCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForCansCollected() {
        return craisSitefileRecordsForCansCollected;
    }

    public void setCraisSitefileRecordsForCansCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCansCollected) {
        this.craisSitefileRecordsForCansCollected = craisSitefileRecordsForCansCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherMetalCollected() {
        return craisSitefileRecordsForOtherMetalCollected;
    }

    public void setCraisSitefileRecordsForOtherMetalCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherMetalCollected) {
        this.craisSitefileRecordsForOtherMetalCollected = craisSitefileRecordsForOtherMetalCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForCeramicsCollected() {
        return craisSitefileRecordsForCeramicsCollected;
    }

    public void setCraisSitefileRecordsForCeramicsCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForCeramicsCollected) {
        this.craisSitefileRecordsForCeramicsCollected = craisSitefileRecordsForCeramicsCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForGroundStoneCollected() {
        return craisSitefileRecordsForGroundStoneCollected;
    }

    public void setCraisSitefileRecordsForGroundStoneCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForGroundStoneCollected) {
        this.craisSitefileRecordsForGroundStoneCollected = craisSitefileRecordsForGroundStoneCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForFlakedStoneCollected() {
        return craisSitefileRecordsForFlakedStoneCollected;
    }

    public void setCraisSitefileRecordsForFlakedStoneCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForFlakedStoneCollected) {
        this.craisSitefileRecordsForFlakedStoneCollected = craisSitefileRecordsForFlakedStoneCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForHistCeramicsCollected() {
        return craisSitefileRecordsForHistCeramicsCollected;
    }

    public void setCraisSitefileRecordsForHistCeramicsCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForHistCeramicsCollected) {
        this.craisSitefileRecordsForHistCeramicsCollected = craisSitefileRecordsForHistCeramicsCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForOtherPrehistCollected() {
        return craisSitefileRecordsForOtherPrehistCollected;
    }

    public void setCraisSitefileRecordsForOtherPrehistCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForOtherPrehistCollected) {
        this.craisSitefileRecordsForOtherPrehistCollected = craisSitefileRecordsForOtherPrehistCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForShellCollected() {
        return craisSitefileRecordsForShellCollected;
    }

    public void setCraisSitefileRecordsForShellCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForShellCollected) {
        this.craisSitefileRecordsForShellCollected = craisSitefileRecordsForShellCollected;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForBoneCollected() {
        return craisSitefileRecordsForBoneCollected;
    }

    public void setCraisSitefileRecordsForBoneCollected(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForBoneCollected) {
        this.craisSitefileRecordsForBoneCollected = craisSitefileRecordsForBoneCollected;
    }

}
