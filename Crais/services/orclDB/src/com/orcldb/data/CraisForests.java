
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisForests
 *  12/08/2013 22:45:19
 * 
 */
public class CraisForests {

    private Byte code;
    private String description;
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcldb.data.CraisIstandardsRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForForest = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisDistricts> craisDistrictses = new HashSet<com.orcldb.data.CraisDistricts>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForReportForest = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecordBad> craisSitefileRecordBads = new HashSet<com.orcldb.data.CraisSitefileRecordBad>();

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForForest() {
        return craisSitefileRecordsForForest;
    }

    public void setCraisSitefileRecordsForForest(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForForest) {
        this.craisSitefileRecordsForForest = craisSitefileRecordsForForest;
    }

    public Set<com.orcldb.data.CraisDistricts> getCraisDistrictses() {
        return craisDistrictses;
    }

    public void setCraisDistrictses(Set<com.orcldb.data.CraisDistricts> craisDistrictses) {
        this.craisDistrictses = craisDistrictses;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForReportForest() {
        return craisSitefileRecordsForReportForest;
    }

    public void setCraisSitefileRecordsForReportForest(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForReportForest) {
        this.craisSitefileRecordsForReportForest = craisSitefileRecordsForReportForest;
    }

    public Set<com.orcldb.data.CraisSitefileRecordBad> getCraisSitefileRecordBads() {
        return craisSitefileRecordBads;
    }

    public void setCraisSitefileRecordBads(Set<com.orcldb.data.CraisSitefileRecordBad> craisSitefileRecordBads) {
        this.craisSitefileRecordBads = craisSitefileRecordBads;
    }

}
