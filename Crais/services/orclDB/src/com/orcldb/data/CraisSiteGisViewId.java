
package com.orcldb.data;

import java.io.Serializable;
import java.util.Date;


/**
 *  orclDB.CraisSiteGisViewId
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSiteGisViewId
    implements Serializable
{

    private Date fsSiteNum;
    private Date region;
    private Date forest;
    private Date district;
    private Date districtDesc;
    private Date siteNumber;
    private Date stateSiteNumber;
    private Date reportYear;
    private Date reportForest;
    private Date reportNum;
    private Date inventoryMonth;
    private Date inventoryDay;
    private Date inventoryYear;
    private Date recordedBy;
    private Date recordedByDesc;
    private Date t;
    private Date township;
    private Date NS;
    private Date r;
    private Date range;
    private Date EW;
    private Date zone;
    private Date northing1;
    private Date northing2;
    private Date northing3;
    private Date easting1;
    private Date easting2;
    private Date easting3;
    private Date evaluation;
    private Date evaluationDesc;
    private Date condition;
    private Date conditionDesc;
    private Date percentDisturbance;
    private Date siteMarking;
    private Date siteMarkingDesc;
    private Date vegetationCode;
    private Date vegetationCodeDesc;
    private Date landform;
    private Date landformDesc;
    private Date siteAspect;
    private Date averageSlope;
    private Date elevation;
    private Date siteClass;
    private Date siteClassDesc;
    private Date siteUse;
    private Date siteUseDesc;
    private Date siteType;
    private Date siteTypeDesc;
    private Date siteCoreArea;
    private Date siteDispersedArea;
    private Date collectionMade;
    private Date collectionType;
    private Date collectionTypeDesc;
    private Date flakedStoneObserved;
    private Date flakedStoneObservedDesc;
    private Date groundStoneObserved;
    private Date groundStoneObservedDesc;
    private Date ceramicsObserved;
    private Date ceramicsObservedDesc;
    private Date boneObserved;
    private Date boneObservedDesc;
    private Date shellObserved;
    private Date shellObservedDesc;
    private Date otherPrehistObserved;
    private Date otherPrehistObservedDesc;
    private Date histCeramicsObserved;
    private Date histCeramicsObservedDesc;
    private Date glassObserved;
    private Date glassObservedDesc;
    private Date cansObserved;
    private Date cansObservedDesc;
    private Date otherMetalObserved;
    private Date otherMetalObservedDesc;
    private Date otherHistoricObserved;
    private Date otherHistoricObservedDesc;
    private Date constructionMaterial;
    private Date constructionMaterialDesc;
    private Date yearsFrom;
    private Date yearsTo;
    private Date dateBasedOn;
    private Date dateBasedOnDesc;
    private Date subsurfaceRooms;
    private Date surfaceRooms;
    private Date nonRoomWalls;
    private Date partialShelters;
    private Date middens;
    private Date hearth;
    private Date cists;
    private Date roastingPit;
    private Date nonRoofedArea;
    private Date modifiedCave;
    private Date depression;
    private Date waterSoilControl;
    private Date nonMiddenMound;
    private Date bedrockGrinding;
    private Date quarryMine;
    private Date buildings;
    private Date rockArt;
    private Date rockArtDesc;
    private Date miscFeatures;
    private Date multiComponents;
    private Date cultClass;
    private Date cultClassDesc;
    private Date cultClassPhase;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisSiteGisViewId)) {
            return false;
        }
        CraisSiteGisViewId other = ((CraisSiteGisViewId) o);
        if (this.fsSiteNum == null) {
            if (other.fsSiteNum!= null) {
                return false;
            }
        } else {
            if (!this.fsSiteNum.equals(other.fsSiteNum)) {
                return false;
            }
        }
        if (this.region == null) {
            if (other.region!= null) {
                return false;
            }
        } else {
            if (!this.region.equals(other.region)) {
                return false;
            }
        }
        if (this.forest == null) {
            if (other.forest!= null) {
                return false;
            }
        } else {
            if (!this.forest.equals(other.forest)) {
                return false;
            }
        }
        if (this.district == null) {
            if (other.district!= null) {
                return false;
            }
        } else {
            if (!this.district.equals(other.district)) {
                return false;
            }
        }
        if (this.districtDesc == null) {
            if (other.districtDesc!= null) {
                return false;
            }
        } else {
            if (!this.districtDesc.equals(other.districtDesc)) {
                return false;
            }
        }
        if (this.siteNumber == null) {
            if (other.siteNumber!= null) {
                return false;
            }
        } else {
            if (!this.siteNumber.equals(other.siteNumber)) {
                return false;
            }
        }
        if (this.stateSiteNumber == null) {
            if (other.stateSiteNumber!= null) {
                return false;
            }
        } else {
            if (!this.stateSiteNumber.equals(other.stateSiteNumber)) {
                return false;
            }
        }
        if (this.reportYear == null) {
            if (other.reportYear!= null) {
                return false;
            }
        } else {
            if (!this.reportYear.equals(other.reportYear)) {
                return false;
            }
        }
        if (this.reportForest == null) {
            if (other.reportForest!= null) {
                return false;
            }
        } else {
            if (!this.reportForest.equals(other.reportForest)) {
                return false;
            }
        }
        if (this.reportNum == null) {
            if (other.reportNum!= null) {
                return false;
            }
        } else {
            if (!this.reportNum.equals(other.reportNum)) {
                return false;
            }
        }
        if (this.inventoryMonth == null) {
            if (other.inventoryMonth!= null) {
                return false;
            }
        } else {
            if (!this.inventoryMonth.equals(other.inventoryMonth)) {
                return false;
            }
        }
        if (this.inventoryDay == null) {
            if (other.inventoryDay!= null) {
                return false;
            }
        } else {
            if (!this.inventoryDay.equals(other.inventoryDay)) {
                return false;
            }
        }
        if (this.inventoryYear == null) {
            if (other.inventoryYear!= null) {
                return false;
            }
        } else {
            if (!this.inventoryYear.equals(other.inventoryYear)) {
                return false;
            }
        }
        if (this.recordedBy == null) {
            if (other.recordedBy!= null) {
                return false;
            }
        } else {
            if (!this.recordedBy.equals(other.recordedBy)) {
                return false;
            }
        }
        if (this.recordedByDesc == null) {
            if (other.recordedByDesc!= null) {
                return false;
            }
        } else {
            if (!this.recordedByDesc.equals(other.recordedByDesc)) {
                return false;
            }
        }
        if (this.t == null) {
            if (other.t!= null) {
                return false;
            }
        } else {
            if (!this.t.equals(other.t)) {
                return false;
            }
        }
        if (this.township == null) {
            if (other.township!= null) {
                return false;
            }
        } else {
            if (!this.township.equals(other.township)) {
                return false;
            }
        }
        if (this.NS == null) {
            if (other.NS!= null) {
                return false;
            }
        } else {
            if (!this.NS.equals(other.NS)) {
                return false;
            }
        }
        if (this.r == null) {
            if (other.r!= null) {
                return false;
            }
        } else {
            if (!this.r.equals(other.r)) {
                return false;
            }
        }
        if (this.range == null) {
            if (other.range!= null) {
                return false;
            }
        } else {
            if (!this.range.equals(other.range)) {
                return false;
            }
        }
        if (this.EW == null) {
            if (other.EW!= null) {
                return false;
            }
        } else {
            if (!this.EW.equals(other.EW)) {
                return false;
            }
        }
        if (this.zone == null) {
            if (other.zone!= null) {
                return false;
            }
        } else {
            if (!this.zone.equals(other.zone)) {
                return false;
            }
        }
        if (this.northing1 == null) {
            if (other.northing1 != null) {
                return false;
            }
        } else {
            if (!this.northing1 .equals(other.northing1)) {
                return false;
            }
        }
        if (this.northing2 == null) {
            if (other.northing2 != null) {
                return false;
            }
        } else {
            if (!this.northing2 .equals(other.northing2)) {
                return false;
            }
        }
        if (this.northing3 == null) {
            if (other.northing3 != null) {
                return false;
            }
        } else {
            if (!this.northing3 .equals(other.northing3)) {
                return false;
            }
        }
        if (this.easting1 == null) {
            if (other.easting1 != null) {
                return false;
            }
        } else {
            if (!this.easting1 .equals(other.easting1)) {
                return false;
            }
        }
        if (this.easting2 == null) {
            if (other.easting2 != null) {
                return false;
            }
        } else {
            if (!this.easting2 .equals(other.easting2)) {
                return false;
            }
        }
        if (this.easting3 == null) {
            if (other.easting3 != null) {
                return false;
            }
        } else {
            if (!this.easting3 .equals(other.easting3)) {
                return false;
            }
        }
        if (this.evaluation == null) {
            if (other.evaluation!= null) {
                return false;
            }
        } else {
            if (!this.evaluation.equals(other.evaluation)) {
                return false;
            }
        }
        if (this.evaluationDesc == null) {
            if (other.evaluationDesc!= null) {
                return false;
            }
        } else {
            if (!this.evaluationDesc.equals(other.evaluationDesc)) {
                return false;
            }
        }
        if (this.condition == null) {
            if (other.condition!= null) {
                return false;
            }
        } else {
            if (!this.condition.equals(other.condition)) {
                return false;
            }
        }
        if (this.conditionDesc == null) {
            if (other.conditionDesc!= null) {
                return false;
            }
        } else {
            if (!this.conditionDesc.equals(other.conditionDesc)) {
                return false;
            }
        }
        if (this.percentDisturbance == null) {
            if (other.percentDisturbance!= null) {
                return false;
            }
        } else {
            if (!this.percentDisturbance.equals(other.percentDisturbance)) {
                return false;
            }
        }
        if (this.siteMarking == null) {
            if (other.siteMarking!= null) {
                return false;
            }
        } else {
            if (!this.siteMarking.equals(other.siteMarking)) {
                return false;
            }
        }
        if (this.siteMarkingDesc == null) {
            if (other.siteMarkingDesc!= null) {
                return false;
            }
        } else {
            if (!this.siteMarkingDesc.equals(other.siteMarkingDesc)) {
                return false;
            }
        }
        if (this.vegetationCode == null) {
            if (other.vegetationCode!= null) {
                return false;
            }
        } else {
            if (!this.vegetationCode.equals(other.vegetationCode)) {
                return false;
            }
        }
        if (this.vegetationCodeDesc == null) {
            if (other.vegetationCodeDesc!= null) {
                return false;
            }
        } else {
            if (!this.vegetationCodeDesc.equals(other.vegetationCodeDesc)) {
                return false;
            }
        }
        if (this.landform == null) {
            if (other.landform!= null) {
                return false;
            }
        } else {
            if (!this.landform.equals(other.landform)) {
                return false;
            }
        }
        if (this.landformDesc == null) {
            if (other.landformDesc!= null) {
                return false;
            }
        } else {
            if (!this.landformDesc.equals(other.landformDesc)) {
                return false;
            }
        }
        if (this.siteAspect == null) {
            if (other.siteAspect!= null) {
                return false;
            }
        } else {
            if (!this.siteAspect.equals(other.siteAspect)) {
                return false;
            }
        }
        if (this.averageSlope == null) {
            if (other.averageSlope!= null) {
                return false;
            }
        } else {
            if (!this.averageSlope.equals(other.averageSlope)) {
                return false;
            }
        }
        if (this.elevation == null) {
            if (other.elevation!= null) {
                return false;
            }
        } else {
            if (!this.elevation.equals(other.elevation)) {
                return false;
            }
        }
        if (this.siteClass == null) {
            if (other.siteClass!= null) {
                return false;
            }
        } else {
            if (!this.siteClass.equals(other.siteClass)) {
                return false;
            }
        }
        if (this.siteClassDesc == null) {
            if (other.siteClassDesc!= null) {
                return false;
            }
        } else {
            if (!this.siteClassDesc.equals(other.siteClassDesc)) {
                return false;
            }
        }
        if (this.siteUse == null) {
            if (other.siteUse!= null) {
                return false;
            }
        } else {
            if (!this.siteUse.equals(other.siteUse)) {
                return false;
            }
        }
        if (this.siteUseDesc == null) {
            if (other.siteUseDesc!= null) {
                return false;
            }
        } else {
            if (!this.siteUseDesc.equals(other.siteUseDesc)) {
                return false;
            }
        }
        if (this.siteType == null) {
            if (other.siteType!= null) {
                return false;
            }
        } else {
            if (!this.siteType.equals(other.siteType)) {
                return false;
            }
        }
        if (this.siteTypeDesc == null) {
            if (other.siteTypeDesc!= null) {
                return false;
            }
        } else {
            if (!this.siteTypeDesc.equals(other.siteTypeDesc)) {
                return false;
            }
        }
        if (this.siteCoreArea == null) {
            if (other.siteCoreArea!= null) {
                return false;
            }
        } else {
            if (!this.siteCoreArea.equals(other.siteCoreArea)) {
                return false;
            }
        }
        if (this.siteDispersedArea == null) {
            if (other.siteDispersedArea!= null) {
                return false;
            }
        } else {
            if (!this.siteDispersedArea.equals(other.siteDispersedArea)) {
                return false;
            }
        }
        if (this.collectionMade == null) {
            if (other.collectionMade!= null) {
                return false;
            }
        } else {
            if (!this.collectionMade.equals(other.collectionMade)) {
                return false;
            }
        }
        if (this.collectionType == null) {
            if (other.collectionType!= null) {
                return false;
            }
        } else {
            if (!this.collectionType.equals(other.collectionType)) {
                return false;
            }
        }
        if (this.collectionTypeDesc == null) {
            if (other.collectionTypeDesc!= null) {
                return false;
            }
        } else {
            if (!this.collectionTypeDesc.equals(other.collectionTypeDesc)) {
                return false;
            }
        }
        if (this.flakedStoneObserved == null) {
            if (other.flakedStoneObserved!= null) {
                return false;
            }
        } else {
            if (!this.flakedStoneObserved.equals(other.flakedStoneObserved)) {
                return false;
            }
        }
        if (this.flakedStoneObservedDesc == null) {
            if (other.flakedStoneObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.flakedStoneObservedDesc.equals(other.flakedStoneObservedDesc)) {
                return false;
            }
        }
        if (this.groundStoneObserved == null) {
            if (other.groundStoneObserved!= null) {
                return false;
            }
        } else {
            if (!this.groundStoneObserved.equals(other.groundStoneObserved)) {
                return false;
            }
        }
        if (this.groundStoneObservedDesc == null) {
            if (other.groundStoneObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.groundStoneObservedDesc.equals(other.groundStoneObservedDesc)) {
                return false;
            }
        }
        if (this.ceramicsObserved == null) {
            if (other.ceramicsObserved!= null) {
                return false;
            }
        } else {
            if (!this.ceramicsObserved.equals(other.ceramicsObserved)) {
                return false;
            }
        }
        if (this.ceramicsObservedDesc == null) {
            if (other.ceramicsObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.ceramicsObservedDesc.equals(other.ceramicsObservedDesc)) {
                return false;
            }
        }
        if (this.boneObserved == null) {
            if (other.boneObserved!= null) {
                return false;
            }
        } else {
            if (!this.boneObserved.equals(other.boneObserved)) {
                return false;
            }
        }
        if (this.boneObservedDesc == null) {
            if (other.boneObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.boneObservedDesc.equals(other.boneObservedDesc)) {
                return false;
            }
        }
        if (this.shellObserved == null) {
            if (other.shellObserved!= null) {
                return false;
            }
        } else {
            if (!this.shellObserved.equals(other.shellObserved)) {
                return false;
            }
        }
        if (this.shellObservedDesc == null) {
            if (other.shellObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.shellObservedDesc.equals(other.shellObservedDesc)) {
                return false;
            }
        }
        if (this.otherPrehistObserved == null) {
            if (other.otherPrehistObserved!= null) {
                return false;
            }
        } else {
            if (!this.otherPrehistObserved.equals(other.otherPrehistObserved)) {
                return false;
            }
        }
        if (this.otherPrehistObservedDesc == null) {
            if (other.otherPrehistObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.otherPrehistObservedDesc.equals(other.otherPrehistObservedDesc)) {
                return false;
            }
        }
        if (this.histCeramicsObserved == null) {
            if (other.histCeramicsObserved!= null) {
                return false;
            }
        } else {
            if (!this.histCeramicsObserved.equals(other.histCeramicsObserved)) {
                return false;
            }
        }
        if (this.histCeramicsObservedDesc == null) {
            if (other.histCeramicsObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.histCeramicsObservedDesc.equals(other.histCeramicsObservedDesc)) {
                return false;
            }
        }
        if (this.glassObserved == null) {
            if (other.glassObserved!= null) {
                return false;
            }
        } else {
            if (!this.glassObserved.equals(other.glassObserved)) {
                return false;
            }
        }
        if (this.glassObservedDesc == null) {
            if (other.glassObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.glassObservedDesc.equals(other.glassObservedDesc)) {
                return false;
            }
        }
        if (this.cansObserved == null) {
            if (other.cansObserved!= null) {
                return false;
            }
        } else {
            if (!this.cansObserved.equals(other.cansObserved)) {
                return false;
            }
        }
        if (this.cansObservedDesc == null) {
            if (other.cansObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.cansObservedDesc.equals(other.cansObservedDesc)) {
                return false;
            }
        }
        if (this.otherMetalObserved == null) {
            if (other.otherMetalObserved!= null) {
                return false;
            }
        } else {
            if (!this.otherMetalObserved.equals(other.otherMetalObserved)) {
                return false;
            }
        }
        if (this.otherMetalObservedDesc == null) {
            if (other.otherMetalObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.otherMetalObservedDesc.equals(other.otherMetalObservedDesc)) {
                return false;
            }
        }
        if (this.otherHistoricObserved == null) {
            if (other.otherHistoricObserved!= null) {
                return false;
            }
        } else {
            if (!this.otherHistoricObserved.equals(other.otherHistoricObserved)) {
                return false;
            }
        }
        if (this.otherHistoricObservedDesc == null) {
            if (other.otherHistoricObservedDesc!= null) {
                return false;
            }
        } else {
            if (!this.otherHistoricObservedDesc.equals(other.otherHistoricObservedDesc)) {
                return false;
            }
        }
        if (this.constructionMaterial == null) {
            if (other.constructionMaterial!= null) {
                return false;
            }
        } else {
            if (!this.constructionMaterial.equals(other.constructionMaterial)) {
                return false;
            }
        }
        if (this.constructionMaterialDesc == null) {
            if (other.constructionMaterialDesc!= null) {
                return false;
            }
        } else {
            if (!this.constructionMaterialDesc.equals(other.constructionMaterialDesc)) {
                return false;
            }
        }
        if (this.yearsFrom == null) {
            if (other.yearsFrom!= null) {
                return false;
            }
        } else {
            if (!this.yearsFrom.equals(other.yearsFrom)) {
                return false;
            }
        }
        if (this.yearsTo == null) {
            if (other.yearsTo!= null) {
                return false;
            }
        } else {
            if (!this.yearsTo.equals(other.yearsTo)) {
                return false;
            }
        }
        if (this.dateBasedOn == null) {
            if (other.dateBasedOn!= null) {
                return false;
            }
        } else {
            if (!this.dateBasedOn.equals(other.dateBasedOn)) {
                return false;
            }
        }
        if (this.dateBasedOnDesc == null) {
            if (other.dateBasedOnDesc!= null) {
                return false;
            }
        } else {
            if (!this.dateBasedOnDesc.equals(other.dateBasedOnDesc)) {
                return false;
            }
        }
        if (this.subsurfaceRooms == null) {
            if (other.subsurfaceRooms!= null) {
                return false;
            }
        } else {
            if (!this.subsurfaceRooms.equals(other.subsurfaceRooms)) {
                return false;
            }
        }
        if (this.surfaceRooms == null) {
            if (other.surfaceRooms!= null) {
                return false;
            }
        } else {
            if (!this.surfaceRooms.equals(other.surfaceRooms)) {
                return false;
            }
        }
        if (this.nonRoomWalls == null) {
            if (other.nonRoomWalls!= null) {
                return false;
            }
        } else {
            if (!this.nonRoomWalls.equals(other.nonRoomWalls)) {
                return false;
            }
        }
        if (this.partialShelters == null) {
            if (other.partialShelters!= null) {
                return false;
            }
        } else {
            if (!this.partialShelters.equals(other.partialShelters)) {
                return false;
            }
        }
        if (this.middens == null) {
            if (other.middens!= null) {
                return false;
            }
        } else {
            if (!this.middens.equals(other.middens)) {
                return false;
            }
        }
        if (this.hearth == null) {
            if (other.hearth!= null) {
                return false;
            }
        } else {
            if (!this.hearth.equals(other.hearth)) {
                return false;
            }
        }
        if (this.cists == null) {
            if (other.cists!= null) {
                return false;
            }
        } else {
            if (!this.cists.equals(other.cists)) {
                return false;
            }
        }
        if (this.roastingPit == null) {
            if (other.roastingPit!= null) {
                return false;
            }
        } else {
            if (!this.roastingPit.equals(other.roastingPit)) {
                return false;
            }
        }
        if (this.nonRoofedArea == null) {
            if (other.nonRoofedArea!= null) {
                return false;
            }
        } else {
            if (!this.nonRoofedArea.equals(other.nonRoofedArea)) {
                return false;
            }
        }
        if (this.modifiedCave == null) {
            if (other.modifiedCave!= null) {
                return false;
            }
        } else {
            if (!this.modifiedCave.equals(other.modifiedCave)) {
                return false;
            }
        }
        if (this.depression == null) {
            if (other.depression!= null) {
                return false;
            }
        } else {
            if (!this.depression.equals(other.depression)) {
                return false;
            }
        }
        if (this.waterSoilControl == null) {
            if (other.waterSoilControl!= null) {
                return false;
            }
        } else {
            if (!this.waterSoilControl.equals(other.waterSoilControl)) {
                return false;
            }
        }
        if (this.nonMiddenMound == null) {
            if (other.nonMiddenMound!= null) {
                return false;
            }
        } else {
            if (!this.nonMiddenMound.equals(other.nonMiddenMound)) {
                return false;
            }
        }
        if (this.bedrockGrinding == null) {
            if (other.bedrockGrinding!= null) {
                return false;
            }
        } else {
            if (!this.bedrockGrinding.equals(other.bedrockGrinding)) {
                return false;
            }
        }
        if (this.quarryMine == null) {
            if (other.quarryMine!= null) {
                return false;
            }
        } else {
            if (!this.quarryMine.equals(other.quarryMine)) {
                return false;
            }
        }
        if (this.buildings == null) {
            if (other.buildings!= null) {
                return false;
            }
        } else {
            if (!this.buildings.equals(other.buildings)) {
                return false;
            }
        }
        if (this.rockArt == null) {
            if (other.rockArt!= null) {
                return false;
            }
        } else {
            if (!this.rockArt.equals(other.rockArt)) {
                return false;
            }
        }
        if (this.rockArtDesc == null) {
            if (other.rockArtDesc!= null) {
                return false;
            }
        } else {
            if (!this.rockArtDesc.equals(other.rockArtDesc)) {
                return false;
            }
        }
        if (this.miscFeatures == null) {
            if (other.miscFeatures!= null) {
                return false;
            }
        } else {
            if (!this.miscFeatures.equals(other.miscFeatures)) {
                return false;
            }
        }
        if (this.multiComponents == null) {
            if (other.multiComponents!= null) {
                return false;
            }
        } else {
            if (!this.multiComponents.equals(other.multiComponents)) {
                return false;
            }
        }
        if (this.cultClass == null) {
            if (other.cultClass!= null) {
                return false;
            }
        } else {
            if (!this.cultClass.equals(other.cultClass)) {
                return false;
            }
        }
        if (this.cultClassDesc == null) {
            if (other.cultClassDesc!= null) {
                return false;
            }
        } else {
            if (!this.cultClassDesc.equals(other.cultClassDesc)) {
                return false;
            }
        }
        if (this.cultClassPhase == null) {
            if (other.cultClassPhase!= null) {
                return false;
            }
        } else {
            if (!this.cultClassPhase.equals(other.cultClassPhase)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.fsSiteNum!= null) {
            rtn = (rtn + this.fsSiteNum.hashCode());
        }
        rtn = (rtn* 37);
        if (this.region!= null) {
            rtn = (rtn + this.region.hashCode());
        }
        rtn = (rtn* 37);
        if (this.forest!= null) {
            rtn = (rtn + this.forest.hashCode());
        }
        rtn = (rtn* 37);
        if (this.district!= null) {
            rtn = (rtn + this.district.hashCode());
        }
        rtn = (rtn* 37);
        if (this.districtDesc!= null) {
            rtn = (rtn + this.districtDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteNumber!= null) {
            rtn = (rtn + this.siteNumber.hashCode());
        }
        rtn = (rtn* 37);
        if (this.stateSiteNumber!= null) {
            rtn = (rtn + this.stateSiteNumber.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportYear!= null) {
            rtn = (rtn + this.reportYear.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportForest!= null) {
            rtn = (rtn + this.reportForest.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportNum!= null) {
            rtn = (rtn + this.reportNum.hashCode());
        }
        rtn = (rtn* 37);
        if (this.inventoryMonth!= null) {
            rtn = (rtn + this.inventoryMonth.hashCode());
        }
        rtn = (rtn* 37);
        if (this.inventoryDay!= null) {
            rtn = (rtn + this.inventoryDay.hashCode());
        }
        rtn = (rtn* 37);
        if (this.inventoryYear!= null) {
            rtn = (rtn + this.inventoryYear.hashCode());
        }
        rtn = (rtn* 37);
        if (this.recordedBy!= null) {
            rtn = (rtn + this.recordedBy.hashCode());
        }
        rtn = (rtn* 37);
        if (this.recordedByDesc!= null) {
            rtn = (rtn + this.recordedByDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.t!= null) {
            rtn = (rtn + this.t.hashCode());
        }
        rtn = (rtn* 37);
        if (this.township!= null) {
            rtn = (rtn + this.township.hashCode());
        }
        rtn = (rtn* 37);
        if (this.NS!= null) {
            rtn = (rtn + this.NS.hashCode());
        }
        rtn = (rtn* 37);
        if (this.r!= null) {
            rtn = (rtn + this.r.hashCode());
        }
        rtn = (rtn* 37);
        if (this.range!= null) {
            rtn = (rtn + this.range.hashCode());
        }
        rtn = (rtn* 37);
        if (this.EW!= null) {
            rtn = (rtn + this.EW.hashCode());
        }
        rtn = (rtn* 37);
        if (this.zone!= null) {
            rtn = (rtn + this.zone.hashCode());
        }
        rtn = (rtn* 37);
        if (this.northing1 != null) {
            rtn = (rtn + this.northing1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.northing2 != null) {
            rtn = (rtn + this.northing2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.northing3 != null) {
            rtn = (rtn + this.northing3 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.easting1 != null) {
            rtn = (rtn + this.easting1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.easting2 != null) {
            rtn = (rtn + this.easting2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.easting3 != null) {
            rtn = (rtn + this.easting3 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.evaluation!= null) {
            rtn = (rtn + this.evaluation.hashCode());
        }
        rtn = (rtn* 37);
        if (this.evaluationDesc!= null) {
            rtn = (rtn + this.evaluationDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.condition!= null) {
            rtn = (rtn + this.condition.hashCode());
        }
        rtn = (rtn* 37);
        if (this.conditionDesc!= null) {
            rtn = (rtn + this.conditionDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.percentDisturbance!= null) {
            rtn = (rtn + this.percentDisturbance.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteMarking!= null) {
            rtn = (rtn + this.siteMarking.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteMarkingDesc!= null) {
            rtn = (rtn + this.siteMarkingDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.vegetationCode!= null) {
            rtn = (rtn + this.vegetationCode.hashCode());
        }
        rtn = (rtn* 37);
        if (this.vegetationCodeDesc!= null) {
            rtn = (rtn + this.vegetationCodeDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.landform!= null) {
            rtn = (rtn + this.landform.hashCode());
        }
        rtn = (rtn* 37);
        if (this.landformDesc!= null) {
            rtn = (rtn + this.landformDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteAspect!= null) {
            rtn = (rtn + this.siteAspect.hashCode());
        }
        rtn = (rtn* 37);
        if (this.averageSlope!= null) {
            rtn = (rtn + this.averageSlope.hashCode());
        }
        rtn = (rtn* 37);
        if (this.elevation!= null) {
            rtn = (rtn + this.elevation.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteClass!= null) {
            rtn = (rtn + this.siteClass.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteClassDesc!= null) {
            rtn = (rtn + this.siteClassDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteUse!= null) {
            rtn = (rtn + this.siteUse.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteUseDesc!= null) {
            rtn = (rtn + this.siteUseDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteType!= null) {
            rtn = (rtn + this.siteType.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteTypeDesc!= null) {
            rtn = (rtn + this.siteTypeDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteCoreArea!= null) {
            rtn = (rtn + this.siteCoreArea.hashCode());
        }
        rtn = (rtn* 37);
        if (this.siteDispersedArea!= null) {
            rtn = (rtn + this.siteDispersedArea.hashCode());
        }
        rtn = (rtn* 37);
        if (this.collectionMade!= null) {
            rtn = (rtn + this.collectionMade.hashCode());
        }
        rtn = (rtn* 37);
        if (this.collectionType!= null) {
            rtn = (rtn + this.collectionType.hashCode());
        }
        rtn = (rtn* 37);
        if (this.collectionTypeDesc!= null) {
            rtn = (rtn + this.collectionTypeDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.flakedStoneObserved!= null) {
            rtn = (rtn + this.flakedStoneObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.flakedStoneObservedDesc!= null) {
            rtn = (rtn + this.flakedStoneObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.groundStoneObserved!= null) {
            rtn = (rtn + this.groundStoneObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.groundStoneObservedDesc!= null) {
            rtn = (rtn + this.groundStoneObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ceramicsObserved!= null) {
            rtn = (rtn + this.ceramicsObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.ceramicsObservedDesc!= null) {
            rtn = (rtn + this.ceramicsObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.boneObserved!= null) {
            rtn = (rtn + this.boneObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.boneObservedDesc!= null) {
            rtn = (rtn + this.boneObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.shellObserved!= null) {
            rtn = (rtn + this.shellObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.shellObservedDesc!= null) {
            rtn = (rtn + this.shellObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.otherPrehistObserved!= null) {
            rtn = (rtn + this.otherPrehistObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.otherPrehistObservedDesc!= null) {
            rtn = (rtn + this.otherPrehistObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.histCeramicsObserved!= null) {
            rtn = (rtn + this.histCeramicsObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.histCeramicsObservedDesc!= null) {
            rtn = (rtn + this.histCeramicsObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.glassObserved!= null) {
            rtn = (rtn + this.glassObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.glassObservedDesc!= null) {
            rtn = (rtn + this.glassObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cansObserved!= null) {
            rtn = (rtn + this.cansObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cansObservedDesc!= null) {
            rtn = (rtn + this.cansObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.otherMetalObserved!= null) {
            rtn = (rtn + this.otherMetalObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.otherMetalObservedDesc!= null) {
            rtn = (rtn + this.otherMetalObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.otherHistoricObserved!= null) {
            rtn = (rtn + this.otherHistoricObserved.hashCode());
        }
        rtn = (rtn* 37);
        if (this.otherHistoricObservedDesc!= null) {
            rtn = (rtn + this.otherHistoricObservedDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.constructionMaterial!= null) {
            rtn = (rtn + this.constructionMaterial.hashCode());
        }
        rtn = (rtn* 37);
        if (this.constructionMaterialDesc!= null) {
            rtn = (rtn + this.constructionMaterialDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.yearsFrom!= null) {
            rtn = (rtn + this.yearsFrom.hashCode());
        }
        rtn = (rtn* 37);
        if (this.yearsTo!= null) {
            rtn = (rtn + this.yearsTo.hashCode());
        }
        rtn = (rtn* 37);
        if (this.dateBasedOn!= null) {
            rtn = (rtn + this.dateBasedOn.hashCode());
        }
        rtn = (rtn* 37);
        if (this.dateBasedOnDesc!= null) {
            rtn = (rtn + this.dateBasedOnDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.subsurfaceRooms!= null) {
            rtn = (rtn + this.subsurfaceRooms.hashCode());
        }
        rtn = (rtn* 37);
        if (this.surfaceRooms!= null) {
            rtn = (rtn + this.surfaceRooms.hashCode());
        }
        rtn = (rtn* 37);
        if (this.nonRoomWalls!= null) {
            rtn = (rtn + this.nonRoomWalls.hashCode());
        }
        rtn = (rtn* 37);
        if (this.partialShelters!= null) {
            rtn = (rtn + this.partialShelters.hashCode());
        }
        rtn = (rtn* 37);
        if (this.middens!= null) {
            rtn = (rtn + this.middens.hashCode());
        }
        rtn = (rtn* 37);
        if (this.hearth!= null) {
            rtn = (rtn + this.hearth.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cists!= null) {
            rtn = (rtn + this.cists.hashCode());
        }
        rtn = (rtn* 37);
        if (this.roastingPit!= null) {
            rtn = (rtn + this.roastingPit.hashCode());
        }
        rtn = (rtn* 37);
        if (this.nonRoofedArea!= null) {
            rtn = (rtn + this.nonRoofedArea.hashCode());
        }
        rtn = (rtn* 37);
        if (this.modifiedCave!= null) {
            rtn = (rtn + this.modifiedCave.hashCode());
        }
        rtn = (rtn* 37);
        if (this.depression!= null) {
            rtn = (rtn + this.depression.hashCode());
        }
        rtn = (rtn* 37);
        if (this.waterSoilControl!= null) {
            rtn = (rtn + this.waterSoilControl.hashCode());
        }
        rtn = (rtn* 37);
        if (this.nonMiddenMound!= null) {
            rtn = (rtn + this.nonMiddenMound.hashCode());
        }
        rtn = (rtn* 37);
        if (this.bedrockGrinding!= null) {
            rtn = (rtn + this.bedrockGrinding.hashCode());
        }
        rtn = (rtn* 37);
        if (this.quarryMine!= null) {
            rtn = (rtn + this.quarryMine.hashCode());
        }
        rtn = (rtn* 37);
        if (this.buildings!= null) {
            rtn = (rtn + this.buildings.hashCode());
        }
        rtn = (rtn* 37);
        if (this.rockArt!= null) {
            rtn = (rtn + this.rockArt.hashCode());
        }
        rtn = (rtn* 37);
        if (this.rockArtDesc!= null) {
            rtn = (rtn + this.rockArtDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.miscFeatures!= null) {
            rtn = (rtn + this.miscFeatures.hashCode());
        }
        rtn = (rtn* 37);
        if (this.multiComponents!= null) {
            rtn = (rtn + this.multiComponents.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cultClass!= null) {
            rtn = (rtn + this.cultClass.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cultClassDesc!= null) {
            rtn = (rtn + this.cultClassDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.cultClassPhase!= null) {
            rtn = (rtn + this.cultClassPhase.hashCode());
        }
        return rtn;
    }

    public Date getFsSiteNum() {
        return fsSiteNum;
    }

    public void setFsSiteNum(Date fsSiteNum) {
        this.fsSiteNum = fsSiteNum;
    }

    public Date getRegion() {
        return region;
    }

    public void setRegion(Date region) {
        this.region = region;
    }

    public Date getForest() {
        return forest;
    }

    public void setForest(Date forest) {
        this.forest = forest;
    }

    public Date getDistrict() {
        return district;
    }

    public void setDistrict(Date district) {
        this.district = district;
    }

    public Date getDistrictDesc() {
        return districtDesc;
    }

    public void setDistrictDesc(Date districtDesc) {
        this.districtDesc = districtDesc;
    }

    public Date getSiteNumber() {
        return siteNumber;
    }

    public void setSiteNumber(Date siteNumber) {
        this.siteNumber = siteNumber;
    }

    public Date getStateSiteNumber() {
        return stateSiteNumber;
    }

    public void setStateSiteNumber(Date stateSiteNumber) {
        this.stateSiteNumber = stateSiteNumber;
    }

    public Date getReportYear() {
        return reportYear;
    }

    public void setReportYear(Date reportYear) {
        this.reportYear = reportYear;
    }

    public Date getReportForest() {
        return reportForest;
    }

    public void setReportForest(Date reportForest) {
        this.reportForest = reportForest;
    }

    public Date getReportNum() {
        return reportNum;
    }

    public void setReportNum(Date reportNum) {
        this.reportNum = reportNum;
    }

    public Date getInventoryMonth() {
        return inventoryMonth;
    }

    public void setInventoryMonth(Date inventoryMonth) {
        this.inventoryMonth = inventoryMonth;
    }

    public Date getInventoryDay() {
        return inventoryDay;
    }

    public void setInventoryDay(Date inventoryDay) {
        this.inventoryDay = inventoryDay;
    }

    public Date getInventoryYear() {
        return inventoryYear;
    }

    public void setInventoryYear(Date inventoryYear) {
        this.inventoryYear = inventoryYear;
    }

    public Date getRecordedBy() {
        return recordedBy;
    }

    public void setRecordedBy(Date recordedBy) {
        this.recordedBy = recordedBy;
    }

    public Date getRecordedByDesc() {
        return recordedByDesc;
    }

    public void setRecordedByDesc(Date recordedByDesc) {
        this.recordedByDesc = recordedByDesc;
    }

    public Date getT() {
        return t;
    }

    public void setT(Date t) {
        this.t = t;
    }

    public Date getTownship() {
        return township;
    }

    public void setTownship(Date township) {
        this.township = township;
    }

    public Date getNS() {
        return NS;
    }

    public void setNS(Date NS) {
        this.NS = NS;
    }

    public Date getR() {
        return r;
    }

    public void setR(Date r) {
        this.r = r;
    }

    public Date getRange() {
        return range;
    }

    public void setRange(Date range) {
        this.range = range;
    }

    public Date getEW() {
        return EW;
    }

    public void setEW(Date EW) {
        this.EW = EW;
    }

    public Date getZone() {
        return zone;
    }

    public void setZone(Date zone) {
        this.zone = zone;
    }

    public Date getNorthing1() {
        return northing1;
    }

    public void setNorthing1(Date northing1) {
        this.northing1 = northing1;
    }

    public Date getNorthing2() {
        return northing2;
    }

    public void setNorthing2(Date northing2) {
        this.northing2 = northing2;
    }

    public Date getNorthing3() {
        return northing3;
    }

    public void setNorthing3(Date northing3) {
        this.northing3 = northing3;
    }

    public Date getEasting1() {
        return easting1;
    }

    public void setEasting1(Date easting1) {
        this.easting1 = easting1;
    }

    public Date getEasting2() {
        return easting2;
    }

    public void setEasting2(Date easting2) {
        this.easting2 = easting2;
    }

    public Date getEasting3() {
        return easting3;
    }

    public void setEasting3(Date easting3) {
        this.easting3 = easting3;
    }

    public Date getEvaluation() {
        return evaluation;
    }

    public void setEvaluation(Date evaluation) {
        this.evaluation = evaluation;
    }

    public Date getEvaluationDesc() {
        return evaluationDesc;
    }

    public void setEvaluationDesc(Date evaluationDesc) {
        this.evaluationDesc = evaluationDesc;
    }

    public Date getCondition() {
        return condition;
    }

    public void setCondition(Date condition) {
        this.condition = condition;
    }

    public Date getConditionDesc() {
        return conditionDesc;
    }

    public void setConditionDesc(Date conditionDesc) {
        this.conditionDesc = conditionDesc;
    }

    public Date getPercentDisturbance() {
        return percentDisturbance;
    }

    public void setPercentDisturbance(Date percentDisturbance) {
        this.percentDisturbance = percentDisturbance;
    }

    public Date getSiteMarking() {
        return siteMarking;
    }

    public void setSiteMarking(Date siteMarking) {
        this.siteMarking = siteMarking;
    }

    public Date getSiteMarkingDesc() {
        return siteMarkingDesc;
    }

    public void setSiteMarkingDesc(Date siteMarkingDesc) {
        this.siteMarkingDesc = siteMarkingDesc;
    }

    public Date getVegetationCode() {
        return vegetationCode;
    }

    public void setVegetationCode(Date vegetationCode) {
        this.vegetationCode = vegetationCode;
    }

    public Date getVegetationCodeDesc() {
        return vegetationCodeDesc;
    }

    public void setVegetationCodeDesc(Date vegetationCodeDesc) {
        this.vegetationCodeDesc = vegetationCodeDesc;
    }

    public Date getLandform() {
        return landform;
    }

    public void setLandform(Date landform) {
        this.landform = landform;
    }

    public Date getLandformDesc() {
        return landformDesc;
    }

    public void setLandformDesc(Date landformDesc) {
        this.landformDesc = landformDesc;
    }

    public Date getSiteAspect() {
        return siteAspect;
    }

    public void setSiteAspect(Date siteAspect) {
        this.siteAspect = siteAspect;
    }

    public Date getAverageSlope() {
        return averageSlope;
    }

    public void setAverageSlope(Date averageSlope) {
        this.averageSlope = averageSlope;
    }

    public Date getElevation() {
        return elevation;
    }

    public void setElevation(Date elevation) {
        this.elevation = elevation;
    }

    public Date getSiteClass() {
        return siteClass;
    }

    public void setSiteClass(Date siteClass) {
        this.siteClass = siteClass;
    }

    public Date getSiteClassDesc() {
        return siteClassDesc;
    }

    public void setSiteClassDesc(Date siteClassDesc) {
        this.siteClassDesc = siteClassDesc;
    }

    public Date getSiteUse() {
        return siteUse;
    }

    public void setSiteUse(Date siteUse) {
        this.siteUse = siteUse;
    }

    public Date getSiteUseDesc() {
        return siteUseDesc;
    }

    public void setSiteUseDesc(Date siteUseDesc) {
        this.siteUseDesc = siteUseDesc;
    }

    public Date getSiteType() {
        return siteType;
    }

    public void setSiteType(Date siteType) {
        this.siteType = siteType;
    }

    public Date getSiteTypeDesc() {
        return siteTypeDesc;
    }

    public void setSiteTypeDesc(Date siteTypeDesc) {
        this.siteTypeDesc = siteTypeDesc;
    }

    public Date getSiteCoreArea() {
        return siteCoreArea;
    }

    public void setSiteCoreArea(Date siteCoreArea) {
        this.siteCoreArea = siteCoreArea;
    }

    public Date getSiteDispersedArea() {
        return siteDispersedArea;
    }

    public void setSiteDispersedArea(Date siteDispersedArea) {
        this.siteDispersedArea = siteDispersedArea;
    }

    public Date getCollectionMade() {
        return collectionMade;
    }

    public void setCollectionMade(Date collectionMade) {
        this.collectionMade = collectionMade;
    }

    public Date getCollectionType() {
        return collectionType;
    }

    public void setCollectionType(Date collectionType) {
        this.collectionType = collectionType;
    }

    public Date getCollectionTypeDesc() {
        return collectionTypeDesc;
    }

    public void setCollectionTypeDesc(Date collectionTypeDesc) {
        this.collectionTypeDesc = collectionTypeDesc;
    }

    public Date getFlakedStoneObserved() {
        return flakedStoneObserved;
    }

    public void setFlakedStoneObserved(Date flakedStoneObserved) {
        this.flakedStoneObserved = flakedStoneObserved;
    }

    public Date getFlakedStoneObservedDesc() {
        return flakedStoneObservedDesc;
    }

    public void setFlakedStoneObservedDesc(Date flakedStoneObservedDesc) {
        this.flakedStoneObservedDesc = flakedStoneObservedDesc;
    }

    public Date getGroundStoneObserved() {
        return groundStoneObserved;
    }

    public void setGroundStoneObserved(Date groundStoneObserved) {
        this.groundStoneObserved = groundStoneObserved;
    }

    public Date getGroundStoneObservedDesc() {
        return groundStoneObservedDesc;
    }

    public void setGroundStoneObservedDesc(Date groundStoneObservedDesc) {
        this.groundStoneObservedDesc = groundStoneObservedDesc;
    }

    public Date getCeramicsObserved() {
        return ceramicsObserved;
    }

    public void setCeramicsObserved(Date ceramicsObserved) {
        this.ceramicsObserved = ceramicsObserved;
    }

    public Date getCeramicsObservedDesc() {
        return ceramicsObservedDesc;
    }

    public void setCeramicsObservedDesc(Date ceramicsObservedDesc) {
        this.ceramicsObservedDesc = ceramicsObservedDesc;
    }

    public Date getBoneObserved() {
        return boneObserved;
    }

    public void setBoneObserved(Date boneObserved) {
        this.boneObserved = boneObserved;
    }

    public Date getBoneObservedDesc() {
        return boneObservedDesc;
    }

    public void setBoneObservedDesc(Date boneObservedDesc) {
        this.boneObservedDesc = boneObservedDesc;
    }

    public Date getShellObserved() {
        return shellObserved;
    }

    public void setShellObserved(Date shellObserved) {
        this.shellObserved = shellObserved;
    }

    public Date getShellObservedDesc() {
        return shellObservedDesc;
    }

    public void setShellObservedDesc(Date shellObservedDesc) {
        this.shellObservedDesc = shellObservedDesc;
    }

    public Date getOtherPrehistObserved() {
        return otherPrehistObserved;
    }

    public void setOtherPrehistObserved(Date otherPrehistObserved) {
        this.otherPrehistObserved = otherPrehistObserved;
    }

    public Date getOtherPrehistObservedDesc() {
        return otherPrehistObservedDesc;
    }

    public void setOtherPrehistObservedDesc(Date otherPrehistObservedDesc) {
        this.otherPrehistObservedDesc = otherPrehistObservedDesc;
    }

    public Date getHistCeramicsObserved() {
        return histCeramicsObserved;
    }

    public void setHistCeramicsObserved(Date histCeramicsObserved) {
        this.histCeramicsObserved = histCeramicsObserved;
    }

    public Date getHistCeramicsObservedDesc() {
        return histCeramicsObservedDesc;
    }

    public void setHistCeramicsObservedDesc(Date histCeramicsObservedDesc) {
        this.histCeramicsObservedDesc = histCeramicsObservedDesc;
    }

    public Date getGlassObserved() {
        return glassObserved;
    }

    public void setGlassObserved(Date glassObserved) {
        this.glassObserved = glassObserved;
    }

    public Date getGlassObservedDesc() {
        return glassObservedDesc;
    }

    public void setGlassObservedDesc(Date glassObservedDesc) {
        this.glassObservedDesc = glassObservedDesc;
    }

    public Date getCansObserved() {
        return cansObserved;
    }

    public void setCansObserved(Date cansObserved) {
        this.cansObserved = cansObserved;
    }

    public Date getCansObservedDesc() {
        return cansObservedDesc;
    }

    public void setCansObservedDesc(Date cansObservedDesc) {
        this.cansObservedDesc = cansObservedDesc;
    }

    public Date getOtherMetalObserved() {
        return otherMetalObserved;
    }

    public void setOtherMetalObserved(Date otherMetalObserved) {
        this.otherMetalObserved = otherMetalObserved;
    }

    public Date getOtherMetalObservedDesc() {
        return otherMetalObservedDesc;
    }

    public void setOtherMetalObservedDesc(Date otherMetalObservedDesc) {
        this.otherMetalObservedDesc = otherMetalObservedDesc;
    }

    public Date getOtherHistoricObserved() {
        return otherHistoricObserved;
    }

    public void setOtherHistoricObserved(Date otherHistoricObserved) {
        this.otherHistoricObserved = otherHistoricObserved;
    }

    public Date getOtherHistoricObservedDesc() {
        return otherHistoricObservedDesc;
    }

    public void setOtherHistoricObservedDesc(Date otherHistoricObservedDesc) {
        this.otherHistoricObservedDesc = otherHistoricObservedDesc;
    }

    public Date getConstructionMaterial() {
        return constructionMaterial;
    }

    public void setConstructionMaterial(Date constructionMaterial) {
        this.constructionMaterial = constructionMaterial;
    }

    public Date getConstructionMaterialDesc() {
        return constructionMaterialDesc;
    }

    public void setConstructionMaterialDesc(Date constructionMaterialDesc) {
        this.constructionMaterialDesc = constructionMaterialDesc;
    }

    public Date getYearsFrom() {
        return yearsFrom;
    }

    public void setYearsFrom(Date yearsFrom) {
        this.yearsFrom = yearsFrom;
    }

    public Date getYearsTo() {
        return yearsTo;
    }

    public void setYearsTo(Date yearsTo) {
        this.yearsTo = yearsTo;
    }

    public Date getDateBasedOn() {
        return dateBasedOn;
    }

    public void setDateBasedOn(Date dateBasedOn) {
        this.dateBasedOn = dateBasedOn;
    }

    public Date getDateBasedOnDesc() {
        return dateBasedOnDesc;
    }

    public void setDateBasedOnDesc(Date dateBasedOnDesc) {
        this.dateBasedOnDesc = dateBasedOnDesc;
    }

    public Date getSubsurfaceRooms() {
        return subsurfaceRooms;
    }

    public void setSubsurfaceRooms(Date subsurfaceRooms) {
        this.subsurfaceRooms = subsurfaceRooms;
    }

    public Date getSurfaceRooms() {
        return surfaceRooms;
    }

    public void setSurfaceRooms(Date surfaceRooms) {
        this.surfaceRooms = surfaceRooms;
    }

    public Date getNonRoomWalls() {
        return nonRoomWalls;
    }

    public void setNonRoomWalls(Date nonRoomWalls) {
        this.nonRoomWalls = nonRoomWalls;
    }

    public Date getPartialShelters() {
        return partialShelters;
    }

    public void setPartialShelters(Date partialShelters) {
        this.partialShelters = partialShelters;
    }

    public Date getMiddens() {
        return middens;
    }

    public void setMiddens(Date middens) {
        this.middens = middens;
    }

    public Date getHearth() {
        return hearth;
    }

    public void setHearth(Date hearth) {
        this.hearth = hearth;
    }

    public Date getCists() {
        return cists;
    }

    public void setCists(Date cists) {
        this.cists = cists;
    }

    public Date getRoastingPit() {
        return roastingPit;
    }

    public void setRoastingPit(Date roastingPit) {
        this.roastingPit = roastingPit;
    }

    public Date getNonRoofedArea() {
        return nonRoofedArea;
    }

    public void setNonRoofedArea(Date nonRoofedArea) {
        this.nonRoofedArea = nonRoofedArea;
    }

    public Date getModifiedCave() {
        return modifiedCave;
    }

    public void setModifiedCave(Date modifiedCave) {
        this.modifiedCave = modifiedCave;
    }

    public Date getDepression() {
        return depression;
    }

    public void setDepression(Date depression) {
        this.depression = depression;
    }

    public Date getWaterSoilControl() {
        return waterSoilControl;
    }

    public void setWaterSoilControl(Date waterSoilControl) {
        this.waterSoilControl = waterSoilControl;
    }

    public Date getNonMiddenMound() {
        return nonMiddenMound;
    }

    public void setNonMiddenMound(Date nonMiddenMound) {
        this.nonMiddenMound = nonMiddenMound;
    }

    public Date getBedrockGrinding() {
        return bedrockGrinding;
    }

    public void setBedrockGrinding(Date bedrockGrinding) {
        this.bedrockGrinding = bedrockGrinding;
    }

    public Date getQuarryMine() {
        return quarryMine;
    }

    public void setQuarryMine(Date quarryMine) {
        this.quarryMine = quarryMine;
    }

    public Date getBuildings() {
        return buildings;
    }

    public void setBuildings(Date buildings) {
        this.buildings = buildings;
    }

    public Date getRockArt() {
        return rockArt;
    }

    public void setRockArt(Date rockArt) {
        this.rockArt = rockArt;
    }

    public Date getRockArtDesc() {
        return rockArtDesc;
    }

    public void setRockArtDesc(Date rockArtDesc) {
        this.rockArtDesc = rockArtDesc;
    }

    public Date getMiscFeatures() {
        return miscFeatures;
    }

    public void setMiscFeatures(Date miscFeatures) {
        this.miscFeatures = miscFeatures;
    }

    public Date getMultiComponents() {
        return multiComponents;
    }

    public void setMultiComponents(Date multiComponents) {
        this.multiComponents = multiComponents;
    }

    public Date getCultClass() {
        return cultClass;
    }

    public void setCultClass(Date cultClass) {
        this.cultClass = cultClass;
    }

    public Date getCultClassDesc() {
        return cultClassDesc;
    }

    public void setCultClassDesc(Date cultClassDesc) {
        this.cultClassDesc = cultClassDesc;
    }

    public Date getCultClassPhase() {
        return cultClassPhase;
    }

    public void setCultClassPhase(Date cultClassPhase) {
        this.cultClassPhase = cultClassPhase;
    }

}
