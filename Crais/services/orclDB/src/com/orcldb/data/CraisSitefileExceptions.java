
package com.orcldb.data;



/**
 *  orclDB.CraisSitefileExceptions
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSitefileExceptions {

    private CraisSitefileExceptionsId id;

    public CraisSitefileExceptionsId getId() {
        return id;
    }

    public void setId(CraisSitefileExceptionsId id) {
        this.id = id;
    }

}
