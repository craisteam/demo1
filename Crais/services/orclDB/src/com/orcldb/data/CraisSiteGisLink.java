
package com.orcldb.data;



/**
 *  orclDB.CraisSiteGisLink
 *  12/08/2013 22:45:19
 * 
 */
public class CraisSiteGisLink {

    private CraisSiteGisLinkId id;

    public CraisSiteGisLinkId getId() {
        return id;
    }

    public void setId(CraisSiteGisLinkId id) {
        this.id = id;
    }

}
