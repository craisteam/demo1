
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisDetOfEffect
 *  12/08/2013 22:45:19
 * 
 */
public class CraisDetOfEffect {

    private Boolean code;
    private String description;
    private Set<com.orcldb.data.CraisIstandardsRecordBad> craisIstandardsRecordBads = new HashSet<com.orcldb.data.CraisIstandardsRecordBad>();
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcldb.data.CraisIstandardsRecord>();

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisIstandardsRecordBad> getCraisIstandardsRecordBads() {
        return craisIstandardsRecordBads;
    }

    public void setCraisIstandardsRecordBads(Set<com.orcldb.data.CraisIstandardsRecordBad> craisIstandardsRecordBads) {
        this.craisIstandardsRecordBads = craisIstandardsRecordBads;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
