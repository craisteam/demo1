
package com.orcldb.data;

import java.io.Serializable;
import java.util.Date;


/**
 *  orclDB.CraisIsaGisViewId
 *  12/08/2013 22:45:19
 * 
 */
public class CraisIsaGisViewId
    implements Serializable
{

    private Date data;
    private Date data1;
    private Date region;
    private Date reptYear;
    private Date forest;
    private Date reptNumber;
    private Date series1;
    private Date series2;
    private Date reptMonth;
    private Date reptDay;
    private Date reptYear1;
    private Date author1LastName;
    private Date author1Fi;
    private Date author1Mi;
    private Date author2LastName;
    private Date author2Fi;
    private Date author2Mi;
    private Date reportTitle;
    private Date districtNumber;
    private Date projFunction;
    private Date projFunctionDesc;
    private Date activityType1;
    private Date activityType1Desc;
    private Date activityType2;
    private Date activityType2Desc;
    private Date programming;
    private Date totalProjAcreage;
    private Date institution;
    private Date institutionDesc;
    private Date acreageCompInvent;
    private Date percentSample;
    private Date aveNoIndividuals;
    private Date acreageResurveyed;
    private Date aveSpacing;
    private Date totalSites;
    private Date fieldHours;
    private Date newSitesLocated;
    private Date labLibHours;
    private Date travelHours;
    private Date sitesEligible;
    private Date adminHours;
    private Date sitesNotEligible;
    private Date mileage;
    private Date perDiemRate;
    private Date sitesEnhanced;
    private Date perDiemDaysPaid;
    private Date costWeighting;
    private Date recomDeterEffect;
    private Date recomDeterEffectDesc;
    private Date totalCrmCost;
    private Date actualCrmCost;
    private Date stateProject;
    private Date stateProjectNo;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisIsaGisViewId)) {
            return false;
        }
        CraisIsaGisViewId other = ((CraisIsaGisViewId) o);
        if (this.data == null) {
            if (other.data!= null) {
                return false;
            }
        } else {
            if (!this.data.equals(other.data)) {
                return false;
            }
        }
        if (this.data1 == null) {
            if (other.data1 != null) {
                return false;
            }
        } else {
            if (!this.data1 .equals(other.data1)) {
                return false;
            }
        }
        if (this.region == null) {
            if (other.region!= null) {
                return false;
            }
        } else {
            if (!this.region.equals(other.region)) {
                return false;
            }
        }
        if (this.reptYear == null) {
            if (other.reptYear!= null) {
                return false;
            }
        } else {
            if (!this.reptYear.equals(other.reptYear)) {
                return false;
            }
        }
        if (this.forest == null) {
            if (other.forest!= null) {
                return false;
            }
        } else {
            if (!this.forest.equals(other.forest)) {
                return false;
            }
        }
        if (this.reptNumber == null) {
            if (other.reptNumber!= null) {
                return false;
            }
        } else {
            if (!this.reptNumber.equals(other.reptNumber)) {
                return false;
            }
        }
        if (this.series1 == null) {
            if (other.series1 != null) {
                return false;
            }
        } else {
            if (!this.series1 .equals(other.series1)) {
                return false;
            }
        }
        if (this.series2 == null) {
            if (other.series2 != null) {
                return false;
            }
        } else {
            if (!this.series2 .equals(other.series2)) {
                return false;
            }
        }
        if (this.reptMonth == null) {
            if (other.reptMonth!= null) {
                return false;
            }
        } else {
            if (!this.reptMonth.equals(other.reptMonth)) {
                return false;
            }
        }
        if (this.reptDay == null) {
            if (other.reptDay!= null) {
                return false;
            }
        } else {
            if (!this.reptDay.equals(other.reptDay)) {
                return false;
            }
        }
        if (this.reptYear1 == null) {
            if (other.reptYear1 != null) {
                return false;
            }
        } else {
            if (!this.reptYear1 .equals(other.reptYear1)) {
                return false;
            }
        }
        if (this.author1LastName == null) {
            if (other.author1LastName!= null) {
                return false;
            }
        } else {
            if (!this.author1LastName.equals(other.author1LastName)) {
                return false;
            }
        }
        if (this.author1Fi == null) {
            if (other.author1Fi!= null) {
                return false;
            }
        } else {
            if (!this.author1Fi.equals(other.author1Fi)) {
                return false;
            }
        }
        if (this.author1Mi == null) {
            if (other.author1Mi!= null) {
                return false;
            }
        } else {
            if (!this.author1Mi.equals(other.author1Mi)) {
                return false;
            }
        }
        if (this.author2LastName == null) {
            if (other.author2LastName!= null) {
                return false;
            }
        } else {
            if (!this.author2LastName.equals(other.author2LastName)) {
                return false;
            }
        }
        if (this.author2Fi == null) {
            if (other.author2Fi!= null) {
                return false;
            }
        } else {
            if (!this.author2Fi.equals(other.author2Fi)) {
                return false;
            }
        }
        if (this.author2Mi == null) {
            if (other.author2Mi!= null) {
                return false;
            }
        } else {
            if (!this.author2Mi.equals(other.author2Mi)) {
                return false;
            }
        }
        if (this.reportTitle == null) {
            if (other.reportTitle!= null) {
                return false;
            }
        } else {
            if (!this.reportTitle.equals(other.reportTitle)) {
                return false;
            }
        }
        if (this.districtNumber == null) {
            if (other.districtNumber!= null) {
                return false;
            }
        } else {
            if (!this.districtNumber.equals(other.districtNumber)) {
                return false;
            }
        }
        if (this.projFunction == null) {
            if (other.projFunction!= null) {
                return false;
            }
        } else {
            if (!this.projFunction.equals(other.projFunction)) {
                return false;
            }
        }
        if (this.projFunctionDesc == null) {
            if (other.projFunctionDesc!= null) {
                return false;
            }
        } else {
            if (!this.projFunctionDesc.equals(other.projFunctionDesc)) {
                return false;
            }
        }
        if (this.activityType1 == null) {
            if (other.activityType1 != null) {
                return false;
            }
        } else {
            if (!this.activityType1 .equals(other.activityType1)) {
                return false;
            }
        }
        if (this.activityType1Desc == null) {
            if (other.activityType1Desc!= null) {
                return false;
            }
        } else {
            if (!this.activityType1Desc.equals(other.activityType1Desc)) {
                return false;
            }
        }
        if (this.activityType2 == null) {
            if (other.activityType2 != null) {
                return false;
            }
        } else {
            if (!this.activityType2 .equals(other.activityType2)) {
                return false;
            }
        }
        if (this.activityType2Desc == null) {
            if (other.activityType2Desc!= null) {
                return false;
            }
        } else {
            if (!this.activityType2Desc.equals(other.activityType2Desc)) {
                return false;
            }
        }
        if (this.programming == null) {
            if (other.programming!= null) {
                return false;
            }
        } else {
            if (!this.programming.equals(other.programming)) {
                return false;
            }
        }
        if (this.totalProjAcreage == null) {
            if (other.totalProjAcreage!= null) {
                return false;
            }
        } else {
            if (!this.totalProjAcreage.equals(other.totalProjAcreage)) {
                return false;
            }
        }
        if (this.institution == null) {
            if (other.institution!= null) {
                return false;
            }
        } else {
            if (!this.institution.equals(other.institution)) {
                return false;
            }
        }
        if (this.institutionDesc == null) {
            if (other.institutionDesc!= null) {
                return false;
            }
        } else {
            if (!this.institutionDesc.equals(other.institutionDesc)) {
                return false;
            }
        }
        if (this.acreageCompInvent == null) {
            if (other.acreageCompInvent!= null) {
                return false;
            }
        } else {
            if (!this.acreageCompInvent.equals(other.acreageCompInvent)) {
                return false;
            }
        }
        if (this.percentSample == null) {
            if (other.percentSample!= null) {
                return false;
            }
        } else {
            if (!this.percentSample.equals(other.percentSample)) {
                return false;
            }
        }
        if (this.aveNoIndividuals == null) {
            if (other.aveNoIndividuals!= null) {
                return false;
            }
        } else {
            if (!this.aveNoIndividuals.equals(other.aveNoIndividuals)) {
                return false;
            }
        }
        if (this.acreageResurveyed == null) {
            if (other.acreageResurveyed!= null) {
                return false;
            }
        } else {
            if (!this.acreageResurveyed.equals(other.acreageResurveyed)) {
                return false;
            }
        }
        if (this.aveSpacing == null) {
            if (other.aveSpacing!= null) {
                return false;
            }
        } else {
            if (!this.aveSpacing.equals(other.aveSpacing)) {
                return false;
            }
        }
        if (this.totalSites == null) {
            if (other.totalSites!= null) {
                return false;
            }
        } else {
            if (!this.totalSites.equals(other.totalSites)) {
                return false;
            }
        }
        if (this.fieldHours == null) {
            if (other.fieldHours!= null) {
                return false;
            }
        } else {
            if (!this.fieldHours.equals(other.fieldHours)) {
                return false;
            }
        }
        if (this.newSitesLocated == null) {
            if (other.newSitesLocated!= null) {
                return false;
            }
        } else {
            if (!this.newSitesLocated.equals(other.newSitesLocated)) {
                return false;
            }
        }
        if (this.labLibHours == null) {
            if (other.labLibHours!= null) {
                return false;
            }
        } else {
            if (!this.labLibHours.equals(other.labLibHours)) {
                return false;
            }
        }
        if (this.travelHours == null) {
            if (other.travelHours!= null) {
                return false;
            }
        } else {
            if (!this.travelHours.equals(other.travelHours)) {
                return false;
            }
        }
        if (this.sitesEligible == null) {
            if (other.sitesEligible!= null) {
                return false;
            }
        } else {
            if (!this.sitesEligible.equals(other.sitesEligible)) {
                return false;
            }
        }
        if (this.adminHours == null) {
            if (other.adminHours!= null) {
                return false;
            }
        } else {
            if (!this.adminHours.equals(other.adminHours)) {
                return false;
            }
        }
        if (this.sitesNotEligible == null) {
            if (other.sitesNotEligible!= null) {
                return false;
            }
        } else {
            if (!this.sitesNotEligible.equals(other.sitesNotEligible)) {
                return false;
            }
        }
        if (this.mileage == null) {
            if (other.mileage!= null) {
                return false;
            }
        } else {
            if (!this.mileage.equals(other.mileage)) {
                return false;
            }
        }
        if (this.perDiemRate == null) {
            if (other.perDiemRate!= null) {
                return false;
            }
        } else {
            if (!this.perDiemRate.equals(other.perDiemRate)) {
                return false;
            }
        }
        if (this.sitesEnhanced == null) {
            if (other.sitesEnhanced!= null) {
                return false;
            }
        } else {
            if (!this.sitesEnhanced.equals(other.sitesEnhanced)) {
                return false;
            }
        }
        if (this.perDiemDaysPaid == null) {
            if (other.perDiemDaysPaid!= null) {
                return false;
            }
        } else {
            if (!this.perDiemDaysPaid.equals(other.perDiemDaysPaid)) {
                return false;
            }
        }
        if (this.costWeighting == null) {
            if (other.costWeighting!= null) {
                return false;
            }
        } else {
            if (!this.costWeighting.equals(other.costWeighting)) {
                return false;
            }
        }
        if (this.recomDeterEffect == null) {
            if (other.recomDeterEffect!= null) {
                return false;
            }
        } else {
            if (!this.recomDeterEffect.equals(other.recomDeterEffect)) {
                return false;
            }
        }
        if (this.recomDeterEffectDesc == null) {
            if (other.recomDeterEffectDesc!= null) {
                return false;
            }
        } else {
            if (!this.recomDeterEffectDesc.equals(other.recomDeterEffectDesc)) {
                return false;
            }
        }
        if (this.totalCrmCost == null) {
            if (other.totalCrmCost!= null) {
                return false;
            }
        } else {
            if (!this.totalCrmCost.equals(other.totalCrmCost)) {
                return false;
            }
        }
        if (this.actualCrmCost == null) {
            if (other.actualCrmCost!= null) {
                return false;
            }
        } else {
            if (!this.actualCrmCost.equals(other.actualCrmCost)) {
                return false;
            }
        }
        if (this.stateProject == null) {
            if (other.stateProject!= null) {
                return false;
            }
        } else {
            if (!this.stateProject.equals(other.stateProject)) {
                return false;
            }
        }
        if (this.stateProjectNo == null) {
            if (other.stateProjectNo!= null) {
                return false;
            }
        } else {
            if (!this.stateProjectNo.equals(other.stateProjectNo)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.data!= null) {
            rtn = (rtn + this.data.hashCode());
        }
        rtn = (rtn* 37);
        if (this.data1 != null) {
            rtn = (rtn + this.data1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.region!= null) {
            rtn = (rtn + this.region.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptYear!= null) {
            rtn = (rtn + this.reptYear.hashCode());
        }
        rtn = (rtn* 37);
        if (this.forest!= null) {
            rtn = (rtn + this.forest.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptNumber!= null) {
            rtn = (rtn + this.reptNumber.hashCode());
        }
        rtn = (rtn* 37);
        if (this.series1 != null) {
            rtn = (rtn + this.series1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.series2 != null) {
            rtn = (rtn + this.series2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptMonth!= null) {
            rtn = (rtn + this.reptMonth.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptDay!= null) {
            rtn = (rtn + this.reptDay.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptYear1 != null) {
            rtn = (rtn + this.reptYear1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.author1LastName!= null) {
            rtn = (rtn + this.author1LastName.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author1Fi!= null) {
            rtn = (rtn + this.author1Fi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author1Mi!= null) {
            rtn = (rtn + this.author1Mi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author2LastName!= null) {
            rtn = (rtn + this.author2LastName.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author2Fi!= null) {
            rtn = (rtn + this.author2Fi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author2Mi!= null) {
            rtn = (rtn + this.author2Mi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportTitle!= null) {
            rtn = (rtn + this.reportTitle.hashCode());
        }
        rtn = (rtn* 37);
        if (this.districtNumber!= null) {
            rtn = (rtn + this.districtNumber.hashCode());
        }
        rtn = (rtn* 37);
        if (this.projFunction!= null) {
            rtn = (rtn + this.projFunction.hashCode());
        }
        rtn = (rtn* 37);
        if (this.projFunctionDesc!= null) {
            rtn = (rtn + this.projFunctionDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType1 != null) {
            rtn = (rtn + this.activityType1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType1Desc!= null) {
            rtn = (rtn + this.activityType1Desc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType2 != null) {
            rtn = (rtn + this.activityType2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType2Desc!= null) {
            rtn = (rtn + this.activityType2Desc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.programming!= null) {
            rtn = (rtn + this.programming.hashCode());
        }
        rtn = (rtn* 37);
        if (this.totalProjAcreage!= null) {
            rtn = (rtn + this.totalProjAcreage.hashCode());
        }
        rtn = (rtn* 37);
        if (this.institution!= null) {
            rtn = (rtn + this.institution.hashCode());
        }
        rtn = (rtn* 37);
        if (this.institutionDesc!= null) {
            rtn = (rtn + this.institutionDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.acreageCompInvent!= null) {
            rtn = (rtn + this.acreageCompInvent.hashCode());
        }
        rtn = (rtn* 37);
        if (this.percentSample!= null) {
            rtn = (rtn + this.percentSample.hashCode());
        }
        rtn = (rtn* 37);
        if (this.aveNoIndividuals!= null) {
            rtn = (rtn + this.aveNoIndividuals.hashCode());
        }
        rtn = (rtn* 37);
        if (this.acreageResurveyed!= null) {
            rtn = (rtn + this.acreageResurveyed.hashCode());
        }
        rtn = (rtn* 37);
        if (this.aveSpacing!= null) {
            rtn = (rtn + this.aveSpacing.hashCode());
        }
        rtn = (rtn* 37);
        if (this.totalSites!= null) {
            rtn = (rtn + this.totalSites.hashCode());
        }
        rtn = (rtn* 37);
        if (this.fieldHours!= null) {
            rtn = (rtn + this.fieldHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.newSitesLocated!= null) {
            rtn = (rtn + this.newSitesLocated.hashCode());
        }
        rtn = (rtn* 37);
        if (this.labLibHours!= null) {
            rtn = (rtn + this.labLibHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.travelHours!= null) {
            rtn = (rtn + this.travelHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sitesEligible!= null) {
            rtn = (rtn + this.sitesEligible.hashCode());
        }
        rtn = (rtn* 37);
        if (this.adminHours!= null) {
            rtn = (rtn + this.adminHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sitesNotEligible!= null) {
            rtn = (rtn + this.sitesNotEligible.hashCode());
        }
        rtn = (rtn* 37);
        if (this.mileage!= null) {
            rtn = (rtn + this.mileage.hashCode());
        }
        rtn = (rtn* 37);
        if (this.perDiemRate!= null) {
            rtn = (rtn + this.perDiemRate.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sitesEnhanced!= null) {
            rtn = (rtn + this.sitesEnhanced.hashCode());
        }
        rtn = (rtn* 37);
        if (this.perDiemDaysPaid!= null) {
            rtn = (rtn + this.perDiemDaysPaid.hashCode());
        }
        rtn = (rtn* 37);
        if (this.costWeighting!= null) {
            rtn = (rtn + this.costWeighting.hashCode());
        }
        rtn = (rtn* 37);
        if (this.recomDeterEffect!= null) {
            rtn = (rtn + this.recomDeterEffect.hashCode());
        }
        rtn = (rtn* 37);
        if (this.recomDeterEffectDesc!= null) {
            rtn = (rtn + this.recomDeterEffectDesc.hashCode());
        }
        rtn = (rtn* 37);
        if (this.totalCrmCost!= null) {
            rtn = (rtn + this.totalCrmCost.hashCode());
        }
        rtn = (rtn* 37);
        if (this.actualCrmCost!= null) {
            rtn = (rtn + this.actualCrmCost.hashCode());
        }
        rtn = (rtn* 37);
        if (this.stateProject!= null) {
            rtn = (rtn + this.stateProject.hashCode());
        }
        rtn = (rtn* 37);
        if (this.stateProjectNo!= null) {
            rtn = (rtn + this.stateProjectNo.hashCode());
        }
        return rtn;
    }

    public Date getData() {
        return data;
    }

    public void setData(Date data) {
        this.data = data;
    }

    public Date getData1() {
        return data1;
    }

    public void setData1(Date data1) {
        this.data1 = data1;
    }

    public Date getRegion() {
        return region;
    }

    public void setRegion(Date region) {
        this.region = region;
    }

    public Date getReptYear() {
        return reptYear;
    }

    public void setReptYear(Date reptYear) {
        this.reptYear = reptYear;
    }

    public Date getForest() {
        return forest;
    }

    public void setForest(Date forest) {
        this.forest = forest;
    }

    public Date getReptNumber() {
        return reptNumber;
    }

    public void setReptNumber(Date reptNumber) {
        this.reptNumber = reptNumber;
    }

    public Date getSeries1() {
        return series1;
    }

    public void setSeries1(Date series1) {
        this.series1 = series1;
    }

    public Date getSeries2() {
        return series2;
    }

    public void setSeries2(Date series2) {
        this.series2 = series2;
    }

    public Date getReptMonth() {
        return reptMonth;
    }

    public void setReptMonth(Date reptMonth) {
        this.reptMonth = reptMonth;
    }

    public Date getReptDay() {
        return reptDay;
    }

    public void setReptDay(Date reptDay) {
        this.reptDay = reptDay;
    }

    public Date getReptYear1() {
        return reptYear1;
    }

    public void setReptYear1(Date reptYear1) {
        this.reptYear1 = reptYear1;
    }

    public Date getAuthor1LastName() {
        return author1LastName;
    }

    public void setAuthor1LastName(Date author1LastName) {
        this.author1LastName = author1LastName;
    }

    public Date getAuthor1Fi() {
        return author1Fi;
    }

    public void setAuthor1Fi(Date author1Fi) {
        this.author1Fi = author1Fi;
    }

    public Date getAuthor1Mi() {
        return author1Mi;
    }

    public void setAuthor1Mi(Date author1Mi) {
        this.author1Mi = author1Mi;
    }

    public Date getAuthor2LastName() {
        return author2LastName;
    }

    public void setAuthor2LastName(Date author2LastName) {
        this.author2LastName = author2LastName;
    }

    public Date getAuthor2Fi() {
        return author2Fi;
    }

    public void setAuthor2Fi(Date author2Fi) {
        this.author2Fi = author2Fi;
    }

    public Date getAuthor2Mi() {
        return author2Mi;
    }

    public void setAuthor2Mi(Date author2Mi) {
        this.author2Mi = author2Mi;
    }

    public Date getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(Date reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Date getDistrictNumber() {
        return districtNumber;
    }

    public void setDistrictNumber(Date districtNumber) {
        this.districtNumber = districtNumber;
    }

    public Date getProjFunction() {
        return projFunction;
    }

    public void setProjFunction(Date projFunction) {
        this.projFunction = projFunction;
    }

    public Date getProjFunctionDesc() {
        return projFunctionDesc;
    }

    public void setProjFunctionDesc(Date projFunctionDesc) {
        this.projFunctionDesc = projFunctionDesc;
    }

    public Date getActivityType1() {
        return activityType1;
    }

    public void setActivityType1(Date activityType1) {
        this.activityType1 = activityType1;
    }

    public Date getActivityType1Desc() {
        return activityType1Desc;
    }

    public void setActivityType1Desc(Date activityType1Desc) {
        this.activityType1Desc = activityType1Desc;
    }

    public Date getActivityType2() {
        return activityType2;
    }

    public void setActivityType2(Date activityType2) {
        this.activityType2 = activityType2;
    }

    public Date getActivityType2Desc() {
        return activityType2Desc;
    }

    public void setActivityType2Desc(Date activityType2Desc) {
        this.activityType2Desc = activityType2Desc;
    }

    public Date getProgramming() {
        return programming;
    }

    public void setProgramming(Date programming) {
        this.programming = programming;
    }

    public Date getTotalProjAcreage() {
        return totalProjAcreage;
    }

    public void setTotalProjAcreage(Date totalProjAcreage) {
        this.totalProjAcreage = totalProjAcreage;
    }

    public Date getInstitution() {
        return institution;
    }

    public void setInstitution(Date institution) {
        this.institution = institution;
    }

    public Date getInstitutionDesc() {
        return institutionDesc;
    }

    public void setInstitutionDesc(Date institutionDesc) {
        this.institutionDesc = institutionDesc;
    }

    public Date getAcreageCompInvent() {
        return acreageCompInvent;
    }

    public void setAcreageCompInvent(Date acreageCompInvent) {
        this.acreageCompInvent = acreageCompInvent;
    }

    public Date getPercentSample() {
        return percentSample;
    }

    public void setPercentSample(Date percentSample) {
        this.percentSample = percentSample;
    }

    public Date getAveNoIndividuals() {
        return aveNoIndividuals;
    }

    public void setAveNoIndividuals(Date aveNoIndividuals) {
        this.aveNoIndividuals = aveNoIndividuals;
    }

    public Date getAcreageResurveyed() {
        return acreageResurveyed;
    }

    public void setAcreageResurveyed(Date acreageResurveyed) {
        this.acreageResurveyed = acreageResurveyed;
    }

    public Date getAveSpacing() {
        return aveSpacing;
    }

    public void setAveSpacing(Date aveSpacing) {
        this.aveSpacing = aveSpacing;
    }

    public Date getTotalSites() {
        return totalSites;
    }

    public void setTotalSites(Date totalSites) {
        this.totalSites = totalSites;
    }

    public Date getFieldHours() {
        return fieldHours;
    }

    public void setFieldHours(Date fieldHours) {
        this.fieldHours = fieldHours;
    }

    public Date getNewSitesLocated() {
        return newSitesLocated;
    }

    public void setNewSitesLocated(Date newSitesLocated) {
        this.newSitesLocated = newSitesLocated;
    }

    public Date getLabLibHours() {
        return labLibHours;
    }

    public void setLabLibHours(Date labLibHours) {
        this.labLibHours = labLibHours;
    }

    public Date getTravelHours() {
        return travelHours;
    }

    public void setTravelHours(Date travelHours) {
        this.travelHours = travelHours;
    }

    public Date getSitesEligible() {
        return sitesEligible;
    }

    public void setSitesEligible(Date sitesEligible) {
        this.sitesEligible = sitesEligible;
    }

    public Date getAdminHours() {
        return adminHours;
    }

    public void setAdminHours(Date adminHours) {
        this.adminHours = adminHours;
    }

    public Date getSitesNotEligible() {
        return sitesNotEligible;
    }

    public void setSitesNotEligible(Date sitesNotEligible) {
        this.sitesNotEligible = sitesNotEligible;
    }

    public Date getMileage() {
        return mileage;
    }

    public void setMileage(Date mileage) {
        this.mileage = mileage;
    }

    public Date getPerDiemRate() {
        return perDiemRate;
    }

    public void setPerDiemRate(Date perDiemRate) {
        this.perDiemRate = perDiemRate;
    }

    public Date getSitesEnhanced() {
        return sitesEnhanced;
    }

    public void setSitesEnhanced(Date sitesEnhanced) {
        this.sitesEnhanced = sitesEnhanced;
    }

    public Date getPerDiemDaysPaid() {
        return perDiemDaysPaid;
    }

    public void setPerDiemDaysPaid(Date perDiemDaysPaid) {
        this.perDiemDaysPaid = perDiemDaysPaid;
    }

    public Date getCostWeighting() {
        return costWeighting;
    }

    public void setCostWeighting(Date costWeighting) {
        this.costWeighting = costWeighting;
    }

    public Date getRecomDeterEffect() {
        return recomDeterEffect;
    }

    public void setRecomDeterEffect(Date recomDeterEffect) {
        this.recomDeterEffect = recomDeterEffect;
    }

    public Date getRecomDeterEffectDesc() {
        return recomDeterEffectDesc;
    }

    public void setRecomDeterEffectDesc(Date recomDeterEffectDesc) {
        this.recomDeterEffectDesc = recomDeterEffectDesc;
    }

    public Date getTotalCrmCost() {
        return totalCrmCost;
    }

    public void setTotalCrmCost(Date totalCrmCost) {
        this.totalCrmCost = totalCrmCost;
    }

    public Date getActualCrmCost() {
        return actualCrmCost;
    }

    public void setActualCrmCost(Date actualCrmCost) {
        this.actualCrmCost = actualCrmCost;
    }

    public Date getStateProject() {
        return stateProject;
    }

    public void setStateProject(Date stateProject) {
        this.stateProject = stateProject;
    }

    public Date getStateProjectNo() {
        return stateProjectNo;
    }

    public void setStateProjectNo(Date stateProjectNo) {
        this.stateProjectNo = stateProjectNo;
    }

}
