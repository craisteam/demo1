
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisStates
 *  12/08/2013 22:45:19
 * 
 */
public class CraisStates {

    private Byte code;
    private String state;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisCounties> craisCountieses = new HashSet<com.orcldb.data.CraisCounties>();
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcldb.data.CraisIstandardsRecord>();

    public Byte getCode() {
        return code;
    }

    public void setCode(Byte code) {
        this.code = code;
    }

    public String getState() {
        return state;
    }

    public void setState(String state) {
        this.state = state;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

    public Set<com.orcldb.data.CraisCounties> getCraisCountieses() {
        return craisCountieses;
    }

    public void setCraisCountieses(Set<com.orcldb.data.CraisCounties> craisCountieses) {
        this.craisCountieses = craisCountieses;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

}
