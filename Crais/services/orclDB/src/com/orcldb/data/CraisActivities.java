
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisActivities
 *  12/08/2013 22:45:19
 * 
 */
public class CraisActivities {

    private String code;
    private String description;
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType2 = new HashSet<com.orcldb.data.CraisIstandardsRecord>();
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType1 = new HashSet<com.orcldb.data.CraisIstandardsRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecordsForActivityType2() {
        return craisIstandardsRecordsForActivityType2;
    }

    public void setCraisIstandardsRecordsForActivityType2(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType2) {
        this.craisIstandardsRecordsForActivityType2 = craisIstandardsRecordsForActivityType2;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecordsForActivityType1() {
        return craisIstandardsRecordsForActivityType1;
    }

    public void setCraisIstandardsRecordsForActivityType1(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecordsForActivityType1) {
        this.craisIstandardsRecordsForActivityType1 = craisIstandardsRecordsForActivityType1;
    }

}
