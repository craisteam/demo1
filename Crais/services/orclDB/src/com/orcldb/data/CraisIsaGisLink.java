
package com.orcldb.data;



/**
 *  orclDB.CraisIsaGisLink
 *  12/08/2013 22:45:19
 * 
 */
public class CraisIsaGisLink {

    private CraisIsaGisLinkId id;

    public CraisIsaGisLinkId getId() {
        return id;
    }

    public void setId(CraisIsaGisLinkId id) {
        this.id = id;
    }

}
