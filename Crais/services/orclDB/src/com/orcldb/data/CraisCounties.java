
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisCounties
 *  12/08/2013 22:45:19
 * 
 */
public class CraisCounties {

    private CraisCountiesId id;
    private CraisStates craisStates;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords = new HashSet<com.orcldb.data.CraisSitefileRecord>();

    public CraisCountiesId getId() {
        return id;
    }

    public void setId(CraisCountiesId id) {
        this.id = id;
    }

    public CraisStates getCraisStates() {
        return craisStates;
    }

    public void setCraisStates(CraisStates craisStates) {
        this.craisStates = craisStates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecords() {
        return craisSitefileRecords;
    }

    public void setCraisSitefileRecords(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecords) {
        this.craisSitefileRecords = craisSitefileRecords;
    }

}
