
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisDirection
 *  12/08/2013 22:45:20
 * 
 */
public class CraisDirection {

    private String code;
    private String description;
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForLandDirection = new HashSet<com.orcldb.data.CraisSitefileRecord>();
    private Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForWaterDirection = new HashSet<com.orcldb.data.CraisSitefileRecord>();

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForLandDirection() {
        return craisSitefileRecordsForLandDirection;
    }

    public void setCraisSitefileRecordsForLandDirection(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForLandDirection) {
        this.craisSitefileRecordsForLandDirection = craisSitefileRecordsForLandDirection;
    }

    public Set<com.orcldb.data.CraisSitefileRecord> getCraisSitefileRecordsForWaterDirection() {
        return craisSitefileRecordsForWaterDirection;
    }

    public void setCraisSitefileRecordsForWaterDirection(Set<com.orcldb.data.CraisSitefileRecord> craisSitefileRecordsForWaterDirection) {
        this.craisSitefileRecordsForWaterDirection = craisSitefileRecordsForWaterDirection;
    }

}
