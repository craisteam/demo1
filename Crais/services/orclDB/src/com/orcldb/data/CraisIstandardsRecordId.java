
package com.orcldb.data;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;


/**
 *  orclDB.CraisIstandardsRecordId
 *  12/08/2013 22:45:20
 * 
 */
public class CraisIstandardsRecordId
    implements Serializable
{

    private Byte region;
    private Short reptYear;
    private Byte forest;
    private Short reptNumber;
    private String series1;
    private String series2;
    private Byte reptMonth;
    private Byte reptDay;
    private Short reptYear1;
    private String author1LastName;
    private String author1Fi;
    private String author1Mi;
    private String author2LastName;
    private String author2Fi;
    private String author2Mi;
    private String reportTitle;
    private Byte districtNumber;
    private Byte projFunction;
    private String activityType1;
    private String activityType2;
    private String programming;
    private Integer totalProjAcreage;
    private String institution;
    private Integer acreageCompInvent;
    private Byte percentSample;
    private Byte aveNoIndividuals;
    private Integer acreageResurveyed;
    private Short aveSpacing;
    private Short totalSites;
    private Short fieldHours;
    private Short newSitesLocated;
    private Short labLibHours;
    private Short travelHours;
    private Short sitesEligible;
    private Short adminHours;
    private Short sitesNotEligible;
    private Short mileage;
    private String perDiemRate;
    private Short sitesEnhanced;
    private Byte perDiemDaysPaid;
    private Boolean costWeighting;
    private Boolean recomDeterEffect;
    private String totalCrmCost;
    private Integer actualCrmCost;
    private Byte stateProject;
    private Integer stateProjectNo;
    private String createdBy;
    private Date createdDate;
    private BigDecimal createdInInstance;
    private String modifiedBy;
    private Date modifiedDate;
    private BigDecimal modifiedInInstance;

    public boolean equals(Object o) {
        if (o == this) {
            return true;
        }
        if (!(o instanceof CraisIstandardsRecordId)) {
            return false;
        }
        CraisIstandardsRecordId other = ((CraisIstandardsRecordId) o);
        if (this.region == null) {
            if (other.region!= null) {
                return false;
            }
        } else {
            if (!this.region.equals(other.region)) {
                return false;
            }
        }
        if (this.reptYear == null) {
            if (other.reptYear!= null) {
                return false;
            }
        } else {
            if (!this.reptYear.equals(other.reptYear)) {
                return false;
            }
        }
        if (this.forest == null) {
            if (other.forest!= null) {
                return false;
            }
        } else {
            if (!this.forest.equals(other.forest)) {
                return false;
            }
        }
        if (this.reptNumber == null) {
            if (other.reptNumber!= null) {
                return false;
            }
        } else {
            if (!this.reptNumber.equals(other.reptNumber)) {
                return false;
            }
        }
        if (this.series1 == null) {
            if (other.series1 != null) {
                return false;
            }
        } else {
            if (!this.series1 .equals(other.series1)) {
                return false;
            }
        }
        if (this.series2 == null) {
            if (other.series2 != null) {
                return false;
            }
        } else {
            if (!this.series2 .equals(other.series2)) {
                return false;
            }
        }
        if (this.reptMonth == null) {
            if (other.reptMonth!= null) {
                return false;
            }
        } else {
            if (!this.reptMonth.equals(other.reptMonth)) {
                return false;
            }
        }
        if (this.reptDay == null) {
            if (other.reptDay!= null) {
                return false;
            }
        } else {
            if (!this.reptDay.equals(other.reptDay)) {
                return false;
            }
        }
        if (this.reptYear1 == null) {
            if (other.reptYear1 != null) {
                return false;
            }
        } else {
            if (!this.reptYear1 .equals(other.reptYear1)) {
                return false;
            }
        }
        if (this.author1LastName == null) {
            if (other.author1LastName!= null) {
                return false;
            }
        } else {
            if (!this.author1LastName.equals(other.author1LastName)) {
                return false;
            }
        }
        if (this.author1Fi == null) {
            if (other.author1Fi!= null) {
                return false;
            }
        } else {
            if (!this.author1Fi.equals(other.author1Fi)) {
                return false;
            }
        }
        if (this.author1Mi == null) {
            if (other.author1Mi!= null) {
                return false;
            }
        } else {
            if (!this.author1Mi.equals(other.author1Mi)) {
                return false;
            }
        }
        if (this.author2LastName == null) {
            if (other.author2LastName!= null) {
                return false;
            }
        } else {
            if (!this.author2LastName.equals(other.author2LastName)) {
                return false;
            }
        }
        if (this.author2Fi == null) {
            if (other.author2Fi!= null) {
                return false;
            }
        } else {
            if (!this.author2Fi.equals(other.author2Fi)) {
                return false;
            }
        }
        if (this.author2Mi == null) {
            if (other.author2Mi!= null) {
                return false;
            }
        } else {
            if (!this.author2Mi.equals(other.author2Mi)) {
                return false;
            }
        }
        if (this.reportTitle == null) {
            if (other.reportTitle!= null) {
                return false;
            }
        } else {
            if (!this.reportTitle.equals(other.reportTitle)) {
                return false;
            }
        }
        if (this.districtNumber == null) {
            if (other.districtNumber!= null) {
                return false;
            }
        } else {
            if (!this.districtNumber.equals(other.districtNumber)) {
                return false;
            }
        }
        if (this.projFunction == null) {
            if (other.projFunction!= null) {
                return false;
            }
        } else {
            if (!this.projFunction.equals(other.projFunction)) {
                return false;
            }
        }
        if (this.activityType1 == null) {
            if (other.activityType1 != null) {
                return false;
            }
        } else {
            if (!this.activityType1 .equals(other.activityType1)) {
                return false;
            }
        }
        if (this.activityType2 == null) {
            if (other.activityType2 != null) {
                return false;
            }
        } else {
            if (!this.activityType2 .equals(other.activityType2)) {
                return false;
            }
        }
        if (this.programming == null) {
            if (other.programming!= null) {
                return false;
            }
        } else {
            if (!this.programming.equals(other.programming)) {
                return false;
            }
        }
        if (this.totalProjAcreage == null) {
            if (other.totalProjAcreage!= null) {
                return false;
            }
        } else {
            if (!this.totalProjAcreage.equals(other.totalProjAcreage)) {
                return false;
            }
        }
        if (this.institution == null) {
            if (other.institution!= null) {
                return false;
            }
        } else {
            if (!this.institution.equals(other.institution)) {
                return false;
            }
        }
        if (this.acreageCompInvent == null) {
            if (other.acreageCompInvent!= null) {
                return false;
            }
        } else {
            if (!this.acreageCompInvent.equals(other.acreageCompInvent)) {
                return false;
            }
        }
        if (this.percentSample == null) {
            if (other.percentSample!= null) {
                return false;
            }
        } else {
            if (!this.percentSample.equals(other.percentSample)) {
                return false;
            }
        }
        if (this.aveNoIndividuals == null) {
            if (other.aveNoIndividuals!= null) {
                return false;
            }
        } else {
            if (!this.aveNoIndividuals.equals(other.aveNoIndividuals)) {
                return false;
            }
        }
        if (this.acreageResurveyed == null) {
            if (other.acreageResurveyed!= null) {
                return false;
            }
        } else {
            if (!this.acreageResurveyed.equals(other.acreageResurveyed)) {
                return false;
            }
        }
        if (this.aveSpacing == null) {
            if (other.aveSpacing!= null) {
                return false;
            }
        } else {
            if (!this.aveSpacing.equals(other.aveSpacing)) {
                return false;
            }
        }
        if (this.totalSites == null) {
            if (other.totalSites!= null) {
                return false;
            }
        } else {
            if (!this.totalSites.equals(other.totalSites)) {
                return false;
            }
        }
        if (this.fieldHours == null) {
            if (other.fieldHours!= null) {
                return false;
            }
        } else {
            if (!this.fieldHours.equals(other.fieldHours)) {
                return false;
            }
        }
        if (this.newSitesLocated == null) {
            if (other.newSitesLocated!= null) {
                return false;
            }
        } else {
            if (!this.newSitesLocated.equals(other.newSitesLocated)) {
                return false;
            }
        }
        if (this.labLibHours == null) {
            if (other.labLibHours!= null) {
                return false;
            }
        } else {
            if (!this.labLibHours.equals(other.labLibHours)) {
                return false;
            }
        }
        if (this.travelHours == null) {
            if (other.travelHours!= null) {
                return false;
            }
        } else {
            if (!this.travelHours.equals(other.travelHours)) {
                return false;
            }
        }
        if (this.sitesEligible == null) {
            if (other.sitesEligible!= null) {
                return false;
            }
        } else {
            if (!this.sitesEligible.equals(other.sitesEligible)) {
                return false;
            }
        }
        if (this.adminHours == null) {
            if (other.adminHours!= null) {
                return false;
            }
        } else {
            if (!this.adminHours.equals(other.adminHours)) {
                return false;
            }
        }
        if (this.sitesNotEligible == null) {
            if (other.sitesNotEligible!= null) {
                return false;
            }
        } else {
            if (!this.sitesNotEligible.equals(other.sitesNotEligible)) {
                return false;
            }
        }
        if (this.mileage == null) {
            if (other.mileage!= null) {
                return false;
            }
        } else {
            if (!this.mileage.equals(other.mileage)) {
                return false;
            }
        }
        if (this.perDiemRate == null) {
            if (other.perDiemRate!= null) {
                return false;
            }
        } else {
            if (!this.perDiemRate.equals(other.perDiemRate)) {
                return false;
            }
        }
        if (this.sitesEnhanced == null) {
            if (other.sitesEnhanced!= null) {
                return false;
            }
        } else {
            if (!this.sitesEnhanced.equals(other.sitesEnhanced)) {
                return false;
            }
        }
        if (this.perDiemDaysPaid == null) {
            if (other.perDiemDaysPaid!= null) {
                return false;
            }
        } else {
            if (!this.perDiemDaysPaid.equals(other.perDiemDaysPaid)) {
                return false;
            }
        }
        if (this.costWeighting == null) {
            if (other.costWeighting!= null) {
                return false;
            }
        } else {
            if (!this.costWeighting.equals(other.costWeighting)) {
                return false;
            }
        }
        if (this.recomDeterEffect == null) {
            if (other.recomDeterEffect!= null) {
                return false;
            }
        } else {
            if (!this.recomDeterEffect.equals(other.recomDeterEffect)) {
                return false;
            }
        }
        if (this.totalCrmCost == null) {
            if (other.totalCrmCost!= null) {
                return false;
            }
        } else {
            if (!this.totalCrmCost.equals(other.totalCrmCost)) {
                return false;
            }
        }
        if (this.actualCrmCost == null) {
            if (other.actualCrmCost!= null) {
                return false;
            }
        } else {
            if (!this.actualCrmCost.equals(other.actualCrmCost)) {
                return false;
            }
        }
        if (this.stateProject == null) {
            if (other.stateProject!= null) {
                return false;
            }
        } else {
            if (!this.stateProject.equals(other.stateProject)) {
                return false;
            }
        }
        if (this.stateProjectNo == null) {
            if (other.stateProjectNo!= null) {
                return false;
            }
        } else {
            if (!this.stateProjectNo.equals(other.stateProjectNo)) {
                return false;
            }
        }
        if (this.createdBy == null) {
            if (other.createdBy!= null) {
                return false;
            }
        } else {
            if (!this.createdBy.equals(other.createdBy)) {
                return false;
            }
        }
        if (this.createdDate == null) {
            if (other.createdDate!= null) {
                return false;
            }
        } else {
            if (!this.createdDate.equals(other.createdDate)) {
                return false;
            }
        }
        if (this.createdInInstance == null) {
            if (other.createdInInstance!= null) {
                return false;
            }
        } else {
            if (!this.createdInInstance.equals(other.createdInInstance)) {
                return false;
            }
        }
        if (this.modifiedBy == null) {
            if (other.modifiedBy!= null) {
                return false;
            }
        } else {
            if (!this.modifiedBy.equals(other.modifiedBy)) {
                return false;
            }
        }
        if (this.modifiedDate == null) {
            if (other.modifiedDate!= null) {
                return false;
            }
        } else {
            if (!this.modifiedDate.equals(other.modifiedDate)) {
                return false;
            }
        }
        if (this.modifiedInInstance == null) {
            if (other.modifiedInInstance!= null) {
                return false;
            }
        } else {
            if (!this.modifiedInInstance.equals(other.modifiedInInstance)) {
                return false;
            }
        }
        return true;
    }

    public int hashCode() {
        int rtn = 17;
        rtn = (rtn* 37);
        if (this.region!= null) {
            rtn = (rtn + this.region.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptYear!= null) {
            rtn = (rtn + this.reptYear.hashCode());
        }
        rtn = (rtn* 37);
        if (this.forest!= null) {
            rtn = (rtn + this.forest.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptNumber!= null) {
            rtn = (rtn + this.reptNumber.hashCode());
        }
        rtn = (rtn* 37);
        if (this.series1 != null) {
            rtn = (rtn + this.series1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.series2 != null) {
            rtn = (rtn + this.series2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptMonth!= null) {
            rtn = (rtn + this.reptMonth.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptDay!= null) {
            rtn = (rtn + this.reptDay.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reptYear1 != null) {
            rtn = (rtn + this.reptYear1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.author1LastName!= null) {
            rtn = (rtn + this.author1LastName.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author1Fi!= null) {
            rtn = (rtn + this.author1Fi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author1Mi!= null) {
            rtn = (rtn + this.author1Mi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author2LastName!= null) {
            rtn = (rtn + this.author2LastName.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author2Fi!= null) {
            rtn = (rtn + this.author2Fi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.author2Mi!= null) {
            rtn = (rtn + this.author2Mi.hashCode());
        }
        rtn = (rtn* 37);
        if (this.reportTitle!= null) {
            rtn = (rtn + this.reportTitle.hashCode());
        }
        rtn = (rtn* 37);
        if (this.districtNumber!= null) {
            rtn = (rtn + this.districtNumber.hashCode());
        }
        rtn = (rtn* 37);
        if (this.projFunction!= null) {
            rtn = (rtn + this.projFunction.hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType1 != null) {
            rtn = (rtn + this.activityType1 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.activityType2 != null) {
            rtn = (rtn + this.activityType2 .hashCode());
        }
        rtn = (rtn* 37);
        if (this.programming!= null) {
            rtn = (rtn + this.programming.hashCode());
        }
        rtn = (rtn* 37);
        if (this.totalProjAcreage!= null) {
            rtn = (rtn + this.totalProjAcreage.hashCode());
        }
        rtn = (rtn* 37);
        if (this.institution!= null) {
            rtn = (rtn + this.institution.hashCode());
        }
        rtn = (rtn* 37);
        if (this.acreageCompInvent!= null) {
            rtn = (rtn + this.acreageCompInvent.hashCode());
        }
        rtn = (rtn* 37);
        if (this.percentSample!= null) {
            rtn = (rtn + this.percentSample.hashCode());
        }
        rtn = (rtn* 37);
        if (this.aveNoIndividuals!= null) {
            rtn = (rtn + this.aveNoIndividuals.hashCode());
        }
        rtn = (rtn* 37);
        if (this.acreageResurveyed!= null) {
            rtn = (rtn + this.acreageResurveyed.hashCode());
        }
        rtn = (rtn* 37);
        if (this.aveSpacing!= null) {
            rtn = (rtn + this.aveSpacing.hashCode());
        }
        rtn = (rtn* 37);
        if (this.totalSites!= null) {
            rtn = (rtn + this.totalSites.hashCode());
        }
        rtn = (rtn* 37);
        if (this.fieldHours!= null) {
            rtn = (rtn + this.fieldHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.newSitesLocated!= null) {
            rtn = (rtn + this.newSitesLocated.hashCode());
        }
        rtn = (rtn* 37);
        if (this.labLibHours!= null) {
            rtn = (rtn + this.labLibHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.travelHours!= null) {
            rtn = (rtn + this.travelHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sitesEligible!= null) {
            rtn = (rtn + this.sitesEligible.hashCode());
        }
        rtn = (rtn* 37);
        if (this.adminHours!= null) {
            rtn = (rtn + this.adminHours.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sitesNotEligible!= null) {
            rtn = (rtn + this.sitesNotEligible.hashCode());
        }
        rtn = (rtn* 37);
        if (this.mileage!= null) {
            rtn = (rtn + this.mileage.hashCode());
        }
        rtn = (rtn* 37);
        if (this.perDiemRate!= null) {
            rtn = (rtn + this.perDiemRate.hashCode());
        }
        rtn = (rtn* 37);
        if (this.sitesEnhanced!= null) {
            rtn = (rtn + this.sitesEnhanced.hashCode());
        }
        rtn = (rtn* 37);
        if (this.perDiemDaysPaid!= null) {
            rtn = (rtn + this.perDiemDaysPaid.hashCode());
        }
        rtn = (rtn* 37);
        if (this.costWeighting!= null) {
            rtn = (rtn + this.costWeighting.hashCode());
        }
        rtn = (rtn* 37);
        if (this.recomDeterEffect!= null) {
            rtn = (rtn + this.recomDeterEffect.hashCode());
        }
        rtn = (rtn* 37);
        if (this.totalCrmCost!= null) {
            rtn = (rtn + this.totalCrmCost.hashCode());
        }
        rtn = (rtn* 37);
        if (this.actualCrmCost!= null) {
            rtn = (rtn + this.actualCrmCost.hashCode());
        }
        rtn = (rtn* 37);
        if (this.stateProject!= null) {
            rtn = (rtn + this.stateProject.hashCode());
        }
        rtn = (rtn* 37);
        if (this.stateProjectNo!= null) {
            rtn = (rtn + this.stateProjectNo.hashCode());
        }
        rtn = (rtn* 37);
        if (this.createdBy!= null) {
            rtn = (rtn + this.createdBy.hashCode());
        }
        rtn = (rtn* 37);
        if (this.createdDate!= null) {
            rtn = (rtn + this.createdDate.hashCode());
        }
        rtn = (rtn* 37);
        if (this.createdInInstance!= null) {
            rtn = (rtn + this.createdInInstance.hashCode());
        }
        rtn = (rtn* 37);
        if (this.modifiedBy!= null) {
            rtn = (rtn + this.modifiedBy.hashCode());
        }
        rtn = (rtn* 37);
        if (this.modifiedDate!= null) {
            rtn = (rtn + this.modifiedDate.hashCode());
        }
        rtn = (rtn* 37);
        if (this.modifiedInInstance!= null) {
            rtn = (rtn + this.modifiedInInstance.hashCode());
        }
        return rtn;
    }

    public Byte getRegion() {
        return region;
    }

    public void setRegion(Byte region) {
        this.region = region;
    }

    public Short getReptYear() {
        return reptYear;
    }

    public void setReptYear(Short reptYear) {
        this.reptYear = reptYear;
    }

    public Byte getForest() {
        return forest;
    }

    public void setForest(Byte forest) {
        this.forest = forest;
    }

    public Short getReptNumber() {
        return reptNumber;
    }

    public void setReptNumber(Short reptNumber) {
        this.reptNumber = reptNumber;
    }

    public String getSeries1() {
        return series1;
    }

    public void setSeries1(String series1) {
        this.series1 = series1;
    }

    public String getSeries2() {
        return series2;
    }

    public void setSeries2(String series2) {
        this.series2 = series2;
    }

    public Byte getReptMonth() {
        return reptMonth;
    }

    public void setReptMonth(Byte reptMonth) {
        this.reptMonth = reptMonth;
    }

    public Byte getReptDay() {
        return reptDay;
    }

    public void setReptDay(Byte reptDay) {
        this.reptDay = reptDay;
    }

    public Short getReptYear1() {
        return reptYear1;
    }

    public void setReptYear1(Short reptYear1) {
        this.reptYear1 = reptYear1;
    }

    public String getAuthor1LastName() {
        return author1LastName;
    }

    public void setAuthor1LastName(String author1LastName) {
        this.author1LastName = author1LastName;
    }

    public String getAuthor1Fi() {
        return author1Fi;
    }

    public void setAuthor1Fi(String author1Fi) {
        this.author1Fi = author1Fi;
    }

    public String getAuthor1Mi() {
        return author1Mi;
    }

    public void setAuthor1Mi(String author1Mi) {
        this.author1Mi = author1Mi;
    }

    public String getAuthor2LastName() {
        return author2LastName;
    }

    public void setAuthor2LastName(String author2LastName) {
        this.author2LastName = author2LastName;
    }

    public String getAuthor2Fi() {
        return author2Fi;
    }

    public void setAuthor2Fi(String author2Fi) {
        this.author2Fi = author2Fi;
    }

    public String getAuthor2Mi() {
        return author2Mi;
    }

    public void setAuthor2Mi(String author2Mi) {
        this.author2Mi = author2Mi;
    }

    public String getReportTitle() {
        return reportTitle;
    }

    public void setReportTitle(String reportTitle) {
        this.reportTitle = reportTitle;
    }

    public Byte getDistrictNumber() {
        return districtNumber;
    }

    public void setDistrictNumber(Byte districtNumber) {
        this.districtNumber = districtNumber;
    }

    public Byte getProjFunction() {
        return projFunction;
    }

    public void setProjFunction(Byte projFunction) {
        this.projFunction = projFunction;
    }

    public String getActivityType1() {
        return activityType1;
    }

    public void setActivityType1(String activityType1) {
        this.activityType1 = activityType1;
    }

    public String getActivityType2() {
        return activityType2;
    }

    public void setActivityType2(String activityType2) {
        this.activityType2 = activityType2;
    }

    public String getProgramming() {
        return programming;
    }

    public void setProgramming(String programming) {
        this.programming = programming;
    }

    public Integer getTotalProjAcreage() {
        return totalProjAcreage;
    }

    public void setTotalProjAcreage(Integer totalProjAcreage) {
        this.totalProjAcreage = totalProjAcreage;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Integer getAcreageCompInvent() {
        return acreageCompInvent;
    }

    public void setAcreageCompInvent(Integer acreageCompInvent) {
        this.acreageCompInvent = acreageCompInvent;
    }

    public Byte getPercentSample() {
        return percentSample;
    }

    public void setPercentSample(Byte percentSample) {
        this.percentSample = percentSample;
    }

    public Byte getAveNoIndividuals() {
        return aveNoIndividuals;
    }

    public void setAveNoIndividuals(Byte aveNoIndividuals) {
        this.aveNoIndividuals = aveNoIndividuals;
    }

    public Integer getAcreageResurveyed() {
        return acreageResurveyed;
    }

    public void setAcreageResurveyed(Integer acreageResurveyed) {
        this.acreageResurveyed = acreageResurveyed;
    }

    public Short getAveSpacing() {
        return aveSpacing;
    }

    public void setAveSpacing(Short aveSpacing) {
        this.aveSpacing = aveSpacing;
    }

    public Short getTotalSites() {
        return totalSites;
    }

    public void setTotalSites(Short totalSites) {
        this.totalSites = totalSites;
    }

    public Short getFieldHours() {
        return fieldHours;
    }

    public void setFieldHours(Short fieldHours) {
        this.fieldHours = fieldHours;
    }

    public Short getNewSitesLocated() {
        return newSitesLocated;
    }

    public void setNewSitesLocated(Short newSitesLocated) {
        this.newSitesLocated = newSitesLocated;
    }

    public Short getLabLibHours() {
        return labLibHours;
    }

    public void setLabLibHours(Short labLibHours) {
        this.labLibHours = labLibHours;
    }

    public Short getTravelHours() {
        return travelHours;
    }

    public void setTravelHours(Short travelHours) {
        this.travelHours = travelHours;
    }

    public Short getSitesEligible() {
        return sitesEligible;
    }

    public void setSitesEligible(Short sitesEligible) {
        this.sitesEligible = sitesEligible;
    }

    public Short getAdminHours() {
        return adminHours;
    }

    public void setAdminHours(Short adminHours) {
        this.adminHours = adminHours;
    }

    public Short getSitesNotEligible() {
        return sitesNotEligible;
    }

    public void setSitesNotEligible(Short sitesNotEligible) {
        this.sitesNotEligible = sitesNotEligible;
    }

    public Short getMileage() {
        return mileage;
    }

    public void setMileage(Short mileage) {
        this.mileage = mileage;
    }

    public String getPerDiemRate() {
        return perDiemRate;
    }

    public void setPerDiemRate(String perDiemRate) {
        this.perDiemRate = perDiemRate;
    }

    public Short getSitesEnhanced() {
        return sitesEnhanced;
    }

    public void setSitesEnhanced(Short sitesEnhanced) {
        this.sitesEnhanced = sitesEnhanced;
    }

    public Byte getPerDiemDaysPaid() {
        return perDiemDaysPaid;
    }

    public void setPerDiemDaysPaid(Byte perDiemDaysPaid) {
        this.perDiemDaysPaid = perDiemDaysPaid;
    }

    public Boolean getCostWeighting() {
        return costWeighting;
    }

    public void setCostWeighting(Boolean costWeighting) {
        this.costWeighting = costWeighting;
    }

    public Boolean getRecomDeterEffect() {
        return recomDeterEffect;
    }

    public void setRecomDeterEffect(Boolean recomDeterEffect) {
        this.recomDeterEffect = recomDeterEffect;
    }

    public String getTotalCrmCost() {
        return totalCrmCost;
    }

    public void setTotalCrmCost(String totalCrmCost) {
        this.totalCrmCost = totalCrmCost;
    }

    public Integer getActualCrmCost() {
        return actualCrmCost;
    }

    public void setActualCrmCost(Integer actualCrmCost) {
        this.actualCrmCost = actualCrmCost;
    }

    public Byte getStateProject() {
        return stateProject;
    }

    public void setStateProject(Byte stateProject) {
        this.stateProject = stateProject;
    }

    public Integer getStateProjectNo() {
        return stateProjectNo;
    }

    public void setStateProjectNo(Integer stateProjectNo) {
        this.stateProjectNo = stateProjectNo;
    }

    public String getCreatedBy() {
        return createdBy;
    }

    public void setCreatedBy(String createdBy) {
        this.createdBy = createdBy;
    }

    public Date getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(Date createdDate) {
        this.createdDate = createdDate;
    }

    public BigDecimal getCreatedInInstance() {
        return createdInInstance;
    }

    public void setCreatedInInstance(BigDecimal createdInInstance) {
        this.createdInInstance = createdInInstance;
    }

    public String getModifiedBy() {
        return modifiedBy;
    }

    public void setModifiedBy(String modifiedBy) {
        this.modifiedBy = modifiedBy;
    }

    public Date getModifiedDate() {
        return modifiedDate;
    }

    public void setModifiedDate(Date modifiedDate) {
        this.modifiedDate = modifiedDate;
    }

    public BigDecimal getModifiedInInstance() {
        return modifiedInInstance;
    }

    public void setModifiedInInstance(BigDecimal modifiedInInstance) {
        this.modifiedInInstance = modifiedInInstance;
    }

}
