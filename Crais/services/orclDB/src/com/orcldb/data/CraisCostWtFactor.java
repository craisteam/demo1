
package com.orcldb.data;

import java.util.HashSet;
import java.util.Set;


/**
 *  orclDB.CraisCostWtFactor
 *  12/08/2013 22:45:19
 * 
 */
public class CraisCostWtFactor {

    private Boolean code;
    private String description;
    private Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords = new HashSet<com.orcldb.data.CraisIstandardsRecord>();
    private Set<com.orcldb.data.CraisIstandardsRecordBad> craisIstandardsRecordBads = new HashSet<com.orcldb.data.CraisIstandardsRecordBad>();

    public Boolean getCode() {
        return code;
    }

    public void setCode(Boolean code) {
        this.code = code;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public Set<com.orcldb.data.CraisIstandardsRecord> getCraisIstandardsRecords() {
        return craisIstandardsRecords;
    }

    public void setCraisIstandardsRecords(Set<com.orcldb.data.CraisIstandardsRecord> craisIstandardsRecords) {
        this.craisIstandardsRecords = craisIstandardsRecords;
    }

    public Set<com.orcldb.data.CraisIstandardsRecordBad> getCraisIstandardsRecordBads() {
        return craisIstandardsRecordBads;
    }

    public void setCraisIstandardsRecordBads(Set<com.orcldb.data.CraisIstandardsRecordBad> craisIstandardsRecordBads) {
        this.craisIstandardsRecordBads = craisIstandardsRecordBads;
    }

}
