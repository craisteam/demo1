
package com.orcldb.data;



/**
 *  orclDB.CraisIstandardsRecordView
 *  12/08/2013 22:45:20
 * 
 */
public class CraisIstandardsRecordView {

    private CraisIstandardsRecordViewId id;

    public CraisIstandardsRecordViewId getId() {
        return id;
    }

    public void setId(CraisIstandardsRecordViewId id) {
        this.id = id;
    }

}
