
package com.hrdb.data;



/**
 *  hrdb.Report
 *  12/18/2013 09:27:37
 * 
 */
public class Report {

    private Integer id;
    private String projectName;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getProjectName() {
        return projectName;
    }

    public void setProjectName(String projectName) {
        this.projectName = projectName;
    }

}
