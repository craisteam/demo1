
package com.hrdb.data;

import java.util.Date;


/**
 *  hrdb.Crais_istandards_record
 *  12/18/2013 09:55:59
 * 
 */
public class Crais_istandards_record {

    private Integer id;
    private Integer region;
    private Integer rept_year;
    private Integer forest;
    private Integer rept_number;
    private String series_1;
    private String series_2;
    private Integer rept_month;
    private Integer rept_day;
    private String author_1_last_name;
    private String author_1_FI;
    private String author_1_MI;
    private String author_2_last_name;
    private String author_2_FI;
    private String author_2_MI;
    private String report_title;
    private Integer district_number;
    private Integer proj_function;
    private String activity_type_1;
    private String activity_type_2;
    private String programming;
    private Integer total_proj_acreage;
    private String institution;
    private Integer acreaege_comp_invent;
    private Integer percent_sample;
    private Integer ave_no_individuals;
    private Integer acreage_resurveyed;
    private Integer ave_spacing;
    private Integer total_sites;
    private Integer field_hours;
    private Integer new_sites_located;
    private Integer lab_lib_hours;
    private Integer travel_hours;
    private Integer sites_eligible;
    private Integer admin_hours;
    private Integer sites_not_eligible;
    private Integer mileage;
    private String per_diem_rate;
    private Integer sites_enhanced;
    private Integer per_diem_days_paid;
    private Integer cost_weighting;
    private Integer record_deter_effect;
    private String total_crm_cost;
    private Integer actual_crm_cost;
    private Integer state_project;
    private Integer state_project_no;
    private String created_by;
    private Date created_date;
    private Integer created_in_instance;
    private String modified_by;
    private Date modified_date;
    private Integer modified_in_instance;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getRegion() {
        return region;
    }

    public void setRegion(Integer region) {
        this.region = region;
    }

    public Integer getRept_year() {
        return rept_year;
    }

    public void setRept_year(Integer rept_year) {
        this.rept_year = rept_year;
    }

    public Integer getForest() {
        return forest;
    }

    public void setForest(Integer forest) {
        this.forest = forest;
    }

    public Integer getRept_number() {
        return rept_number;
    }

    public void setRept_number(Integer rept_number) {
        this.rept_number = rept_number;
    }

    public String getSeries_1() {
        return series_1;
    }

    public void setSeries_1(String series_1) {
        this.series_1 = series_1;
    }

    public String getSeries_2() {
        return series_2;
    }

    public void setSeries_2(String series_2) {
        this.series_2 = series_2;
    }

    public Integer getRept_month() {
        return rept_month;
    }

    public void setRept_month(Integer rept_month) {
        this.rept_month = rept_month;
    }

    public Integer getRept_day() {
        return rept_day;
    }

    public void setRept_day(Integer rept_day) {
        this.rept_day = rept_day;
    }

    public String getAuthor_1_last_name() {
        return author_1_last_name;
    }

    public void setAuthor_1_last_name(String author_1_last_name) {
        this.author_1_last_name = author_1_last_name;
    }

    public String getAuthor_1_FI() {
        return author_1_FI;
    }

    public void setAuthor_1_FI(String author_1_FI) {
        this.author_1_FI = author_1_FI;
    }

    public String getAuthor_1_MI() {
        return author_1_MI;
    }

    public void setAuthor_1_MI(String author_1_MI) {
        this.author_1_MI = author_1_MI;
    }

    public String getAuthor_2_last_name() {
        return author_2_last_name;
    }

    public void setAuthor_2_last_name(String author_2_last_name) {
        this.author_2_last_name = author_2_last_name;
    }

    public String getAuthor_2_FI() {
        return author_2_FI;
    }

    public void setAuthor_2_FI(String author_2_FI) {
        this.author_2_FI = author_2_FI;
    }

    public String getAuthor_2_MI() {
        return author_2_MI;
    }

    public void setAuthor_2_MI(String author_2_MI) {
        this.author_2_MI = author_2_MI;
    }

    public String getReport_title() {
        return report_title;
    }

    public void setReport_title(String report_title) {
        this.report_title = report_title;
    }

    public Integer getDistrict_number() {
        return district_number;
    }

    public void setDistrict_number(Integer district_number) {
        this.district_number = district_number;
    }

    public Integer getProj_function() {
        return proj_function;
    }

    public void setProj_function(Integer proj_function) {
        this.proj_function = proj_function;
    }

    public String getActivity_type_1() {
        return activity_type_1;
    }

    public void setActivity_type_1(String activity_type_1) {
        this.activity_type_1 = activity_type_1;
    }

    public String getActivity_type_2() {
        return activity_type_2;
    }

    public void setActivity_type_2(String activity_type_2) {
        this.activity_type_2 = activity_type_2;
    }

    public String getProgramming() {
        return programming;
    }

    public void setProgramming(String programming) {
        this.programming = programming;
    }

    public Integer getTotal_proj_acreage() {
        return total_proj_acreage;
    }

    public void setTotal_proj_acreage(Integer total_proj_acreage) {
        this.total_proj_acreage = total_proj_acreage;
    }

    public String getInstitution() {
        return institution;
    }

    public void setInstitution(String institution) {
        this.institution = institution;
    }

    public Integer getAcreaege_comp_invent() {
        return acreaege_comp_invent;
    }

    public void setAcreaege_comp_invent(Integer acreaege_comp_invent) {
        this.acreaege_comp_invent = acreaege_comp_invent;
    }

    public Integer getPercent_sample() {
        return percent_sample;
    }

    public void setPercent_sample(Integer percent_sample) {
        this.percent_sample = percent_sample;
    }

    public Integer getAve_no_individuals() {
        return ave_no_individuals;
    }

    public void setAve_no_individuals(Integer ave_no_individuals) {
        this.ave_no_individuals = ave_no_individuals;
    }

    public Integer getAcreage_resurveyed() {
        return acreage_resurveyed;
    }

    public void setAcreage_resurveyed(Integer acreage_resurveyed) {
        this.acreage_resurveyed = acreage_resurveyed;
    }

    public Integer getAve_spacing() {
        return ave_spacing;
    }

    public void setAve_spacing(Integer ave_spacing) {
        this.ave_spacing = ave_spacing;
    }

    public Integer getTotal_sites() {
        return total_sites;
    }

    public void setTotal_sites(Integer total_sites) {
        this.total_sites = total_sites;
    }

    public Integer getField_hours() {
        return field_hours;
    }

    public void setField_hours(Integer field_hours) {
        this.field_hours = field_hours;
    }

    public Integer getNew_sites_located() {
        return new_sites_located;
    }

    public void setNew_sites_located(Integer new_sites_located) {
        this.new_sites_located = new_sites_located;
    }

    public Integer getLab_lib_hours() {
        return lab_lib_hours;
    }

    public void setLab_lib_hours(Integer lab_lib_hours) {
        this.lab_lib_hours = lab_lib_hours;
    }

    public Integer getTravel_hours() {
        return travel_hours;
    }

    public void setTravel_hours(Integer travel_hours) {
        this.travel_hours = travel_hours;
    }

    public Integer getSites_eligible() {
        return sites_eligible;
    }

    public void setSites_eligible(Integer sites_eligible) {
        this.sites_eligible = sites_eligible;
    }

    public Integer getAdmin_hours() {
        return admin_hours;
    }

    public void setAdmin_hours(Integer admin_hours) {
        this.admin_hours = admin_hours;
    }

    public Integer getSites_not_eligible() {
        return sites_not_eligible;
    }

    public void setSites_not_eligible(Integer sites_not_eligible) {
        this.sites_not_eligible = sites_not_eligible;
    }

    public Integer getMileage() {
        return mileage;
    }

    public void setMileage(Integer mileage) {
        this.mileage = mileage;
    }

    public String getPer_diem_rate() {
        return per_diem_rate;
    }

    public void setPer_diem_rate(String per_diem_rate) {
        this.per_diem_rate = per_diem_rate;
    }

    public Integer getSites_enhanced() {
        return sites_enhanced;
    }

    public void setSites_enhanced(Integer sites_enhanced) {
        this.sites_enhanced = sites_enhanced;
    }

    public Integer getPer_diem_days_paid() {
        return per_diem_days_paid;
    }

    public void setPer_diem_days_paid(Integer per_diem_days_paid) {
        this.per_diem_days_paid = per_diem_days_paid;
    }

    public Integer getCost_weighting() {
        return cost_weighting;
    }

    public void setCost_weighting(Integer cost_weighting) {
        this.cost_weighting = cost_weighting;
    }

    public Integer getRecord_deter_effect() {
        return record_deter_effect;
    }

    public void setRecord_deter_effect(Integer record_deter_effect) {
        this.record_deter_effect = record_deter_effect;
    }

    public String getTotal_crm_cost() {
        return total_crm_cost;
    }

    public void setTotal_crm_cost(String total_crm_cost) {
        this.total_crm_cost = total_crm_cost;
    }

    public Integer getActual_crm_cost() {
        return actual_crm_cost;
    }

    public void setActual_crm_cost(Integer actual_crm_cost) {
        this.actual_crm_cost = actual_crm_cost;
    }

    public Integer getState_project() {
        return state_project;
    }

    public void setState_project(Integer state_project) {
        this.state_project = state_project;
    }

    public Integer getState_project_no() {
        return state_project_no;
    }

    public void setState_project_no(Integer state_project_no) {
        this.state_project_no = state_project_no;
    }

    public String getCreated_by() {
        return created_by;
    }

    public void setCreated_by(String created_by) {
        this.created_by = created_by;
    }

    public Date getCreated_date() {
        return created_date;
    }

    public void setCreated_date(Date created_date) {
        this.created_date = created_date;
    }

    public Integer getCreated_in_instance() {
        return created_in_instance;
    }

    public void setCreated_in_instance(Integer created_in_instance) {
        this.created_in_instance = created_in_instance;
    }

    public String getModified_by() {
        return modified_by;
    }

    public void setModified_by(String modified_by) {
        this.modified_by = modified_by;
    }

    public Date getModified_date() {
        return modified_date;
    }

    public void setModified_date(Date modified_date) {
        this.modified_date = modified_date;
    }

    public Integer getModified_in_instance() {
        return modified_in_instance;
    }

    public void setModified_in_instance(Integer modified_in_instance) {
        this.modified_in_instance = modified_in_instance;
    }

}
