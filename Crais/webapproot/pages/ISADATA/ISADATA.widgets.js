ISADATA.widgets = {
	varTemplateLogout: ["wm.LogoutVariable", {}, {}, {
		input: ["wm.ServiceInput", {"type":"logoutInputs"}, {}]
	}],
	statesVar: ["wm.Variable", {"isList":true,"json":"[{name: \"Alabama\", dataValue: \"AL\"},{name:\"Alaska\", dataValue:\"AK\"},{name:\"American Samoa\", dataValue:\"AS\"},{name:\"Arizona\", dataValue:\"AZ\"},{name:\"Arkansas\", dataValue:\"AR\"},{name:\"California\", dataValue:\"CA\"},{name:\"Colorado\", dataValue:\"CO\"},{name:\"Connecticut\", dataValue:\"CT\"},{name:\"Delaware\", dataValue:\"DE\"},{name:\"District of Columbia\", dataValue:\"DC\"},{name:\"Federated States of Micronesia\", dataValue: \"FM\"},{name: \"Florida\", dataValue:\"FL\"},{name:\"Georgia\", dataValue:\"GA\"},{name:\"Guam\", dataValue:\"GU\"},{name:\"Hawaii\", dataValue:\"HI\"},{name:\"Idaho\", dataValue:\"ID\"},{name:\"Illinois\", dataValue:\"IL\"},{name:\"Indiana\", dataValue:\"IN\"},{name:\"Iowa\", dataValue:\"IA\"},{name:\"Kansas\", dataValue:\"KS\"},{name:\"Kentucky\", dataValue:\"KY\"},{name:\"Louisiana\", dataValue:\"LA\"},{name:\"Maine\", dataValue:\"ME\"},{name:\"Marshall islands\", dataValue:\"MH\"},{name:\"Maryland\", dataValue:\"MD\"},{name:\"Massachusetts\", dataValue:\"MA\"},{name:\"Michigan\", dataValue:\"MI\"},{name:\"Minnesota\", dataValue:\"MN\"},{name:\"Mississippi\", dataValue:\"MS\"},{name:\"Missouri\", dataValue:\"MO\"},{name:\"Montana\", dataValue:\"MT\"},{name:\"Nebraska\", dataValue:\"NE\"},{name:\"Nevada\", dataValue:\"NV\"},{name:\"New Hampshire\", dataValue:\"NH\"},{name:\"New Jersey\", dataValue:\"NJ\"},{name:\"New Mexico\", dataValue:\"NM\"},{name:\"New York\", dataValue:\"NY\"},{name:\"North Carolina\", dataValue:\"NC\"},{name:\"North Dakota\", dataValue:\"ND\"},{name:\"Northern Mariana Islands\", dataValue:\"MP\"},{name:\"Ohio\", dataValue:\"OH\"},{name:\"Oklahoma\", dataValue:\"OK\"},{name:\"Oregon\", dataValue:\"OR\"},{name:\"Palau\", dataValue:\"PW\"},{name:\"Pennsylvania\", dataValue:\"PA\"},{name:\"Puerto Rico\", dataValue:\"PR\"},{name:\"Rhode Island\", dataValue:\"RI\"},{name:\"South Carolina\", dataValue:\"SC\"},{name:\"South Dakota\", dataValue:\"SD\"},{name:\"Tennessee\", dataValue:\"TN\"},{name:\"Texas\", dataValue:\"TX\"},{name:\"Utah\", dataValue:\"UT\"},{name:\"Vermont\", dataValue:\"VT\"},{name:\"Virgin Islands\", dataValue:\"VI\"},{name:\"Virginia\", dataValue:\"VA\"},{name:\"Washington\", dataValue:\"WA\"},{name:\"West Virginia\", dataValue:\"WV\"},{name:\"Wisconsin\", dataValue:\"WI\"},{name:\"Wyoming\", dataValue:\"WY\"}]","type":"StringData"}, {}],
	projectnameVariable: ["wm.LiveVariable", {"ignoreCase":true,"startUpdate":false,"type":"com.hrdb.data.Report"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Report","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"ProjectName","sortable":true,"dataIndex":"projectName","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2001,"subType":null,"widthUnits":"px"}
]}, {}]
	}],
	nextISAPage: ["wm.NavigationCall", {"operation":"gotoPage"}, {}, {
		input: ["wm.ServiceInput", {"type":"gotoPageInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":"\"NextISAData\"","targetProperty":"pageName"}, {}]
			}]
		}]
	}],
	gotoCraisMainPage: ["wm.NavigationCall", {"operation":"gotoPage"}, {}, {
		input: ["wm.ServiceInput", {"type":"gotoPageInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":"\"CraisProject\"","targetProperty":"pageName"}, {}]
			}]
		}]
	}],
	layoutBox1: ["wm.Layout", {"horizontalAlign":"center","styles":{},"verticalAlign":"top","width":"200%"}, {}, {
		TitleBar: ["wm.Panel", {"_classes":{"domNode":["titlebar"]},"border":"0,0,4,0","borderColor":"#FBFBFB","height":"96px","horizontalAlign":"left","layoutKind":"left-to-right","padding":"8","verticalAlign":"top","width":"100%"}, {}, {
			isandalabel: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_24px"]},"align":"center","caption":"INVENTORY STANDARDS AND ACCOUNTING","height":"100%","padding":"4","styles":{"fontWeight":"bolder","textAlign":"center"},"width":"100%"}, {}]
		}],
		panel5: ["wm.Panel", {"borderColor":"#FBFBFB","height":"28px","horizontalAlign":"right","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			date1Panel: ["wm.Panel", {"borderColor":"#FBFBFB","height":"26px","horizontalAlign":"left","verticalAlign":"top","width":"300px"}, {}, {
				date1: ["wm.Date", {"caption":"Report Date:","displayValue":"12/18/2013","styles":{},"width":"100%"}, {"onchange":"date1Change"}, {
					binding: ["wm.Binding", {}, {}, {
						wire: ["wm.Wire", {"expression":"new Date()","targetProperty":"dataValue"}, {}]
					}]
				}]
			}]
		}],
		panel3: ["wm.Panel", {"borderColor":"#FBFBFB","height":"151px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			label1: ["wm.Label", {"align":"left","caption":"Report Number:","padding":"4","styles":{"fontWeight":"bolder"},"width":"143px"}, {}],
			year: ["wm.Text", {"caption":"Year","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"160px"}, {}],
			forest: ["wm.Text", {"caption":"Forest","captionAlign":"left","displayValue":"","styles":{},"width":"178px"}, {"onfocus":"forestFocus"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"app.forestnumVariable.dataValue","targetProperty":"dataValue"}, {}]
				}]
			}],
			number: ["wm.Text", {"caption":"Number","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"200px"}, {}],
			panel6: ["wm.Panel", {"borderColor":"#FBFBFB","height":"26px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
				series: ["wm.Text", {"caption":"Series","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"120px"}, {}],
				series2: ["wm.Text", {"caption":undefined,"dataValue":undefined,"displayValue":"","styles":{},"width":"30px"}, {}]
			}]
		}],
		panel4: ["wm.Panel", {"borderColor":"#FBFBFB","height":"74px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			author: ["wm.Label", {"caption":"Author:","padding":"4","styles":{"fontWeight":"bolder"},"width":"100px"}, {}],
			authorFirst: ["wm.Text", {"caption":"FirstName:","captionAlign":"left","dataValue":undefined,"displayValue":"","required":true,"styles":{}}, {}],
			lastname: ["wm.Text", {"caption":"LastName:","captionAlign":"left","dataValue":undefined,"displayValue":"","required":true,"styles":{}}, {}]
		}],
		panel1: ["wm.Panel", {"borderColor":"#FBFBFB","height":"30px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			reportTitle: ["wm.SelectMenu", {"caption":"Project Name:","dataField":"projectName","dataType":"com.hrdb.data.Report","dataValue":undefined,"desktopHeight":"28px","displayField":"projectName","displayValue":"","height":"28px","styles":{},"width":"500px"}, {}, {
				binding: ["wm.Binding", {}, {}, {
					wire1: ["wm.Wire", {"expression":undefined,"source":"projectnameVariable","targetProperty":"dataSet"}, {}]
				}]
			}]
		}],
		panel2: ["wm.Panel", {"borderColor":"#FBFBFB","height":"50px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			statePrj: ["wm.Text", {"caption":"State Project:","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"150px"}, {}],
			prjNo: ["wm.Text", {"caption":"Project No:","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"150px"}, {}]
		}],
		panel8: ["wm.Panel", {"height":"59px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"276px"}, {}, {
			next: ["wm.Button", {"caption":"NEXT","desktopHeight":"35px","height":"35px","margin":"0,0,0,0","styles":{},"width":"100px"}, {"onclick":"nextISAPage"}],
			panel7: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"31px"}, {}],
			back: ["wm.Button", {"caption":"BACK","desktopHeight":"35px","height":"35px","margin":"0,0,0,0","styles":{},"width":"100px"}, {"onclick":"gotoCraisMainPage"}]
		}]
	}]
}