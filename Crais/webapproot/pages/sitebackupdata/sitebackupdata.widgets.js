sitebackupdata.widgets = {
	varTemplateLogout: ["wm.LogoutVariable", {}, {}, {
		input: ["wm.ServiceInput", {"type":"logoutInputs"}, {}]
	}],
	reportLiveVariable1: ["wm.LiveVariable", {"type":"com.hrdb.data.Report"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Report","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"ProjectName","sortable":true,"dataIndex":"projectName","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable1: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable2: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable3: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable4: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable5: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable6: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable7: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	crais_istandards_recordLiveVariable8: ["wm.LiveVariable", {"type":"com.hrdb.data.Crais_istandards_record"}, {}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}]
	}],
	monthsVar: ["wm.Variable", {"isList":true,"json":"[{name: \"January\", dataValue: 0}, {name: \"February\", dataValue: 1},{name: \"March\", dataValue: 2},{name: \"April\", dataValue: 3},{name: \"May\", dataValue: 4},{name: \"June\", dataValue: 5},{name: \"July\", dataValue: 6},{name: \"August\", dataValue: 7},{name: \"September\", dataValue: 8},{name: \"October\", dataValue: 9},{name: \"November\", dataValue: 10},{name: \"December\", dataValue: 11}]","type":"EntryData"}, {}],
	varTemplateUsername: ["wm.ServiceVariable", {"autoUpdate":true,"designTime":true,"operation":"getUserName","service":"securityService","startUpdate":true}, {}, {
		input: ["wm.ServiceInput", {"type":"getUserNameInputs"}, {}]
	}],
	varTemplateLogout1: ["wm.LogoutVariable", {}, {}, {
		input: ["wm.ServiceInput", {"type":"logoutInputs"}, {}]
	}],
	crais_istandards_recordDialog: ["wm.DesignableDialog", {"buttonBarId":"buttonBar","containerWidgetId":"containerWidget","desktopHeight":"1432px","height":"1432px","title":"crais_istandards_record","width":"500px"}, {}, {
		containerWidget: ["wm.Container", {"_classes":{"domNode":["wmdialogcontainer","MainContent"]},"autoScroll":true,"height":"100%","horizontalAlign":"left","padding":"5","verticalAlign":"top","width":"100%"}, {}, {
			crais_istandards_recordLiveForm1: ["wm.LiveForm", {"alwaysPopulateEditors":true,"fitToContentHeight":true,"height":"1362px","horizontalAlign":"left","liveEditing":false,"margin":"4","verticalAlign":"top"}, {"onSuccess":"hrdbLivePanel.popupLiveFormSuccess"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"expression":undefined,"source":"crais_istandards_recordDojoGrid.selectedItem","targetProperty":"dataSet"}, {}]
				}],
				idEditor1: ["wm.Number", {"caption":"Id","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"id","height":"26px","required":true,"width":"100%"}, {}],
				regionEditor1: ["wm.Number", {"caption":"Region","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"region","height":"26px","width":"100%"}, {}],
				rept_yearEditor1: ["wm.Number", {"caption":"Rept_year","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"rept_year","height":"26px","width":"100%"}, {}],
				forestEditor1: ["wm.Number", {"caption":"Forest","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"forest","height":"26px","width":"100%"}, {}],
				rept_numberEditor1: ["wm.Number", {"caption":"Rept_number","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"rept_number","height":"26px","width":"100%"}, {}],
				series_1Editor1: ["wm.Text", {"caption":"Series_1","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"series_1","height":"26px","width":"100%"}, {}],
				series_2Editor1: ["wm.Text", {"caption":"Series_2","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"series_2","height":"26px","width":"100%"}, {}],
				rept_monthEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Rept_month","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"rept_month","height":"26px","width":"100%"}, {}],
				rept_dayEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Rept_day","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"rept_day","height":"26px","width":"100%"}, {}],
				author_1_last_nameEditor1: ["wm.Text", {"caption":"Author_1_last_name","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"author_1_last_name","height":"26px","width":"100%"}, {}],
				author_1_FIEditor1: ["wm.Text", {"caption":"Author_1_FI","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"author_1_FI","height":"26px","width":"100%"}, {}],
				author_1_MIEditor1: ["wm.Text", {"caption":"Author_1_MI","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"author_1_MI","height":"26px","width":"100%"}, {}],
				author_2_last_nameEditor1: ["wm.Text", {"caption":"Author_2_last_name","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"author_2_last_name","height":"26px","width":"100%"}, {}],
				author_2_FIEditor1: ["wm.Text", {"caption":"Author_2_FI","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"author_2_FI","height":"26px","width":"100%"}, {}],
				author_2_MIEditor1: ["wm.Text", {"caption":"Author_2_MI","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"author_2_MI","height":"26px","width":"100%"}, {}],
				report_titleEditor1: ["wm.Text", {"caption":"Report_title","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"report_title","height":"26px","width":"100%"}, {}],
				district_numberEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"District_number","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"district_number","height":"26px","width":"100%"}, {}],
				proj_functionEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Proj_function","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"proj_function","height":"26px","width":"100%"}, {}],
				activity_type_1Editor1: ["wm.Text", {"caption":"Activity_type_1","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"activity_type_1","height":"26px","width":"100%"}, {}],
				activity_type_2Editor1: ["wm.Text", {"caption":"Activity_type_2","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"activity_type_2","height":"26px","width":"100%"}, {}],
				programmingEditor1: ["wm.Text", {"caption":"Programming","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"programming","height":"26px","width":"100%"}, {}],
				total_proj_acreageEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Total_proj_acreage","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"total_proj_acreage","height":"26px","width":"100%"}, {}],
				institutionEditor1: ["wm.Text", {"caption":"Institution","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"institution","height":"26px","width":"100%"}, {}],
				acreaege_comp_inventEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Acreaege_comp_invent","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"acreaege_comp_invent","height":"26px","width":"100%"}, {}],
				percent_sampleEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Percent_sample","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"percent_sample","height":"26px","width":"100%"}, {}],
				ave_no_individualsEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Ave_no_individuals","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"ave_no_individuals","height":"26px","width":"100%"}, {}],
				acreage_resurveyedEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Acreage_resurveyed","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"acreage_resurveyed","height":"26px","width":"100%"}, {}],
				ave_spacingEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Ave_spacing","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"ave_spacing","height":"26px","width":"100%"}, {}],
				total_sitesEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Total_sites","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"total_sites","height":"26px","width":"100%"}, {}],
				field_hoursEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Field_hours","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"field_hours","height":"26px","width":"100%"}, {}],
				new_sites_locatedEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"New_sites_located","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"new_sites_located","height":"26px","width":"100%"}, {}],
				lab_lib_hoursEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Lab_lib_hours","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"lab_lib_hours","height":"26px","width":"100%"}, {}],
				travel_hoursEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Travel_hours","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"travel_hours","height":"26px","width":"100%"}, {}],
				sites_eligibleEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Sites_eligible","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"sites_eligible","height":"26px","width":"100%"}, {}],
				admin_hoursEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Admin_hours","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"admin_hours","height":"26px","width":"100%"}, {}],
				sites_not_eligibleEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Sites_not_eligible","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"sites_not_eligible","height":"26px","width":"100%"}, {}],
				mileageEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Mileage","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"mileage","height":"26px","width":"100%"}, {}],
				per_diem_rateEditor1: ["wm.Text", {"caption":"Per_diem_rate","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"per_diem_rate","height":"26px","width":"100%"}, {}],
				sites_enhancedEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Sites_enhanced","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"sites_enhanced","height":"26px","width":"100%"}, {}],
				per_diem_days_paidEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Per_diem_days_paid","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"per_diem_days_paid","height":"26px","width":"100%"}, {}],
				cost_weightingEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Cost_weighting","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"cost_weighting","height":"26px","width":"100%"}, {}],
				record_deter_effectEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Record_deter_effect","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"record_deter_effect","height":"26px","width":"100%"}, {}],
				total_crm_costEditor1: ["wm.Text", {"caption":"Total_crm_cost","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"total_crm_cost","height":"26px","width":"100%"}, {}],
				actual_crm_costEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Actual_crm_cost","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"actual_crm_cost","height":"26px","width":"100%"}, {}],
				state_projectEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"State_project","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"state_project","height":"26px","width":"100%"}, {}],
				state_project_noEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"State_project_no","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"state_project_no","height":"26px","width":"100%"}, {}],
				created_byEditor1: ["wm.Text", {"caption":"Created_by","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"created_by","height":"26px","width":"100%"}, {}],
				created_dateEditor1: ["wm.DateTime", {"caption":"Created_date","captionSize":"140px","dateMode":"Date","desktopHeight":"26px","emptyValue":"zero","formField":"created_date","height":"26px","width":"100%"}, {}],
				created_in_instanceEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Created_in_instance","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"created_in_instance","height":"26px","width":"100%"}, {}],
				modified_byEditor1: ["wm.Text", {"caption":"Modified_by","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"emptyString","formField":"modified_by","height":"26px","width":"100%"}, {}],
				modified_dateEditor1: ["wm.DateTime", {"caption":"Modified_date","captionSize":"140px","dateMode":"Date","desktopHeight":"26px","emptyValue":"zero","formField":"modified_date","height":"26px","width":"100%"}, {}],
				modified_in_instanceEditor1: ["wm.Number", {"borderColor":"#FBFBFB","caption":"Modified_in_instance","captionSize":"140px","changeOnKey":true,"desktopHeight":"26px","emptyValue":"zero","formField":"modified_in_instance","height":"26px","width":"100%"}, {}]
			}]
		}],
		buttonBar: ["wm.ButtonBarPanel", {"border":"1,0,0,0","borderColor":"#333333","desktopHeight":"33px","height":"33px"}, {}, {
			crais_istandards_recordSaveButton: ["wm.Button", {"caption":"Save","margin":"4"}, {"onclick":"crais_istandards_recordLiveForm1.saveDataIfValid"}, {
				binding: ["wm.Binding", {}, {}, {
					wire: ["wm.Wire", {"source":"crais_istandards_recordLiveForm1.invalid","targetId":null,"targetProperty":"disabled"}, {}]
				}]
			}],
			crais_istandards_recordCancelButton: ["wm.Button", {"caption":"Cancel","margin":"4"}, {"onclick":"crais_istandards_recordDialog.hide","onclick1":"crais_istandards_recordLiveForm1.cancelEdit"}]
		}]
	}],
	layoutBox1: ["wm.Layout", {"horizontalAlign":"left","verticalAlign":"top"}, {}, {
		TabsTemplate: ["wm.Panel", {"autoScroll":true,"height":"100%","horizontalAlign":"center","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
			panel3: ["wm.Panel", {"height":"100%","horizontalAlign":"left","minDesktopHeight":600,"minHeight":600,"minWidth":900,"verticalAlign":"top","width":"75%"}, {}, {
				panel1: ["wm.HeaderContentPanel", {"border":"0,0,1,0","borderColor":"#333333","height":"65px","horizontalAlign":"left","layoutKind":"left-to-right","padding":"0,10","verticalAlign":"middle","width":"100%"}, {}, {
					picture1: ["wm.Picture", {"height":"50px","source":"lib/wm/base/widget/themes/default/images/wmLogo.png","width":"62px"}, {}],
					label3: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_20px","wm_FontSizePx_24px"]},"caption":"CRAIS","height":"35px","padding":"4","width":"100%"}, {}],
					panel5: ["wm.Panel", {"height":"100%","horizontalAlign":"right","verticalAlign":"middle","width":"100%"}, {}, {
						dojoMenu1: ["wm.DojoMenu", {"fullStructure":[
{"label":"Help"},
{"label":"About"},
{"label":"Logout","onClick":"varTemplateLogout"}
],"height":"24px","localizationStructure":{},"transparent":true,"width":"250px"}, {}]
					}]
				}],
				panel2: ["wm.MainContentPanel", {"height":"100%","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					tabLayers1: ["wm.TabLayers", {"styles":{}}, {}, {
						layer1: ["wm.Layer", {"autoScroll":true,"border":"1","borderColor":"#999999","caption":"INVENTORY STANDARDS AND ACCOUNTING","horizontalAlign":"center","padding":"10","verticalAlign":"top"}, {}, {
							panel7: ["wm.Panel", {"borderColor":"#FBFBFB","height":"28px","horizontalAlign":"right","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
								reportTitle: ["wm.SelectMenu", {"caption":"Project Name:","captionAlign":"left","captionSize":"250px","dataField":"projectName","dataType":"com.hrdb.data.Report","dataValue":undefined,"desktopHeight":"28px","displayField":"projectName","displayValue":"","height":"28px","styles":{},"width":"657px"}, {}, {
									binding: ["wm.Binding", {}, {}, {
										wire: ["wm.Wire", {"expression":undefined,"source":"reportLiveVariable1","targetProperty":"dataSet"}, {}]
									}]
								}],
								date1Panel: ["wm.Panel", {"borderColor":"#FBFBFB","height":"26px","horizontalAlign":"left","verticalAlign":"top","width":"268px"}, {}, {
									date1: ["wm.Date", {"caption":"Report Date:","displayValue":"12/19/2013","styles":{},"width":"262px"}, {"onchange":"date1Change"}, {
										binding: ["wm.Binding", {}, {}, {
											wire: ["wm.Wire", {"expression":"new Date()","targetProperty":"dataValue"}, {}]
										}]
									}]
								}]
							}],
							panel55: ["wm.Panel", {"height":"137px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
								panel4: ["wm.Panel", {"borderColor":"#FBFBFB","height":"131px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"373px"}, {}, {
									label7: ["wm.Label", {"align":"left","caption":"Report Number:","padding":"4","styles":{"fontWeight":"bolder"},"width":"143px"}, {}],
									year: ["wm.Text", {"caption":"Year","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"30%"}, {}],
									forest: ["wm.Text", {"caption":"Forest","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"30%"}, {"onfocus":"forestFocus"}],
									number: ["wm.Text", {"caption":"Number","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"30%"}, {}],
									panel8: ["wm.Panel", {"borderColor":"#FBFBFB","height":"26px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
										series: ["wm.Text", {"caption":"Series","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"25%"}, {}],
										series2: ["wm.Text", {"caption":undefined,"dataValue":undefined,"displayValue":"","styles":{},"width":"40px"}, {}]
									}]
								}],
								panel56: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"486px"}, {}, {
									panel9: ["wm.Panel", {"borderColor":"#FBFBFB","height":"74px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"458px"}, {}, {
										author: ["wm.Label", {"caption":"Author:","padding":"4","styles":{"fontWeight":"bolder"},"width":"100px"}, {}],
										authorFirst: ["wm.Text", {"caption":"FirstName:","captionAlign":"left","dataValue":undefined,"displayValue":"","required":true,"styles":{}}, {}],
										lastname: ["wm.Text", {"caption":"LastName:","captionAlign":"left","dataValue":undefined,"displayValue":"","required":true,"styles":{}}, {}]
									}],
									panel11: ["wm.Panel", {"borderColor":"#FBFBFB","height":"50px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"459px"}, {}, {
										statePrj: ["wm.Text", {"caption":"State Project:","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"150px"}, {}],
										prjNo: ["wm.Text", {"caption":"Project No:","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"150px"}, {}]
									}]
								}]
							}],
							pageContainer1: ["wm.PageContainer", {"deferLoad":true,"showing":false}, {}],
							pageContainer2: ["wm.PageContainer", {"pageName":"ISANDANEXT","showing":false,"subpageEventlist":{},"subpageMethodlist":{},"subpageProplist":{}}, {}],
							panel12: ["wm.Panel", {"height":"433px","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
								panel59: ["wm.Panel", {"height":"151px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
									panel60: ["wm.Panel", {"height":"147px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"377px"}, {}, {
										program: ["wm.Text", {"caption":"PROGRAMMING:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"desktopHeight":"23px","displayValue":"","height":"23px","styles":{},"width":"30%"}, {}],
										sectype: ["wm.Text", {"caption":"SECONDARY ACTVITY TYPE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}],
										primary: ["wm.Text", {"caption":"PRIMARY ACTIVITY TYPE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"25%"}, {}],
										district1: ["wm.Text", {"caption":"RANGER DISTRICT:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}],
										func: ["wm.Text", {"caption":"PROJECT FUNCTION:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}]
									}],
									panel62: ["wm.Panel", {"height":"146px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"366px"}, {}, {
										total: ["wm.Text", {"caption":"TOTAL PROJECT ACREAGE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"57%"}, {}],
										survey: ["wm.Text", {"caption":"ACREAGE COMPLETELY SURVEYED:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}],
										sample: ["wm.Text", {"caption":"SAMPLE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"57%"}, {}],
										resurvey: ["wm.Text", {"caption":"ACREAGE RESURVEYED:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}],
										area: ["wm.Text", {"caption":"TOTAL NO. OF SITES IN PROJECT AREA:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"50%"}, {}],
										sites: ["wm.Text", {"caption":"NEW SITES:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}]
									}]
								}],
								panel61: ["wm.Panel", {"height":"249px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
									panel63: ["wm.Panel", {"height":"243px","horizontalAlign":"left","verticalAlign":"top","width":"380px"}, {}, {
										panel64: ["wm.Panel", {"height":"74px","horizontalAlign":"left","verticalAlign":"top","width":"359px"}, {}, {
											text3: ["wm.Text", {"caption":"INSTITUTION COMD. PROJECT/SURVEY:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
											text1: ["wm.Text", {"caption":"AVG. NO. INDIV. USED:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
											text4: ["wm.Text", {"caption":"AVG. INDIV TRANSECT SPACING (FT):","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}]
										}],
										panel65: ["wm.Panel", {"height":"75px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
											label4: ["wm.Label", {"align":"left","caption":"SITES EVALUATED:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											text13: ["wm.Text", {"caption":"ELIGIBLE:","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
											text14: ["wm.Text", {"caption":"NOT ELIGIBLE:","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}]
										}],
										text15: ["wm.Text", {"caption":"SITES INSP., MON., ENHANCED:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
										text16: ["wm.Text", {"caption":"RECOMMENDED DETERMINATION OF EFFECT:","captionAlign":"left","captionSize":"290px","dataValue":undefined,"displayValue":"","styles":{},"width":"345px"}, {}]
									}],
									panel67: ["wm.Panel", {"height":"243px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"368px"}, {}, {
										hrs: ["wm.Text", {"caption":"FIELD HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text5: ["wm.Text", {"caption":"LAB/LIB HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text6: ["wm.Text", {"caption":"TRAVEL HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text7: ["wm.Text", {"caption":"ADMIN HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text8: ["wm.Text", {"caption":"MILEAGE:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text9: ["wm.Text", {"caption":"PER DIEM RATE:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text10: ["wm.Text", {"caption":"DAYS OF PER DIEM:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text11: ["wm.Text", {"caption":"COST WEIGHT FACTOR:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text12: ["wm.Text", {"caption":"COST (CODE):","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
										text17: ["wm.Text", {"caption":"ACTUAL COST:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}]
									}]
								}]
							}]
						}],
						layer2: ["wm.Layer", {"autoScroll":true,"border":"1","borderColor":"#999999","caption":"ARCHEOLOGICAL AND HISTORICAL SITE INVENTORY","horizontalAlign":"left","verticalAlign":"top"}, {}, {
							accordionLayers1: ["wm.AccordionLayers", {"width":"862px"}, {}, {
								layer4: ["wm.Layer", {"autoScroll":true,"borderColor":"","caption":"Locational and CRM Information","horizontalAlign":"left","showing":false,"themeStyleType":"ContentPanel","verticalAlign":"top"}, {}, {
									formPanel1: ["wm.FormPanel", {"desktopHeight":"507px","height":"507px","horizontalAlign":"center"}, {}, {
										panel15: ["wm.Panel", {"height":"74px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"862px"}, {}, {
											panel16: ["wm.Panel", {"height":"72px","horizontalAlign":"left","verticalAlign":"top","width":"403px"}, {}, {
												text2: ["wm.Text", {"caption":"Forest:","captionAlign":"left","captionSize":"120px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"222px"}, {}],
												state1: ["wm.Text", {"caption":"State:","captionAlign":"left","captionSize":"120px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"222px"}, {}]
											}],
											panel14: ["wm.Panel", {"height":"69px","horizontalAlign":"left","verticalAlign":"top","width":"289px"}, {}, {
												district: ["wm.Text", {"caption":"District","captionAlign":"left","captionSize":"120px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"222px"}, {}],
												county: ["wm.Text", {"caption":"County:","captionAlign":"left","captionSize":"120px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"222px"}, {}]
											}]
										}],
										panel13: ["wm.Panel", {"height":"91px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"900px"}, {}, {
											panel18: ["wm.Panel", {"height":"83px","horizontalAlign":"left","verticalAlign":"top","width":"401px"}, {}, {
												rtmnum: ["wm.Text", {"caption":"RTM Number(Class I Sites):","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												rtmnum1: ["wm.Text", {"caption":"Site Evaluation By Professional:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												rtmnum2: ["wm.Text", {"caption":"Cultural Resource Specialist Only:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}]
											}],
											panel32: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"497px"}, {}, {
												collectiontype1: ["wm.Text", {"caption":"Collection Type:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												collectionmade1: ["wm.Text", {"caption":"Collection Made:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												sitemarked1: ["wm.Text", {"caption":"Site Marked on Ground:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}]
											}]
										}],
										panel19: ["wm.Panel", {"height":"136px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
											panel20: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"403px"}, {}, {
												sitenum: ["wm.Text", {"caption":"USFS Site Number:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												statenum: ["wm.Text", {"caption":"State Site Number:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												sitecon: ["wm.Text", {"caption":"Site Condition:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												sitefile: ["wm.Text", {"caption":"Site File Check:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												percent: ["wm.Text", {"caption":"Percent of Disturbance:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}]
											}],
											panel33: ["wm.Panel", {"height":"87px","horizontalAlign":"left","verticalAlign":"top","width":"409px"}, {}, {
												hrsonsite1: ["wm.Text", {"caption":"Hours Expended on Site:","captionAlign":"left","captionSize":"236px","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"280px"}, {}],
												date3: ["wm.Date", {"caption":"Date Site Inventoried:","captionAlign":"left","captionSize":"150px","desktopHeight":"26px","displayValue":"12/19/2013","height":"26px","width":"100%"}, {}, {
													binding: ["wm.Binding", {}, {}, {
														wire: ["wm.Wire", {"expression":"new Date();","targetProperty":"dataValue"}, {}]
													}]
												}],
												recordedby1: ["wm.Text", {"caption":"Recorded By:","captionAlign":"left","dataValue":undefined,"desktopHeight":"26px","displayValue":"","height":"26px","width":"200px"}, {}]
											}]
										}],
										panel22: ["wm.Panel", {"height":"96px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
											panel23: ["wm.Panel", {"height":"93px","horizontalAlign":"left","verticalAlign":"top","width":"411px"}, {}, {
												panel29: ["wm.Panel", {"height":"84px","horizontalAlign":"left","verticalAlign":"top","width":"400px"}, {}, {
													panel30: ["wm.Panel", {"height":"34px","horizontalAlign":"left","verticalAlign":"top","width":"400px"}, {}, {
														utm: ["wm.Label", {"align":"left","caption":"U.T.M:","height":"27px","padding":"4","styles":{"fontWeight":"bold"},"width":"259px"}, {}]
													}],
													panel31: ["wm.Panel", {"height":"48px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
														zone6: ["wm.Text", {"caption":"Zone","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"47px","displayValue":"","height":"47px","styles":{},"width":"91px"}, {}],
														zone7: ["wm.Text", {"caption":"Northing","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"80px"}, {}],
														zone8: ["wm.Text", {"caption":"Easting","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"80px"}, {}]
													}]
												}]
											}],
											panel17: ["wm.Panel", {"height":"92px","horizontalAlign":"left","verticalAlign":"top","width":"459px"}, {}, {
												panel21: ["wm.Panel", {"height":"39px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
													culturalreportnum1: ["wm.Label", {"align":"left","caption":"Cultural Resources Report Number:","height":"30px","padding":"4","styles":{"fontWeight":"bold"},"width":"259px"}, {}]
												}],
												panel34: ["wm.Panel", {"height":"48px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
													zone9: ["wm.Text", {"caption":"Year","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"47px","displayValue":"","height":"47px","styles":{},"width":"91px"}, {}],
													zone10: ["wm.Text", {"caption":"Forest","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"80px"}, {}],
													zone11: ["wm.Text", {"caption":"Number","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"80px"}, {}]
												}]
											}]
										}],
										page2: ["wm.Button", {"caption":"Environmental and Site Information","desktopHeight":"43px","height":"43px","margin":"4","width":"188px"}, {"onclick":"page2Click"}]
									}]
								}],
								layer5: ["wm.Layer", {"autoScroll":true,"borderColor":"","caption":"Environmental and Site Descriptive Information","horizontalAlign":"center","themeStyleType":"ContentPanel","verticalAlign":"top"}, {}, {
									panel26: ["wm.Panel", {"height":"131px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"854px"}, {}, {
										panel27: ["wm.Panel", {"height":"123px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"345px"}, {}, {
											label5: ["wm.Label", {"align":"left","caption":"Vegetation of Site Area:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											reg: ["wm.Text", {"caption":"Reg.","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											fm: ["wm.Text", {"caption":"Fm.","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											reg2: ["wm.Text", {"caption":"Series","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											association: ["wm.Text", {"caption":"Association","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}],
										panel28: ["wm.Panel", {"height":"98px","horizontalAlign":"left","verticalAlign":"top","width":"274px"}, {}, {
											soils: ["wm.Label", {"align":"left","caption":"Soils:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											greatgroup: ["wm.Text", {"caption":"Great Group","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											subgroup: ["wm.Text", {"caption":"Sub Group","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											depth: ["wm.Text", {"caption":"Depth (in cm)","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}],
										panel43: ["wm.Panel", {"height":"100px","horizontalAlign":"left","verticalAlign":"top","width":"202px"}, {}, {
											onsite1: ["wm.Label", {"align":"left","caption":"On Site Topography:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											panel44: ["wm.Panel", {"height":"50px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
												n1: ["wm.Text", {"caption":"N","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"43px"}, {}],
												e1: ["wm.Text", {"caption":"E","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"43px"}, {}],
												s1: ["wm.Text", {"caption":"S","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"43px"}, {}],
												w1: ["wm.Text", {"caption":"W","captionAlign":"center","captionPosition":"top","captionSize":"28px","dataValue":undefined,"desktopHeight":"48px","displayValue":"","height":"48px","styles":{},"width":"43px"}, {}]
											}]
										}]
									}],
									panel24: ["wm.Panel", {"height":"54px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"854px"}, {}, {
										panel35: ["wm.Panel", {"height":"52px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"345px"}, {}, {
											landform: ["wm.Text", {"caption":"Landform of Area","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											aspect: ["wm.Text", {"caption":"Aspect of Site","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}],
										panel25: ["wm.Panel", {"height":"52px","horizontalAlign":"left","verticalAlign":"top","width":"285px"}, {}, {
											avg: ["wm.Text", {"caption":"Average Slope (in degrees)","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											elevation: ["wm.Text", {"caption":"Elevation (in feet)","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}]
									}],
									panel37: ["wm.Panel", {"height":"110px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"850px"}, {}, {
										panel38: ["wm.Panel", {"height":"99px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"347px"}, {}, {
											nearestwater: ["wm.Label", {"align":"left","caption":"Nearest Water:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											reg1: ["wm.Text", {"caption":"Type:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											direction: ["wm.Text", {"caption":"Direction:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											distance: ["wm.Text", {"caption":"Distance (KM)","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}],
										panel39: ["wm.Panel", {"height":"98px","horizontalAlign":"left","verticalAlign":"top","width":"262px"}, {}, {
											nearestland: ["wm.Label", {"align":"left","caption":"Nearest Agricultural Land:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											typeland: ["wm.Text", {"caption":"Type:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											directionland: ["wm.Text", {"caption":"Direction:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											distanceland: ["wm.Text", {"caption":"Distance (in KM)","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}]
									}],
									panel45: ["wm.Panel", {"height":"155px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"852px"}, {}, {
										panel46: ["wm.Panel", {"height":"150px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"348px"}, {}, {
											panel36: ["wm.Panel", {"height":"28px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"638px"}, {}, {
												artifacts: ["wm.Label", {"align":"left","caption":"Artifacts Observed:","padding":"4","styles":{"fontWeight":"bold"}}, {}]
											}],
											flakestone: ["wm.Text", {"caption":"Flaked Stone:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											stone: ["wm.Text", {"caption":"Ground Stone:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											ceremic: ["wm.Text", {"caption":"Ceremics:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											cans: ["wm.Text", {"caption":"Cans:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											metal: ["wm.Text", {"caption":"Other Metal:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}],
										panel47: ["wm.Panel", {"height":"156px","horizontalAlign":"left","verticalAlign":"top","width":"262px"}, {}, {
											bone: ["wm.Text", {"caption":"Bone:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											shell: ["wm.Text", {"caption":"Shell:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											prelist: ["wm.Text", {"caption":"Other Prelist:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											histcere: ["wm.Text", {"caption":"Hist. Ceramics:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											construction: ["wm.Text", {"caption":"Construction:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											glass: ["wm.Text", {"caption":"Glass:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}]
									}],
									panel40: ["wm.Panel", {"height":"96px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"854px"}, {}, {
										panel41: ["wm.Panel", {"height":"99px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"348px"}, {}, {
											sitedesc: ["wm.Label", {"align":"left","caption":"Site Description:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											classsite: ["wm.Text", {"caption":"Class:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											use: ["wm.Text", {"caption":"Use:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											distance1: ["wm.Text", {"caption":"Type:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}],
										panel42: ["wm.Panel", {"height":"98px","horizontalAlign":"left","verticalAlign":"top","width":"262px"}, {}, {
											sitesize: ["wm.Label", {"align":"left","caption":"Site Size:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
											corearea: ["wm.Text", {"caption":"Core Area:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											dispersedarea: ["wm.Text", {"caption":"Dispersed Area:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}],
											sqmeter: ["wm.Text", {"caption":"Square Meters:","captionAlign":"left","captionSize":"190px","dataValue":undefined,"displayValue":"","styles":{},"width":"260px"}, {}]
										}]
									}]
								}]
							}]
						}],
						layer3: ["wm.Layer", {"border":"1","borderColor":"#999999","caption":"SAMPLE IS&A DATA","horizontalAlign":"left","styles":{},"verticalAlign":"top"}, {}, {
							layers1: ["wm.Layers", {"margin":"3,0,0,0"}, {}, {
								layer6: ["wm.Layer", {"border":"1","borderColor":"#999999","caption":"INVENTORY STANDARDS AND ACCOUNTING","horizontalAlign":"center","padding":"10","verticalAlign":"top"}, {}, {
									panel48: ["wm.Panel", {"borderColor":"#FBFBFB","height":"28px","horizontalAlign":"right","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
										date2: ["wm.Panel", {"borderColor":"#FBFBFB","height":"26px","horizontalAlign":"left","verticalAlign":"top","width":"300px"}, {}, {
											date4: ["wm.Date", {"caption":"Report Date:","displayValue":"12/19/2013","styles":{},"width":"100%"}, {"onchange":"date4Change"}, {
												binding: ["wm.Binding", {}, {}, {
													wire: ["wm.Wire", {"expression":"new Date()","targetProperty":"dataValue"}, {}]
												}]
											}]
										}]
									}],
									panel57: ["wm.Panel", {"height":"137px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
										panel49: ["wm.Panel", {"borderColor":"#FBFBFB","height":"131px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"373px"}, {}, {
											label8: ["wm.Label", {"align":"left","caption":"Report Number:","padding":"4","styles":{"fontWeight":"bolder"},"width":"143px"}, {}],
											year1: ["wm.Text", {"caption":"Year","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"160px"}, {}],
											forest1: ["wm.Text", {"caption":"Forest","captionAlign":"left","displayValue":"","styles":{},"width":"160px"}, {"onfocus":"forest1Focus"}, {
												binding: ["wm.Binding", {}, {}, {
													wire: ["wm.Wire", {"expression":undefined,"source":"app.forestnumVariable.dataValue","targetProperty":"dataValue"}, {}]
												}]
											}],
											number1: ["wm.Text", {"caption":"Number","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"160px"}, {}],
											panel50: ["wm.Panel", {"borderColor":"#FBFBFB","height":"26px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
												series1: ["wm.Text", {"caption":"Series","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"140px"}, {}],
												series3: ["wm.Text", {"caption":undefined,"dataValue":undefined,"displayValue":"","styles":{},"width":"40px"}, {}]
											}]
										}],
										panel58: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"486px"}, {}, {
											panel51: ["wm.Panel", {"borderColor":"#FBFBFB","height":"74px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"458px"}, {}, {
												author1: ["wm.Label", {"caption":"Author:","padding":"4","styles":{"fontWeight":"bolder"},"width":"100px"}, {}],
												authorFirst1: ["wm.Text", {"caption":"FirstName:","captionAlign":"left","dataValue":undefined,"displayValue":"","required":true,"styles":{}}, {}],
												lastname1: ["wm.Text", {"caption":"LastName:","captionAlign":"left","dataValue":undefined,"displayValue":"","required":true,"styles":{}}, {}]
											}],
											panel52: ["wm.Panel", {"borderColor":"#FBFBFB","height":"50px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"459px"}, {}, {
												statePrj1: ["wm.Text", {"caption":"State Project:","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"150px"}, {}],
												prjNo1: ["wm.Text", {"caption":"Project No:","captionAlign":"left","dataValue":undefined,"displayValue":"","styles":{},"width":"150px"}, {}]
											}]
										}]
									}],
									panel53: ["wm.Panel", {"borderColor":"#FBFBFB","height":"30px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
										reportTitle1: ["wm.SelectMenu", {"caption":"Project Name:","dataField":"projectName","dataType":"com.hrdb.data.Report","dataValue":undefined,"desktopHeight":"28px","displayField":"projectName","displayValue":"","height":"28px","styles":{},"width":"500px"}, {}, {
											binding: ["wm.Binding", {}, {}, {
												wire: ["wm.Wire", {"expression":undefined,"source":"reportLiveVariable1","targetProperty":"dataSet"}, {}]
											}]
										}]
									}],
									panel54: ["wm.Panel", {"height":"45px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"159px"}, {}, {
										next1: ["wm.Button", {"caption":"NEXT","desktopHeight":"35px","height":"35px","margin":"0,0,0,0","styles":{},"width":"100px"}, {"onclick":"next1Click"}]
									}],
									pageContainer3: ["wm.PageContainer", {"deferLoad":true,"showing":false}, {}],
									pageContainer4: ["wm.PageContainer", {"pageName":"ISANDANEXT","showing":false,"subpageEventlist":{},"subpageMethodlist":{},"subpageProplist":{}}, {}]
								}]
							}]
						}],
						layer7: ["wm.Layer", {"border":"1","borderColor":"#999999","caption":"TEST","horizontalAlign":"left","themeStyleType":"ContentPanel","verticalAlign":"top"}, {}, {
							dashboard1: ["wm.Dashboard", {"margin":"4","styles":{},"portlets":[
{"id":"portlet","title":"portlet","page":"ISADATA","isOpen":true,"isClosable":true,"x":0,"y":0}
]}, {}]
						}]
					}]
				}],
				panel6: ["wm.HeaderContentPanel", {"height":"24px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"100%"}, {}, {
					picture2: ["wm.Picture", {"height":"100%","source":"lib/wm/base/widget/themes/default/images/wmSmallLogo.png","width":"24px"}, {}],
					label2: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_10px"]},"caption":"Powered by WaveMaker","height":"100%","padding":"4"}, {}],
					label1: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_10px"]},"align":"right","caption":"Copyright 2012 [company name]","height":"100%","padding":"4","width":"100%"}, {}]
				}]
			}]
		}]
	}]
}