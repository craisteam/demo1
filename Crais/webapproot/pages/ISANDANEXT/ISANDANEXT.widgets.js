ISANDANEXT.widgets = {
	layoutBox1: ["wm.Layout", {"horizontalAlign":"center","verticalAlign":"top"}, {}, {
		panel8: ["wm.Panel", {"height":"187px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			panel6: ["wm.Panel", {"height":"169px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"347px"}, {}, {
				program: ["wm.Text", {"caption":"PROGRAMMING:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"desktopHeight":"23px","displayValue":"","height":"23px","styles":{},"width":"30%"}, {}],
				sectype: ["wm.Text", {"caption":"SECONDARY ACTVITY TYPE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}],
				primary: ["wm.Text", {"caption":"PRIMARY ACTIVITY TYPE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"25%"}, {}],
				district: ["wm.Text", {"caption":"RANGER DISTRICT:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}],
				func: ["wm.Text", {"caption":"PROJECT FUNCTION:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}]
			}],
			panel7: ["wm.Panel", {"height":"248px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"60px"}, {}],
			panel3: ["wm.Panel", {"height":"146px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"366px"}, {}, {
				total: ["wm.Text", {"caption":"TOTAL PROJECT ACREAGE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"57%"}, {}],
				survey: ["wm.Text", {"caption":"ACREAGE COMPLETELY SURVEYED:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}],
				sample: ["wm.Text", {"caption":"SAMPLE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"57%"}, {}],
				resurvey: ["wm.Text", {"caption":"ACREAGE RESURVEYED:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}],
				area: ["wm.Text", {"caption":"TOTAL NO. OF SITES IN PROJECT AREA:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"50%"}, {}],
				sites: ["wm.Text", {"caption":"NEW SITES:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}]
			}]
		}],
		panel4: ["wm.Panel", {"height":"280px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			panel11: ["wm.Panel", {"height":"278px","horizontalAlign":"left","verticalAlign":"top","width":"352px"}, {}, {
				panel14: ["wm.Panel", {"height":"97px","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
					text2: ["wm.Text", {"caption":"INSTITUTION COMD. PROJECT/SURVEY:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text1: ["wm.Text", {"caption":"AVG. NO. INDIV. USED:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
					text3: ["wm.Text", {"caption":"AVG. INDIV TRANSECT SPACING (FT):","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}]
				}],
				panel13: ["wm.Panel", {"height":"87px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
					label1: ["wm.Label", {"align":"left","caption":"SITES EVALUATED:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
					text13: ["wm.Text", {"caption":"ELIGIBLE:","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
					text14: ["wm.Text", {"caption":"NOT ELIGIBLE:","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}]
				}],
				text15: ["wm.Text", {"caption":"SITES INSP., MON., ENHANCED:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
				text16: ["wm.Text", {"caption":"RECOMMENDED DETERMINATION OF EFFECT:","captionAlign":"left","captionSize":"290px","dataValue":undefined,"displayValue":"","styles":{},"width":"345px"}, {}]
			}],
			panel12: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"58px"}, {}],
			panel2: ["wm.Panel", {"height":"243px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"365px"}, {}, {
				hrs: ["wm.Text", {"caption":"FIELD HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text4: ["wm.Text", {"caption":"LAB/LIB HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text5: ["wm.Text", {"caption":"TRAVEL HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text6: ["wm.Text", {"caption":"ADMIN HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text7: ["wm.Text", {"caption":"MILEAGE:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text8: ["wm.Text", {"caption":"PER DIEM RATE:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text9: ["wm.Text", {"caption":"DAYS OF PER DIEM:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text10: ["wm.Text", {"caption":"COST WEIGHT FACTOR:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text11: ["wm.Text", {"caption":"COST (CODE):","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
				text12: ["wm.Text", {"caption":"ACTUAL COST:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}]
			}]
		}],
		panel9: ["wm.Panel", {"height":"66px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"161px"}, {}, {
			save: ["wm.Button", {"caption":"SAVE","margin":"4"}, {"onclick":"saveVariable"}]
		}]
	}]
}