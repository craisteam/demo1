NextISAData.widgets = {
	saveVariable: ["wm.LiveVariable", {"autoUpdate":false,"inFlightBehavior":"executeAll","operation":"insert","startUpdate":false,"type":"com.hrdb.data.Crais_istandards_record"}, {"onSuccess":"saveVariableSuccess"}, {
		liveView: ["wm.LiveView", {"dataType":"com.hrdb.data.Crais_istandards_record","view":[
{"caption":"Id","sortable":true,"dataIndex":"id","type":"java.lang.Integer","displayType":"Number","required":true,"readonly":true,"includeLists":true,"includeForms":true,"order":0,"subType":null},
{"caption":"Region","sortable":true,"dataIndex":"region","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":1,"subType":null},
{"caption":"Rept_year","sortable":true,"dataIndex":"rept_year","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":2,"subType":null},
{"caption":"Forest","sortable":true,"dataIndex":"forest","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":3,"subType":null},
{"caption":"Rept_number","sortable":true,"dataIndex":"rept_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":4,"subType":null},
{"caption":"Series_1","sortable":true,"dataIndex":"series_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":5,"subType":null},
{"caption":"Series_2","sortable":true,"dataIndex":"series_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":6,"subType":null},
{"caption":"Rept_month","sortable":true,"dataIndex":"rept_month","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":7,"subType":null},
{"caption":"Rept_day","sortable":true,"dataIndex":"rept_day","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":8,"subType":null},
{"caption":"Author_1_last_name","sortable":true,"dataIndex":"author_1_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":9,"subType":null},
{"caption":"Author_1_FI","sortable":true,"dataIndex":"author_1_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":10,"subType":null},
{"caption":"Author_1_MI","sortable":true,"dataIndex":"author_1_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":11,"subType":null},
{"caption":"Author_2_last_name","sortable":true,"dataIndex":"author_2_last_name","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":12,"subType":null},
{"caption":"Author_2_FI","sortable":true,"dataIndex":"author_2_FI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":13,"subType":null},
{"caption":"Author_2_MI","sortable":true,"dataIndex":"author_2_MI","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":14,"subType":null},
{"caption":"Report_title","sortable":true,"dataIndex":"report_title","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":15,"subType":null},
{"caption":"District_number","sortable":true,"dataIndex":"district_number","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":16,"subType":null},
{"caption":"Proj_function","sortable":true,"dataIndex":"proj_function","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":17,"subType":null},
{"caption":"Activity_type_1","sortable":true,"dataIndex":"activity_type_1","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":18,"subType":null},
{"caption":"Activity_type_2","sortable":true,"dataIndex":"activity_type_2","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":19,"subType":null},
{"caption":"Programming","sortable":true,"dataIndex":"programming","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":20,"subType":null},
{"caption":"Total_proj_acreage","sortable":true,"dataIndex":"total_proj_acreage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":21,"subType":null},
{"caption":"Institution","sortable":true,"dataIndex":"institution","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":22,"subType":null},
{"caption":"Acreaege_comp_invent","sortable":true,"dataIndex":"acreaege_comp_invent","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":23,"subType":null},
{"caption":"Percent_sample","sortable":true,"dataIndex":"percent_sample","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":24,"subType":null},
{"caption":"Ave_no_individuals","sortable":true,"dataIndex":"ave_no_individuals","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":25,"subType":null},
{"caption":"Acreage_resurveyed","sortable":true,"dataIndex":"acreage_resurveyed","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":26,"subType":null},
{"caption":"Ave_spacing","sortable":true,"dataIndex":"ave_spacing","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":27,"subType":null},
{"caption":"Total_sites","sortable":true,"dataIndex":"total_sites","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":28,"subType":null},
{"caption":"Field_hours","sortable":true,"dataIndex":"field_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":29,"subType":null},
{"caption":"New_sites_located","sortable":true,"dataIndex":"new_sites_located","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":30,"subType":null},
{"caption":"Lab_lib_hours","sortable":true,"dataIndex":"lab_lib_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":31,"subType":null},
{"caption":"Travel_hours","sortable":true,"dataIndex":"travel_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":32,"subType":null},
{"caption":"Sites_eligible","sortable":true,"dataIndex":"sites_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":33,"subType":null},
{"caption":"Admin_hours","sortable":true,"dataIndex":"admin_hours","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":34,"subType":null},
{"caption":"Sites_not_eligible","sortable":true,"dataIndex":"sites_not_eligible","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":35,"subType":null},
{"caption":"Mileage","sortable":true,"dataIndex":"mileage","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":36,"subType":null},
{"caption":"Per_diem_rate","sortable":true,"dataIndex":"per_diem_rate","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":37,"subType":null},
{"caption":"Sites_enhanced","sortable":true,"dataIndex":"sites_enhanced","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":38,"subType":null},
{"caption":"Per_diem_days_paid","sortable":true,"dataIndex":"per_diem_days_paid","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":39,"subType":null},
{"caption":"Cost_weighting","sortable":true,"dataIndex":"cost_weighting","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":40,"subType":null},
{"caption":"Record_deter_effect","sortable":true,"dataIndex":"record_deter_effect","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":41,"subType":null},
{"caption":"Total_crm_cost","sortable":true,"dataIndex":"total_crm_cost","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":42,"subType":null},
{"caption":"Actual_crm_cost","sortable":true,"dataIndex":"actual_crm_cost","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":43,"subType":null},
{"caption":"State_project","sortable":true,"dataIndex":"state_project","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":44,"subType":null},
{"caption":"State_project_no","sortable":true,"dataIndex":"state_project_no","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":45,"subType":null},
{"caption":"Created_by","sortable":true,"dataIndex":"created_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":46,"subType":null},
{"caption":"Created_date","sortable":true,"dataIndex":"created_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":47,"subType":null},
{"caption":"Created_in_instance","sortable":true,"dataIndex":"created_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":48,"subType":null},
{"caption":"Modified_by","sortable":true,"dataIndex":"modified_by","type":"java.lang.String","displayType":"Text","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":49,"subType":null},
{"caption":"Modified_date","sortable":true,"dataIndex":"modified_date","type":"java.util.Date","displayType":"Date","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":50,"subType":null},
{"caption":"Modified_in_instance","sortable":true,"dataIndex":"modified_in_instance","type":"java.lang.Integer","displayType":"Number","required":false,"readonly":false,"includeLists":true,"includeForms":true,"order":51,"subType":null}
]}, {}],
		binding: ["wm.Binding", {}, {}, {
			wire: ["wm.Wire", {"expression":undefined,"source":"saveVariable","targetProperty":"sourceData"}, {}],
			wire1: ["wm.Wire", {"expression":undefined,"source":"program.dataValue","targetProperty":"sourceData.programming"}, {}],
			wire2: ["wm.Wire", {"expression":undefined,"source":"primary.dataValue","targetProperty":"sourceData.activity_type_1"}, {}],
			wire3: ["wm.Wire", {"expression":undefined,"source":"sectype.dataValue","targetProperty":"sourceData.activity_type_2"}, {}],
			wire4: ["wm.Wire", {"expression":undefined,"source":"text13.dataValue","targetProperty":"sourceData.sites_eligible"}, {}],
			wire5: ["wm.Wire", {"expression":undefined,"source":"text5.dataValue","targetProperty":"sourceData.travel_hours"}, {}],
			wire6: ["wm.Wire", {"expression":undefined,"source":"text8.dataValue","targetProperty":"sourceData.per_diem_rate"}, {}],
			wire7: ["wm.Wire", {"expression":undefined,"source":"text9.dataValue","targetProperty":"sourceData.per_diem_days_paid"}, {}]
		}]
	}],
	gotoISAMainPage: ["wm.NavigationCall", {"operation":"gotoPage"}, {}, {
		input: ["wm.ServiceInput", {"type":"gotoPageInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":"\"ISADATA\"","targetProperty":"pageName"}, {}]
			}]
		}]
	}],
	gotoTestPage: ["wm.NavigationCall", {"operation":"gotoPage"}, {}, {
		input: ["wm.ServiceInput", {"type":"gotoPageInputs"}, {}, {
			binding: ["wm.Binding", {}, {}, {
				wire: ["wm.Wire", {"expression":"\"Test\"","targetProperty":"pageName"}, {}]
			}]
		}]
	}],
	layoutBox1: ["wm.Layout", {"horizontalAlign":"center","styles":{"textAlign":"center"},"verticalAlign":"top"}, {}, {
		panel1: ["wm.Panel", {"height":"580px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
			panel5: ["wm.Panel", {"height":"69px","horizontalAlign":"center","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
				isandaTitle: ["wm.Panel", {"_classes":{"domNode":["titlebar"]},"border":"0,0,4,0","height":"96px","horizontalAlign":"left","layoutKind":"left-to-right","padding":"8","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
					isandaLabel: ["wm.Label", {"_classes":{"domNode":["wm_FontSizePx_24px"]},"align":"center","caption":"INVENTORY STANDARDS AND ACCOUNTING","height":"100%","padding":"4","styles":{"fontWeight":"bolder","textAlign":"center"},"width":"100%"}, {}]
				}]
			}],
			panel10: ["wm.Panel", {"height":"42px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}],
			panel8: ["wm.Panel", {"height":"187px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
				panel6: ["wm.Panel", {"height":"169px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"347px"}, {}, {
					program: ["wm.Text", {"caption":"PROGRAMMING:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"desktopHeight":"23px","displayValue":"","height":"23px","styles":{},"width":"30%"}, {}],
					sectype: ["wm.Text", {"caption":"SECONDARY ACTVITY TYPE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}],
					primary: ["wm.Text", {"caption":"PRIMARY ACTIVITY TYPE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"25%"}, {}],
					district: ["wm.Text", {"caption":"RANGER DISTRICT:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}],
					func: ["wm.Text", {"caption":"PROJECT FUNCTION:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"35%"}, {}]
				}],
				panel7: ["wm.Panel", {"height":"248px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"60px"}, {}],
				panel3: ["wm.Panel", {"height":"146px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"366px"}, {}, {
					total: ["wm.Text", {"caption":"TOTAL PROJECT ACREAGE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"57%"}, {}],
					survey: ["wm.Text", {"caption":"ACREAGE COMPLETELY SURVEYED:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}],
					sample: ["wm.Text", {"caption":"SAMPLE:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"57%"}, {}],
					resurvey: ["wm.Text", {"caption":"ACREAGE RESURVEYED:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}],
					area: ["wm.Text", {"caption":"TOTAL NO. OF SITES IN PROJECT AREA:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"50%"}, {}],
					sites: ["wm.Text", {"caption":"NEW SITES:","captionAlign":"left","captionSize":"265px","dataValue":undefined,"displayValue":"","styles":{},"width":"55%"}, {}]
				}]
			}],
			panel4: ["wm.Panel", {"height":"280px","horizontalAlign":"left","layoutKind":"left-to-right","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
				panel11: ["wm.Panel", {"height":"278px","horizontalAlign":"left","verticalAlign":"top","width":"352px"}, {}, {
					panel14: ["wm.Panel", {"height":"97px","horizontalAlign":"left","verticalAlign":"top","width":"100%"}, {}, {
						text2: ["wm.Text", {"caption":"INSTITUTION COMD. PROJECT/SURVEY:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
						text1: ["wm.Text", {"caption":"AVG. NO. INDIV. USED:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
						text3: ["wm.Text", {"caption":"AVG. INDIV TRANSECT SPACING (FT):","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}]
					}],
					panel13: ["wm.Panel", {"height":"87px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"100%"}, {}, {
						label1: ["wm.Label", {"align":"left","caption":"SITES EVALUATED:","padding":"4","styles":{"fontWeight":"bold"}}, {}],
						text13: ["wm.Text", {"caption":"ELIGIBLE:","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
						text14: ["wm.Text", {"caption":"NOT ELIGIBLE:","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}]
					}],
					text15: ["wm.Text", {"caption":"SITES INSP., MON., ENHANCED:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"85%"}, {}],
					text16: ["wm.Text", {"caption":"RECOMMENDED DETERMINATION OF EFFECT:","captionAlign":"left","captionSize":"290px","dataValue":undefined,"displayValue":"","styles":{},"width":"345px"}, {}]
				}],
				panel12: ["wm.Panel", {"height":"100%","horizontalAlign":"left","verticalAlign":"top","width":"58px"}, {}],
				panel2: ["wm.Panel", {"height":"243px","horizontalAlign":"left","styles":{},"verticalAlign":"top","width":"365px"}, {}, {
					hrs: ["wm.Text", {"caption":"FIELD HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text4: ["wm.Text", {"caption":"LAB/LIB HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text5: ["wm.Text", {"caption":"TRAVEL HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text6: ["wm.Text", {"caption":"ADMIN HOURS:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text7: ["wm.Text", {"caption":"MILEAGE:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text8: ["wm.Text", {"caption":"PER DIEM RATE:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text9: ["wm.Text", {"caption":"DAYS OF PER DIEM:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text10: ["wm.Text", {"caption":"COST WEIGHT FACTOR:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text11: ["wm.Text", {"caption":"COST (CODE):","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}],
					text12: ["wm.Text", {"caption":"ACTUAL COST:","captionAlign":"left","captionSize":"260px","dataValue":undefined,"displayValue":"","styles":{},"width":"75%"}, {}]
				}]
			}]
		}],
		panel9: ["wm.Panel", {"height":"66px","horizontalAlign":"left","layoutKind":"left-to-right","verticalAlign":"top","width":"161px"}, {}, {
			save: ["wm.Button", {"caption":"SAVE","margin":"4"}, {"onclick":"saveVariable"}]
		}]
	}]
}